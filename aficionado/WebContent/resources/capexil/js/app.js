$(function() {
	
	$.capexilapp = {
		init : function() { 
			setTimeout($.capexilapp.ping, 300000);
		},
		ping : function() {
	        $.ajax({
	            url: "/keep-alive"
	        });
	        setTimeout($.capexilapp.ping, 600000);
		}
	};
	
	$.capexilapp.init();
});