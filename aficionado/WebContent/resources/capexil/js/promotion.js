$(document).ready(function() {
	$('.promotion-read-more').click(function(event) {
		event.preventDefault();
		var div = $('.promotion-detail');
		if (div.hasClass('hidden')) {
			$('.promotion-detail').removeClass('hidden');
		} else {
			$('.promotion-detail').addClass('hidden');
		}
	});
});
