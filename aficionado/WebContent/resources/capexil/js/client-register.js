$(function() {
	$('#register').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'register:name' : {
				required : true
			},
			'register:email' : {
				require_from_group : [1, '.group'],
				email : true
			},
			'register:phone' : {
				require_from_group : [1, '.group'],
				validate_phone : true
			}
		},
		messages : {
			'register:name' : {
				required : 'Please enter your name.'
			},
			'register:email' : {
				require_from_group : 'Please enter either your email or your phone number.',
				email : 'Email address must be in the format of name@domain.com.'
			},
			'register:phone' : {
				require_from_group : 'Please enter either your email or your phone number.',
				validate_phone : 'Please enter a valid phone number'
			}
		}
	});
});
