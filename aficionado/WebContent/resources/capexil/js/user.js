$(document).ready(function()
{
	if ($("#edit-user-login\\:update-password").val() == "true")
	{
		$("#password").show();
	}

	$("#edit-password").click(function(event)
	{
		$("#password").toggle();
		var field = $("#edit-user-login\\:update-password");
		field.val(field.val() == "true" ? "false" : "true");
		event.preventDefault();
	});

	$("#edit-user-login").validate(
	{
		errorElement : "span",
		errorClass : "field-error",
		rules :
		{
			"edit-user-login:email" :
			{
				required : true,
				email : true
			},
			"edit-user-login:current-password" :
			{
				required : function()
				{
					return $("#edit-user-login\\:update-password").val() == "true";
				}
			},
			"edit-user-login:new-password" :
			{
				required : function()
				{
					return $("#edit-user-login\\:update-password").val() == "true";
				}
			},
			"edit-user-login:reenter-password" :
			{
				required : function()
				{
					return $("#edit-user-login\\:update-password").val() == "true";
				},
				equalTo : "#edit-user-login\\:new-password"
			}
		},
		messages :
		{
			"edit-user-login:email" :
			{
				required : "Please enter valid email.",
				email : "Email address must be in the format of name@domain.com."
			},
			"edit-user-login:current-password" :
			{
				required : "Please enter a password."
			},
			"edit-user-login:new-password" :
			{
				required : "Please enter new password."
			},
			"edit-user-login:reenter-password" :
			{
				required : "Please re-enter new password.",
				equalTo : "Please enter the same password as above."
			}
		}
	});
});
