$(function() {
	$('#contact').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'contact:name' : {
				required : true
			},
			'contact:email' : {
				required : true,
				email : true
			},
			'contact:comments' : {
				required : true
			}
		},
		messages : {
			'contact:name' : {
				required : 'Please enter your name.'
			},
			'contact:email' : {
				required : 'Please enter your email.',
				email : 'Email address must be in the format of name@domain.com.'
			},
			'contact:comments' : {
				required : 'Please enter your question or comments.'
			}
		}
	});
});
