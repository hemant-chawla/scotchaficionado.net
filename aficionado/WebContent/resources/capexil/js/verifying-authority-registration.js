$(document).ready(function() {
	$('#verifying-authority-registration').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'verifying-authority-registration:verifying-authority-type' : {
				required : true
			},
			'verifying-authority-registration:country' : {
				required : true
			},
			'verifying-authority-registration:contact-person' : {
				required : true
			},
			'verifying-authority-registration:phone' : {
				required : true
			},
			'verifying-authority-registration:email' : {
				required : true,
				email : true
			},
		},
		messages : {
			'verifying-authority-registration:verifying-authority-type' : {
				required : 'Please select your verifying authority type.'
			},
			'verifying-authority-registration:country' : {
				required : 'Please select your country.'
			},
			'verifying-authority-registration:contact-person' : {
				required : 'Please enter name of the contact person at your authority.'
			},
			'verifying-authority-registration:phone' : {
				required : 'Please enter your phone number.'
			},
			'verifying-authority-registration:email' : {
				required : 'Please enter your email.',
				email : 'Your email address must be in the format of name@domain.com.'
			},
		}
	});

});