var Page = {};

$(function() {

	$.extend(Page, {
		init : function(options) {
			var settings = this.settings = $.extend({}, {
				param : null,
				server : 'watchmethink.com'
			}, options);

			var t = this;
			var applyState = function(data) {
				t.action = data.action;
				t.id = data.id;

				if (settings.param != null) {
					if (settings.param in data) {
						t[settings.param] = data[settings.param];
					} else {
						t[settings.param] = undefined;
					}
				}

				performAction(data.action, data.id, t[settings.param]);
			};

			History.Adapter.bind(window, 'statechange', function() {
				var state = History.getState();
				applyState(state.data);
			});

			$('ol.breadcrumb a').click(function(e) {
				e.preventDefault();
				t.performAction('list');
			});

			$('#navbar li.active ul.dropdown-menu a').click(function(e) {
				e.preventDefault();
				t.performAction($(this).parent().data('action'));
			});

			var state = History.getState();
			var url = window.location.pathname;
			var urlKey = $('.section-content').data('section');
			var keyIndex = url.lastIndexOf('/' + urlKey + '/')
					+ urlKey.length + 2;

			settings.urlRoot = url.substring(url.indexOf(settings.server),
					keyIndex);

			if (state.data.action == null) {
				var parts = url.substring(keyIndex).split('/');
				var data = {};

				if (parts.length == 3) {
					var val = parts.pop();
					if (settings.param != null) {
						data[settings.param] = val;
					}
				}

				data.action = parts.pop();

				if (parts.length > 0) {
					data.id = parts[0];
				}

				History.replaceState(data, document.title, url);
			} else {
				applyState(state.data);
			}
		},
		performAction : function(action, id, param) {
			if (this.action == action && this.id == id) {
				if (this.settings.param == null
						|| this[this.settings.param] == param) {
					return;
				}
			}

			var url = this.settings.urlRoot;
			var data = {};

			if (id != null) {
				url += id + '/';
				data.id = id;
			}

			url += action;
			data.action = action;

			if (this.settings.param != null && param != null) {
				url += '/' + param;
				data[this.settings.param] = param;
			}

			History.pushState(data, document.title, url);
		},
		onPerformAction : function() {
			if (this.action == 'list') {
				$('#list').fadeIn(100).removeClass('hidden');
				$('ol.breadcrumb').addClass('hidden');
				this.initList();
			} else {
				$('ol.breadcrumb').removeClass('hidden');
				this.initItemNav();
				$('#item-container').fadeIn(100).removeClass('hidden');
				var section = this.action == 'new' ? 'edit' : this.action;
				$('#' + section).removeClass('hidden');

				$('#item-nav li[data-action=' + section + ']').addClass(
						'active');

				this.initSection(section);
			}
		},
		initItemNav : function() {
			var t = this;
			$('#item-nav a').click(function(e) {
				e.preventDefault();
				var action = $(this).parent().data('action');				
				if(action){
					t.performAction(action, t.id);					
				}
			});
		},
		showMessages : function() {
			$('#messages').delay(5000).fadeOut('slow');
		},
		initList : function() {
		},
		initSection : function(section) {
		}
	});
});