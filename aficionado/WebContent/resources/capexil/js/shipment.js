$(document).ready(function() {
	$('#shipment').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'shipment:products' : {
				required : true
			},
			'shipment:shipmentDate' : {
				required : true
			},
			'shipment:supporting-unit' : {
				required : true
			},
			'shipment:origin' : {
				required : true
			},
			'shipment:destination' : {
				required : true
			},
			'shipment:destinationPort' : {
				required : true
			},
			'shipment:loadingPort' : {
				required : true
			},
			'shipment:containerReceiptPlace' : {
				required : true
			},
			'shipment:consignee-address' : {
				required : true
			},
			'shipment:importPermitNumber' : {
				required : true
			},
			'shipment:ladingBillNumber' : {
				required : true
			},
			'shipment:vesselName' : {
				required : true
			},
			'shipment:ladingBillDate' : {
				required : true
			},
			'shipment:grossWeight' : {
				required : true
			},
			'shipment:netWeight' : {
				required : true
			},
			'shipment:weightUnit' : {
				required : true
			},
			'shipment:marksOnBag' : {
				required : true
			},
			'shipment:invoice-number' : {
				required : true
			},
			'shipment:invoice-date' : {
				required : true
			},
			'shipment:shipment-type' : {
				required : true
			},
			'shipment:invoice-amount' : {
				required : true
			},
			'shipment:invoice-currency' : {
				required : true
			},
			'shipment:packaging-type' : {
				required : true
			},
			'shipment:totalBags' : {
				required : true
			},
			'shipment:totalBagsWithCargo' : {
				required : true
			},
			'shipment:totalCargo' : {
				required : true
			},
			'shipment:totalCartons' : {
				required : true
			},
			'shipment:totalPieces' : {
				required : true
			},
			'shipment:totalContainers' : {
				required : true
			},
			'shipment:air-cargo-office' : {
				required : true
			},
			'shipment:transport-means' : {
				required : true
			},
			'shipment:payment-mode' : {
				required : true
			},
			'shipment:bank-name' : {
				required : true
			},
			'shipment:cheque-number' : {
				required : true
			},
			'shipment:cheque-date' : {
				required : true
			},
			'shipment:regional-office' : {
				required : true
			}
		},
		messages : {
			'shipment:product' : {
				required : 'Please select a product.'
			},
			'shipment:shipmentDate' : {
				required : 'Please enter a shipment date.'
			},
			'shipment:origin' : {
				required : 'Please select country of origin.'
			},
			'shipment:destination' : {
				required : 'Please select country of destination.'
			},
			'shipment:destinationPort' : {
				required : 'Please select destination port.'
			},
			'shipment:loadingPort' : {
				required : 'Please select port of loading.'
			},
			'shipment:containerReceiptPlace' : {
				required : 'Please select place of receipt of containers'
			},
			'shipment:consignee-address' : {
				required : 'Please enter consignee address'
			},
			'shipment:importPermitNumber' : {
				required : 'Please enter import permit number'
			},
			'shipment:ladingBillNumber' : {
				required : 'Please enter lading bill number'
			},
			'shipment:vesselName' : {
				required : 'Please enter vessel name.'
			},
			'shipment:ladingBillDate' : {
				required : 'Please enter lading bill date.'
			},
			'shipment:supporting-unit' : {
				required : 'Please mention your supporting unit.'
			},
			'shipment:grossWeight' : {
				required : 'Please enter gross weight of shipment.'
			},
			'shipment:netWeight' : {
				required : 'Please enter net weight of shipment.'
			},
			'shipment:weightUnit' : {
				required : 'Please select unit of weight.'
			},
			'shipment:marksOnBag' : {
				required : 'Please enter marks on bag information.'
			},
			'shipment:invoice-number' : {
				required : 'Please enter invoice number.'
			},
			'shipment:invoice-date' : {
				required : 'Please enter invoice date.'
			},
			'shipment:shipment-type' : {
				required : 'Please select shipment type.'
			},
			'shipment:air-cargo-office' : {
				required : 'Please enter air cargo office.'
			},
			'shipment:invoice-amount' : {
				required : 'Please enter invoice amount.'
			},
			'shipment:invoice-currency' : {
				required : 'Please enter invoice currency.'
			},
			'shipment:packaging-type' : {
				required : 'Please select packaging type.'
			},
			'shipment:totalBags' : {
				required : 'Please enter total number of bags in the shipment.'
			},
			'shipment:totalBagsWithCargo' : {
				required : 'Please enter total number of bags in the shipment.'
			},
			'shipment:totalCargo' : {
				required : 'Please enter total number of loose cargos in the shipment.'
			},
			'shipment:totalCartons' : {
				required : 'Please enter total number of cartons in the shipment.'
			},
			'shipment:totalPieces' : {
				required : 'Please enter total number of pieces in the shipment.'
			},
			'shipment:totalContainers' : {
				required : 'Please enter total number of containers in the shipment.'
			},
			'shipment:payment-mode' : {
				required : 'Please select a payment mode.'
			},
			'shipment:transport-means' : {
				required : 'Please selects means of transport.'
			},
			'shipment:bank-name' : {
				required : 'Please enter bank name.'
			},
			'shipment:cheque-number' : {
				required : 'Please enter cheque or DD number.'
			},
			'shipment:cheque-date' : {
				required : 'Please enter date on cheque or DD.'
			},
			'shipment:regional-office' : {
				required : 'Please select your regional office.'
			}
		}
	});

});