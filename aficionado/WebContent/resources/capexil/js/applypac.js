$(document).ready(function()
{
	$("#applypac").validate(
	{
		errorElement : "span",
		errorClass : "field-error",
		rules :
		{
			"applypac:first-name" :
			{
				required : true
			},
			"applypac:last-name" :
			{
				required : true
			},
			"applypac:address" :
			{
				required : true
			},
			"applypac:plant-location" :
			{
				required : true
			},
			"applypac:date" :
			{
				required : true
			}
		},
		messages :
		{
			"applypac:first-name" :
			{
				required : "Please enter your first name."
			},
			"applypac:last-name" :
			{
				required : "Please enter your last name."
			},
			"applypac:address" :
			{
				required : "Please enter your address."
			},
			"applypac:plant-location" :
			{
				required : "Please enter your plant location."
			},
			"applypac:date" :
			{
				required : "Please enter establishment date."
			}
		}
	});
});
