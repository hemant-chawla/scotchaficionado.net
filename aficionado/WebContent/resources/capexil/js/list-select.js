$(function() {
	$.fn.listSelect = function() {
		return this.each(function() {
			var $this = $(this);

			var checkAll = function() {
				var all = true;
				$('.select', $this).each(function() {
					if (!$(this).prop('checked')) {
						all = false;
						return false;
					}
				});

				$('.select-all', $this).prop('checked', all);
			};

			checkAll();

			$('.select', $this).change(function(e) {
				checkAll();
			});

			$('.select-all', $this).change(function(e) {
				$('.select', $this).prop('checked', $(this).prop('checked'));
			});
		});
	};
});
