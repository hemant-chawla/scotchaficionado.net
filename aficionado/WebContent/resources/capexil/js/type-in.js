$(function() {
	$.fn.typeIn = function(method) {
		var methods = {
			init : function(options) {
				this.typeIn.settings = options;

				return this.each(function() {
					var $element = $(this);
					element = this;

					$element.select2($.extend(options.selectOptions, {
						query : function(args) {
							$element.typeIn.callback = args.callback;
							options.callback(args.term);
						},
						initSelection : function(element, callback) {
							var data = {
								id : null,
								text : ''
							};
							if (element.val() != '') {
								data = $.parseJSON(element.val());
							}
							var val = '';
							if ($.isArray(data)) {
								for (var i = 0; i < data.length; i++) {
									if (val.length > 0) {
										val += ',';
									}
									val += data[i].id;
								}
							} else if ($.isPlainObject(data)) {
								val = data.id;
							}

							element.val(val);
							callback(data);
						}
					}));
				});
			},

			loadData : function(data) {
				this.typeIn.callback({
					results : data
				});
			}
		};

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method "' + method + '" does not exist in pluginName plugin!');
		}
	};

	$.fn.typeIn.settings = {};
});