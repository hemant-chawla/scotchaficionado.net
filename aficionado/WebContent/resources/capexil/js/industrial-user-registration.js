$(document).ready(function() {
	$('#industrial-user-registration').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'industrial-user-registration:industrial-user-type' : {
				required : true
			},
			'industrial-user-registration:company-name' : {
				required : true
			},
			'industrial-user-registration:company-address' : {
				required : true
			},
			'industrial-user-registration:plant-location' : {
				required : true
			},
			'industrial-user-registration:plant-state' : {
				required : true
			},
			'industrial-user-registration:district' : {
				required : true
			},
			'industrial-user-registration:plant-establishment-date' : {
				required : true
			},
			'industrial-user-registration:contact-person' : {
				required : true
			},
			'industrial-user-registration:phone' : {
				required : true
			},
			'industrial-user-registration:email' : {
				required : true,
				email : true
			},
		},
		messages : {
			'industrial-user-registration:industrial-user-type' : {
				required : 'Please select your industrial user type.'
			},
			'industrial-user-registration:company-name' : {
				required : 'Please enter your company name.'
			},
			'industrial-user-registration:company-address' : {
				required : 'Please enter your company address.'
			},
			'industrial-user-registration:plant-location' : {
				required : 'Please enter your plant location.'
			},
			'industrial-user-registration:plant-state' : {
				required : 'Please select the state as per your plant location.'
			},
			'industrial-user-registration:district' : {
				required : 'Please select your district.'
			},
			'industrial-user-registration:plant-establishment-date' : {
				required : 'Please enter your plant establishment date.'
			},
			'industrial-user-registration:contact-person' : {
				required : 'Please enter name of the contact person at your company.'
			},
			'industrial-user-registration:phone' : {
				required : 'Please enter your phone number.'
			},
			'industrial-user-registration:email' : {
				required : 'Please enter your email.',
				email : 'Your email address must be in the format of name@domain.com.'
			},
		}
	});

});