$(function() {
	$.settings = {
		init : function() {
			$('.date-time-picker').datetimepicker({
				 useSeconds: false,
				 sideBySide: true
			});
			
			$('#settings').validate({
				errorElement : 'span',
				errorClass : 'text-danger',
				rules : {
					'settings:pot' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:payout-date' : {
						required : true,
					},
					'settings:referralAwardAmount' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:brlRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:cadRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:cnyRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:eurRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:gbpRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:hkdRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:inrRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:jpyRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:nzdRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:rubRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:tryRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:usdRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:zarRate' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:keeper' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:favourite' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:free' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:free-retained' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:approved' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:retained' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					},
					'settings:rating' : {
						required : true,
						range : [0, Number.MAX_VALUE]
					}
				},
				messages : {
					'settings:pot' : {
						required : 'Please enter a Pot amount.',
						range : 'Please enter a valid value.'
					},
					'settings:payout-date' : {
						required : 'Please choose a date for pot payment.'
					},
					'settings:referralAwardAmount' : {
						required : 'Please enter an amount for the Referral Award.',
						range : 'Please enter a valid value.'
					},
					'settings:brlRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:cadRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:cnyRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:eurRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:gbpRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:hkdRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:inrRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:jpyRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:nzdRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:rubRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:tryRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:usdRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:zarRate' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:keeper' : {
						required : 'Please enter a conversion rate.',
						range : 'Please enter a valid value.'
					},
					'settings:keeper' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					},
					'settings:favourite' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					},
					'settings:free' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					},
					'settings:free-retained' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					},
					'settings:approved' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					},
					'settings:retained' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					},
					'settings:rating' : {
						required : 'Please enter a points value.',
						range : 'Please enter a valid value.'
					}
				}
			});
		},
		onSave : function() {
			this.showMessages();
			this.init();
		},
		showMessages : function() {
			$('#messages').delay(5000).fadeOut('slow');
		}
	};

	$.settings.init();
});