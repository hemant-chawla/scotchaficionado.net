$(function() {
	$('#forgot-password').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'forgot-password:email' : {
				required : true,
				email : true
			}
		},
		messages : {
			'forgot-password:email' : {
				required : 'Please enter your email.',
				email : 'Your email address must be in the format of name@domain.com.'
			}
		}
	});
});
