$(function() {
	$.validator.addMethod('validate_phone', function(value, element) {
		return this.optional(element) || /^(\+?[0-9\- \(\)]*$)/i.test(value);
	}, 'Please enter a valid phone number.');

	jQuery.validator.addMethod('require_from_group', function(value, element, options) {
		var validator = this;
		var selector = options[1];
		var validOrNot = $(selector, element.form).filter(function() {
			return validator.elementValue(this);
		}).length >= options[0];

		/*
		 * if(!$(element).data('being_validated')) { var fields = $(selector,
		 * element.form); fields.data('being_validated', true); fields.valid();
		 * fields.data('being_validated', false); }
		 */

		return validOrNot;
	}, jQuery.format('Please fill at least {0} of these fields.'));

	$.validator.addMethod('date_range', function(value, element, arg) {
		var enteredDate = Date.parse(value);
		if (enteredDate == Number.NaN) {
			return false;
		}

		var startDate = null;
		if (arg[0] != null) {
			startDate = Date.parse(arg[0]);
		}

		var endDate = null;
		if (arg[1] != null) {
			endDate = Date.parse(arg[1]);
		}

		return (startDate == null || startDate <= enteredDate) && (endDate == null || endDate >= enteredDate);

	}, $.validator.format('Please specify a date between {0} and {1}.'));

	$.validator.addMethod('equals', function(value, element, option) {
		return value == option;
	}, 'Please enter correct value.');

	$.validator.addMethod('emailWithWhitespace', function(value, element) {
		return (this.optional(element) || $.validator.methods.email.call(this, $.trim(value), element));
	}, 'Please enter a valid email');

	$.validator.addMethod('emailList', function(value, element) {
		if (this.optional(element)) {
			return true;
		}

		var values = value.split(/\s*[\s,]\s*/);
		var ret = true;
		for (var i = 0; i < values.length; i++) {
			var val = $.trim(values[i]);
			if (val != '') {
				ret &= $.validator.methods.email.call(this, val, element);
			}
		}

		return ret;
	}, 'Please enter a valid email');
});
