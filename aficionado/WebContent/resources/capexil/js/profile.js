$(document).ready(function() {
	$('#edit-user-personal-info').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'edit-user-personal-info:firm-name' : {
				required : true
			},
			'edit-user-personal-info:name' : {
				required : true
			},
			'edit-user-personal-info:phone' : {
				required : true
			},
			'edit-user-personal-info:fax' : {
				required : true
			}			
		},
		messages : {
			'edit-user-personal-info:firm-name' : {
				required : 'Please enter company name.'
			},
			'edit-user-personal-info:name' : {
				required : 'Please enter name of contact person.'
			},
			'edit-user-personal-info:phone' : {
				required : 'Please enter a valid phone number.'
			},
			'edit-user-personal-info:fax' : {
				required : 'Please enter fax number.'
			}
		}
	});

});
