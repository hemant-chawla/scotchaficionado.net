$(function() {
	$('#login').validate({
		errorElement : 'span',
		errorClass : 'text-danger',
		rules : {
			'login:email' : {
				required : true,
			},
			'login:password' : {
				required : true
			}
		},
		messages : {
			'login:email' : {
				required : 'Please enter your email.',
			},
			'login:password' : {
				required : 'Please enter your password.'
			}
		}
	});
});
