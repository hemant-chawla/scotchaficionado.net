$(function() {
	$.administrator = $.extend({}, Page, {
		initSection : function(section) {
			if (section == 'edit') {
				this.initForm();
			} else if (section == 'roles') {
				this.initRoles();
			}
		},
		initForm : function() {
			if (this.action == 'new') {
				$('#password').removeClass('hidden');
			}

			$('#edit-password').click(function(e) {
				e.preventDefault();
				if ($('#password').hasClass('hidden')) {
					$('#password').removeClass('hidden');
				} else {
					$('#password').addClass('hidden');
					$('#password').val('');
				}
			});

			$('.upload').upload({
				url : '/api/file-upload/profile',
				runtimes : 'html5,flash,silverlight',
				min_chunk_size : 512000,
				max_chunk_count : 20,
				max_file_size : '10mb',
				max_retries : 5,
				mime_types : [{
					title : 'Image files',
					extensions : 'jpg,jpeg,png,gif'
				}],
				complete : function(info) {
					reloadAvatar(info.file);
				}
			});

			$('#administrator').validate({
				errorElement : 'span',
				errorClass : 'text-danger',
				rules : {
					'administrator:email' : {
						required : true,
						email : true
					},
					'administrator:new-password' : {
						required : function() {
							return $.administrator.id == null;
						}
					},
					'administrator:reenter-password' : {
						required : function() {
							return $.administrator.id == null;
						},
						equalTo : '#administrator\\:new-password'
					},
					'administrator:first-name' : {
						required : true
					},
					'administrator:last-name' : {
						required : true
					},
					'administrator:display-name' : {
						required : true
					},
					'administrator:phone' : {
						validate_phone : true
					}
				},
				messages : {
					'administrator:email' : {
						required : 'Please enter a valid email.',
						email : 'Email address must be in the format of name@domain.com.'
					},
					'administrator:new-password' : {
						required : 'Please enter a new password.'
					},
					'administrator:reenter-password' : {
						required : 'Please re-enter the new password.',
						equalTo : 'Please enter the same password as above.'
					},
					'administrator:first-name' : {
						required : 'Please enter the first name.'
					},
					'administrator:last-name' : {
						required : 'Please enter the last name.'
					},
					'administrator:display-name' : {
						required : 'Please enter the display name.'
					},
					'administrator:phone' : {
						validate_phone : 'Please enter the phone number in a valid format.'
					}
				}
			});
		},
		initRoles : function() {
			$('#form-roles').ace('list-global');
		},
		onSave : function(id) {
			this.showMessages();
			if (id != null) {
				this.action = null;
				this.performAction('edit', id);
			} else {
				this.initForm();
			}
		},
		onDelete : function(success) {
			this.showMessages();
			if (success) {
				this.performAction('list');
			}
		},
		onToggleEnabled : function() {
			this.showMessages();
			this.initItemNav();
		},
		onSaveRoles : function(id) {
			this.showMessages();
			this.initRoles();
		}
	});

	$.administrator.init({
		urlKey : 'administrator'
	});
});