$(function() {
	$.fn.upload = function(opt) {
		var options = $.extend({
			runtimes : 'html5,flash,silverlight',
			min_chunk_size : 512000,
			max_chunk_count : 20,
			max_retries : 5,
			mime_types : [],
			multipart_params : {},
			s3 : false
		}, opt);

		if (options.s3) {
			options.key = options.multipart_params.key;
		}

		return this.each(function() {
			var $this = $(this);

			var resetUi = function() {
				$('.upload-complete', $this).addClass('hidden');
				$('.upload-error', $this).addClass('hidden');
				$('.upload-error .alert-type', $this).addClass('hidden');
				$('.upload-progress', $this).addClass('hidden');
				$('.filename', $this).html('');
				$('.progress-bar', $this).css('width', 0 + '%').attr('aria-valuenow', 0);
				$('.progress-bar span', $this).html('0%');
			};

			var uploader = new plupload.Uploader({
				browse_button : $('.upload-button', $this).get(0),
				container : $this.get(0),
				runtimes : options.runtimes,
				url : options.url,
				multi_selection : false,
				multipart_params : options.multipart_params,
				max_retries : options.max_retries,
				filters : {
					max_file_size : options.max_file_size,
					mime_types : options.mime_types
				},

				flash_swf_url : '/faces/javax.faces.resource/plupload/Moxie.swf',
				silverlight_xap_url : '/faces/javax.faces.resource/plupload/Moxie.xap',
				urlstream_upload : true,

				init : {

					FileFiltered : function(up, file) {
						resetUi();
						$('.filename', $this).html(file.name);
						$('.upload-progress', $this).removeClass('hidden');
					},

					FilesAdded : function(up, files) {
						setTimeout(function() {
							up.start();
						}, 1);
						if (options.start != null) {
							options.start();
						}

					},

					BeforeUpload : function(up, file) {
						var chunk_size = 0;
						if (file.size > options.min_chunk_size) {
							chunk_size = Math.ceil(file.size / options.max_chunk_count);
							if (chunk_size < options.min_chunk_size) {
								chunk_size = options.min_chunk_size;
							}
						}

						up.settings.chunk_size = chunk_size;

						if (!options.s3) {
							if (chunk_size > 0) {
								up.settings.multipart_params.chunksize = chunk_size;
							} else {
								delete (up.settings.multipart_params.chunksize);
							}
						} else {
							var name = options.key + '/source.' + file.name.split('.').pop().toLowerCase();
							if (chunk_size > 0) {
								name += '.1';
								up.settings.multipart_params.key = name;
								up.settings.multipart_params.Filename = name;
							} else {
								up.settings.multipart_params.key = name;
								up.settings.multipart_params.Filename = name;
								up.settings.multipart_params.chunk = 0;
								up.settings.multipart_params.chunks = -1;
							}
						}
					},

					ChunkUploaded : function(up, file, info) {
						if (options.s3) {
							var index = Math.floor(file.loaded / up.settings.chunk_size) + 1;
							var name = options.key + '/source.' + file.name.split('.').pop().toLowerCase() + '.'
									+ index;
							up.settings.multipart_params.key = name;
							up.settings.multipart_params.Filename = name;
						}
					},

					UploadProgress : function(up, file) {
						$('.progress-bar', $this).css('width', file.percent + '%').attr('aria-valuenow', file.percent);
						$('.progress-bar span', $this).html(file.percent + '%');
					},

					FileUploaded : function(up, file, info) {
						$('.upload-progress', $this).addClass('hidden');
						$('.upload-complete', $this).removeClass('hidden');
						if (options.complete != null) {
							var resp = null;
							if (info.response) {
								resp = jQuery.parseJSON(info.response).success;
							}

							options.complete(resp, up, file);
						}
					},

					Error : function(up, err) {
						resetUi();
						
						$('.upload-error', $this).removeClass('hidden');

						if (err.code == -600) {
							$('.alert-size', $this).removeClass('hidden');
						} else if (err.code == -601) {
							$('.alert-format', $this).removeClass('hidden');
						} else {
							$('.alert-other', $this).removeClass('hidden');
						}

						if (options.error != null) {
							options.error();
						}						
					}
				}
			});

			uploader.init();
		});
	};
});
