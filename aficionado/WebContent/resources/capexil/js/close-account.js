$(function() {
	$('.close-account-confirm').hide();

	$('.close-account').click(function(event) {
		$(this).hide();
		$('.close-account-confirm').show();
		event.preventDefault();
	});

	$('.close-account-cancel').click(function(event) {
		$('.close-account-confirm').hide();
		$('.close-account').show();
		event.preventDefault();
	});

});
