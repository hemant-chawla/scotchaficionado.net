package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ComponentSystemEvent;

import com.aficionado.domain.Log;
import com.aficionado.service.LogService;
import com.aficionado.service.UserService;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.util.Filter;
import com.aficionado.util.FilterType;
import com.aficionado.util.SortOrder;
import com.aficionado.web.controller.exception.ControllerException;
import com.aficionado.web.controller.model.ListDataModel;

@ManagedBean
@ViewScoped
public class LogListController implements Serializable
{

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{authController}")
	private AuthController authController;

	private int entityId;
	
	private ListDataModel<Log> list;

	@EJB
	private LogService logService;
	
	@EJB
	private UserService userService;

	@PreDestroy
	public void destroy()
	{
		list = null;
	}

	public int getEntityId()
	{
		return entityId;
	}

	public ListDataModel<Log> getList()
	{
		return list;
	}

	public void init(ComponentSystemEvent event)
	{
		if (list != null)
		{
			return;
		}

		Collection<Filter> filters = new ArrayList<Filter>(4);
		filters.add(new Filter("entityType", FilterType.Equals));
		filters.add(new Filter("type", FilterType.Equals));
		filters.add(new Filter("user.email", FilterType.Contains));
		filters.add(new Filter("user.firstName", FilterType.Contains));

		list = new ListDataModel<Log>("createdAt", SortOrder.Descending, filters)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected Long getCount(Collection<Filter> filters)
			{
				try
				{
					return logService.getCount(filters, authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

			@Override
			protected Object getKey(Log log)
			{
				return log.getId();
			}

			@Override
			protected List<Log> getList(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
			{
				try
				{
					return logService.getAll(first, pageSize, sortField, sortOrder, filters, authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

		};
	}

	public void initByEntityId(ComponentSystemEvent event)
	{
		if (list != null)
		{
			return;
		}

		Collection<Filter> filters = new ArrayList<Filter>(4);
		filters.add(new Filter("type", FilterType.Equals));
		filters.add(new Filter("user.email", FilterType.Contains));
		filters.add(new Filter("user.firstName", FilterType.Contains));

		list = new ListDataModel<Log>("createdAt", SortOrder.Descending, filters)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected Long getCount(Collection<Filter> filters)
			{
				try
				{
					return logService.getCount(filters, authController.getUser(), entityId);
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

			@Override
			protected Object getKey(Log log)
			{
				return log.getId();
			}

			@Override
			protected List<Log> getList(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
			{
				try
				{
					return logService.getAllByEntityId(first, pageSize, sortField, sortOrder, filters, authController.getUser(), entityId);
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

		};
	}

	public void initByUser(ComponentSystemEvent event)
	{
		if (list != null)
		{
			return;
		}

		Collection<Filter> filters = new ArrayList<Filter>(2);
		filters.add(new Filter("entityType", FilterType.Equals));
		filters.add(new Filter("type", FilterType.Equals));
		
		list = new ListDataModel<Log>("createdAt", SortOrder.Descending, filters)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected Long getCount(Collection<Filter> filters)
			{
				try
				{
					return logService.getCount(filters, authController.getUser(), authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

			@Override
			protected Object getKey(Log log)
			{
				return log.getId();
			}

			@Override
			protected List<Log> getList(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
			{
				try
				{
					return logService.getAllByUser(first, pageSize, sortField, sortOrder, filters, authController.getUser(), authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

		};
	}
	
	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}

	public void setEntityId(int entityId)
	{
		this.entityId = entityId;
	}
}
