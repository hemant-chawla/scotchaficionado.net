package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.aficionado.domain.Setting;
import com.aficionado.service.SettingService;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.web.controller.exception.ControllerException;

@ManagedBean
@ViewScoped
public class SettingController implements Serializable
{
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{authController}")
	private AuthController authController;

	@EJB
	private SettingService settingService;

	private Map<String, Setting> settings;

	public Setting getSetting(String id)
	{
		return settings.get(id);
	}

	@PostConstruct
	public void init()
	{
		try
		{
			settings = settingService.getAll(authController.getUser());
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}
	}

	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}

	public void save()
	{
		try
		{		
			FacesContext context = FacesContext.getCurrentInstance();
			settingService.save(settings, authController.getUser());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Settings have been saved."));
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}			
	}

}
