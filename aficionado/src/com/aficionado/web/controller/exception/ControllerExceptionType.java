package com.aficionado.web.controller.exception;

public enum ControllerExceptionType
{
	EmailAlreadyExists, InvalidParam, InvalidPassword, System, Unauthorized
}