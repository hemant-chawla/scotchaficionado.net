package com.aficionado.web.controller.exception;

import javax.faces.FacesException;

import com.aficionado.service.exception.ServiceException;

public class ControllerException extends FacesException
{
	private static final long serialVersionUID = 1L;

	private ControllerExceptionType exceptionType;

	public ControllerException(ControllerExceptionType exceptionType)
	{
		super();
		this.exceptionType = exceptionType;
	}

	public ControllerException(ControllerExceptionType exceptionType, Throwable cause)
	{
		super(cause);
		this.exceptionType = exceptionType;
	}

	public ControllerException(ServiceException exception)
	{
		super(exception);
		switch (exception.getExceptionType())
		{
		case EmailAlreadyExists:
			this.exceptionType = ControllerExceptionType.EmailAlreadyExists;
			break;
		case InvalidParam:
			this.exceptionType = ControllerExceptionType.InvalidParam;
			break;
		case InvalidPassword:
			this.exceptionType = ControllerExceptionType.InvalidPassword;
			break;
		case Unauthorized:
			this.exceptionType = ControllerExceptionType.Unauthorized;
			break;
		case System:
		default:
			this.exceptionType = ControllerExceptionType.System;
			break;
		}
	}

	public ControllerExceptionType getExceptionType()
	{
		return exceptionType;
	}
}
