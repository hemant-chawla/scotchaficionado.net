package com.aficionado.web.controller.exception;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

public class AficionadoExceptionHandler extends ExceptionHandlerWrapper
{
	private static final Logger log = Logger.getLogger(AficionadoExceptionHandler.class.getName());

	private ExceptionHandler wrapped;

	public AficionadoExceptionHandler(ExceptionHandler wrapped)
	{
		this.wrapped = wrapped;
	}

	@Override
	public ExceptionHandler getWrapped()
	{
		return this.wrapped;
	}

	@Override
	public void handle() throws FacesException
	{
		for (Iterator<ExceptionQueuedEvent> iterator = getUnhandledExceptionQueuedEvents().iterator(); iterator.hasNext();)
		{
			ExceptionQueuedEvent event = iterator.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();
			Throwable throwable = context.getException();
			FacesContext facesContext = FacesContext.getCurrentInstance();

			if (throwable instanceof ViewExpiredException)
			{
				NavigationHandler navigationHandler = facesContext.getApplication().getNavigationHandler();
				try
				{

					navigationHandler.handleNavigation(facesContext, null, "/account/login");
					facesContext.renderResponse();

				}
				finally
				{
					iterator.remove();
				}
			}
			else
			{
				if (!(throwable.getCause() instanceof ControllerException) || ((ControllerException) throwable.getCause()).getExceptionType() == ControllerExceptionType.System)
				{
					log.log(Level.SEVERE, "System Error", throwable);
				}
			}
		}

		wrapped.handle();
	}
}
