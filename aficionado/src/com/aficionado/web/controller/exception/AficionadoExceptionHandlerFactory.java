package com.aficionado.web.controller.exception;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class AficionadoExceptionHandlerFactory extends ExceptionHandlerFactory
{
	private ExceptionHandlerFactory wrapped;

	public AficionadoExceptionHandlerFactory(ExceptionHandlerFactory wrapped)
	{
		this.wrapped = wrapped;
	}

	@Override
	public ExceptionHandler getExceptionHandler()
	{
		return  new AficionadoExceptionHandler(wrapped.getExceptionHandler());
	}
	
    @Override
    public ExceptionHandlerFactory getWrapped() 
    {
    	return wrapped;
    }
}
