package com.aficionado.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aficionado.domain.User;
import com.aficionado.service.UserService;
import com.aficionado.util.Constants;
import com.aficionado.web.controller.exception.ControllerException;
import com.aficionado.web.controller.exception.ControllerExceptionType;

@ManagedBean
@SessionScoped
public class AuthController implements Serializable
{
	private static final long serialVersionUID = 1L;

	private long lastUpdateTime;

	private String redirect = null;

	private User user;

	private static final Logger log = Logger.getLogger(AuthController.class.getName());

	@EJB
	private UserService userService;

	@PostConstruct
	public void construct()
	{
		if (user == null || user.getId() != null)
		{
			user = new User();
		}
	}

	private String getRedirectPath()
	{
		User user = getUser();
		
		if (user.getId() == null)
		{
			return null;
		}

		if (redirect != null)
		{
			return null;
		}
		
		if (user.getUserType().equals("SuperAdministrator"))
		{
			return "/super/index?faces-redirect=true";
		}

		if (user.getUserType().equals("AccountUser"))
		{
			return "/account-user/index?faces-redirect=true";
		}
		
		return null;
	}

	public User getUser()
	{
		initUser();
		return user;
	}
	
	public void init(ComponentSystemEvent event)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		initUser();
		if (user.getId() == null)
		{
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

			String forward = (String) request.getAttribute("javax.servlet.forward.request_uri");
			if (forward != null && !forward.equalsIgnoreCase("/account/login"))
			{
				redirect = forward;
			}
			return;
		}

		String outcome = getRedirectPath();
		log.info("User: "+user.getEmail() + ", redirectPath: "+outcome);
		if (outcome != null)
		{
			context.getApplication().getNavigationHandler().handleNavigation(context, null, outcome);
			return;
		}

		if (redirect != null)
		{
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			try
			{
				response.sendRedirect(redirect);
				redirect = null;
			}
			catch (IOException e)
			{
				redirect = null;
				throw new ControllerException(ControllerExceptionType.System, e);
			}
		}
	}

	private void initUser()
	{
		if (user.getId() == null)
		{
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			
			Principal principal = request.getUserPrincipal();
			if (principal == null)
			{
				return;
			}

			user = userService.get(principal.getName());
			lastUpdateTime = System.currentTimeMillis();
		}
		else
		{
			if (System.currentTimeMillis() - lastUpdateTime > Constants.USER_CACHE_INTERVAL)
			{
				user = userService.refresh(user);
				lastUpdateTime = System.currentTimeMillis();
			}
		}
	}

	public String login()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try
		{
			request.login(user.getEmail(), user.getPassword());
			user = userService.login(user.getEmail());
			
			return getRedirectPath();
		}
		catch (ServletException e)
		{
			e.printStackTrace();
			context.addMessage("login", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "The email or password you've entered is incorrect."));
		}

		return null;
	}

	public void logout(ComponentSystemEvent event)
	{
		FacesContext context = FacesContext.getCurrentInstance();

		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();

		try
		{
			request.logout();
			user = new User();
			context.getApplication().getNavigationHandler().handleNavigation(context, null, "/account/login?faces-redirect=true");
		}
		catch (ServletException e)
		{
			throw new ControllerException(ControllerExceptionType.System, e);
		}
	}

	public void refreshUser()
	{
		lastUpdateTime = 0;
	}
}