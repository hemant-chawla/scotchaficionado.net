package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.aficionado.domain.User;
import com.aficionado.service.UserService;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.web.controller.exception.ControllerException;

public abstract class UserController implements Serializable
{
	
	private static final Logger log = Logger.getLogger(UserController.class.getName());

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{authController}")
	protected AuthController authController;
	
	protected Integer id;
	
	protected String newPassword;
	
	protected String password;
	
	protected boolean updatePassword;
	
	protected User user;
	
	@EJB
	protected UserService userService;
	
	@PreDestroy
	public void destroy()
	{
		user = null;
	}

	protected abstract User getById(Integer id) throws ServiceException;

	public Integer getId()
	{
		return id;
	}

	public String getNewPassword()
	{
		return newPassword;
	}

	public String getPassword()
	{
		return password;
	}

	public User getUser()
	{
		return user;
	}

	public void init(ComponentSystemEvent event)
	{
		/*if (user != null)
		{
			return;
		}*/
		try
		{
			user = getById(authController.getUser().getId());
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}
		updatePassword = (user.getId() == null);
	}

	public void init(Integer id)
	{
		if (user != null)
		{
			return;
		}
		try
		{
			user = getById(id);
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}
		
		updatePassword = (user.getId() == null);
	}


	/**
	 * 
	 * @param event
	 */
	public void initNewUser(ComponentSystemEvent event)
	{
		if (user != null)
		{
			return;
		}
		try
		{
			user = getById(id);
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}
		
		if (user.getPassword() != null)
		{
			password = user.getPassword();	
		}
		
		password = user.getPassword();
		updatePassword = (user.getId() == null);
	}

	/**
	 * 
	 * @param event
	 */
	public void initUser(ComponentSystemEvent event)
	{
		try
		{
			user = getById(id);
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}
		
		password = user.getPassword();
		updatePassword = (user.getId() == null);
	}

	public boolean isUpdatePassword()
	{
		return updatePassword;
	}
	
	public void resetPassword()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		
		try
		{
			log.info("Resetting password for user : "+user.getEmail());
			userService.resetPassword(user.getEmail());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Password sent to the user's registered email."));
		}
		catch (ServiceException e)
		{

			throw new ControllerException(e);

		}
	}
	
	

	public String save()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		try
		{
			boolean isNew = user.getId() == null;
			user = userService.save(user, updatePassword, newPassword, password, authController.getUser());

			if (user.equals(authController.getUser()))
			{
				authController.refreshUser();
			}
			if (isNew)
			{
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, "New user has been created successfully. Login credentials sent to the user's email."));
			}
			else
			{
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, "User information has been updated successfully."));
			}
			
			if (isNew)
			{
				return context.getViewRoot().getViewId() + "?faces-redirect=true&id=" + user.getId();
			}
			else
			{
				return context.getViewRoot().getViewId() + "?faces-redirect=true&includeViewParams=true";
			}
		}
		catch (ServiceException e)
		{
			switch (e.getExceptionType())
			{
				case EmailAlreadyExists:
					context.addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "This email is already registered with Scotch Aficionado."));
					break;
				case InvalidPassword:
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Password is invalid. Please try again."));
					break;
				default:
					throw new ControllerException(e);
			}
		}

		return null;
	}

	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setNewPassword(String newPassword)
	{
		this.newPassword = newPassword;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void setUpdatePassword(boolean updatePassword)
	{
		this.updatePassword = updatePassword;
	}
}
