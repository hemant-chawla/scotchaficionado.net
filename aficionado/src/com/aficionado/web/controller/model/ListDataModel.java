package com.aficionado.web.controller.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.richfaces.model.DataVisitor;
import org.richfaces.model.ExtendedDataModel;
import org.richfaces.model.Range;
import org.richfaces.model.SequenceRange;

import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;


public abstract class ListDataModel<E> extends ExtendedDataModel<E> implements Serializable, PropertyChangeListener
{
	private static final long serialVersionUID = 1L;
	private Map<String, Filter> filters;
	private Long rowCount;
	private Object rowKey;
	private Map<Object, E> rows;
	private SortOrder sortOrder;
	private String sortProperty;
	private List<E> selected;
	private SequenceRange range;
	private boolean reinitRows;

	public ListDataModel()
	{
		super();
		reinitRows = true;
	}

	public ListDataModel(Collection<Filter> filters)
	{
		this(null, null, filters);
	}

	public ListDataModel(String sortProperty, SortOrder sortOrder)
	{
		this(sortProperty, sortOrder, null);
	}

	public ListDataModel(String sortProperty, SortOrder sortOrder, Collection<Filter> filters)
	{
		super();
		this.sortOrder = sortOrder;
		this.sortProperty = sortProperty;
		this.filters = new HashMap<String, Filter>(filters.size());
		for (Filter filter : filters)
		{
			filter.addPropertyChangeListener(this);
			this.filters.put(filter.getName(), filter);
		}

		reinitRows = true;
	}

	public void clear()
	{
		selected = null;
		rowCount = null;
		rows = null;
		rowKey = null;
	}

	public void clearFilters()
	{
		for (Filter filter : filters.values())
		{
			filter.setValue(null);
		}
	}

	protected abstract Long getCount(Collection<Filter> filters);

	public Map<String, Filter> getFilters()
	{
		return filters;
	}

	protected abstract Object getKey(E e);

	protected abstract List<E> getList(int first, int pageSize, String sortField, SortOrder sortOrder,
			Collection<Filter> filters);

	@Override
	public int getRowCount()
	{
		if (rowCount == null)
		{
			rowCount = getCount(filters.values());
		}

		return rowCount.intValue();
	}

	@Override
	public E getRowData()
	{
		return rows.get(rowKey);
	}

	@Override
	public int getRowIndex()
	{
		return -1;
	}

	@Override
	public Object getRowKey()
	{
		return rowKey;
	}

	public boolean getRowSelected()
	{
		if (selected == null)
		{
			selected = new ArrayList<E>();
			return false;
		}

		return selected.contains(getRowData());
	}

	public List<E> getSelected()
	{
		return selected;
	}

	public SortOrder getSortOrder(String sortField)
	{
		if (sortField.equalsIgnoreCase(this.sortProperty))
		{
			return sortOrder;
		}

		return SortOrder.Unsorted;
	}

	@Override
	public Object getWrappedData()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isRowAvailable()
	{
		return rowKey != null;
	}

	public void propertyChange(PropertyChangeEvent event)
	{
		reinitRows = true;
		rowCount = null;
	}

	@Override
	public void setRowIndex(int rowIndex)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRowKey(Object key)
	{
		rowKey = key;
	}

	public void setRowSelected(boolean value)
	{
		if (selected == null)
		{
			selected = new ArrayList<E>();
		}

		if (value)
		{
			if (!selected.contains(getRowData()))
			{
				selected.add(getRowData());
			}
		}
		else
		{
			if (selected.contains(getRowData()))
			{
				selected.remove(getRowData());
			}
		}
	}

	public void setSelected(List<E> selected)
	{
		this.selected = selected;
	}

	public void setSortOrder(String sortProperty, SortOrder sortOrder)
	{
		if (this.sortProperty != sortProperty || this.sortOrder != sortOrder)
		{
			reinitRows = true;
		}

		this.sortProperty = sortProperty;
		this.sortOrder = sortOrder;
	}

	@Override
	public void setWrappedData(Object data)
	{
		throw new UnsupportedOperationException();
	}

	public void toggleSortOrder(String sortProperty)
	{
		reinitRows = true;

		if (this.sortProperty != sortProperty)
		{
			this.sortOrder = SortOrder.Unsorted;
		}

		this.sortProperty = sortProperty;

		switch (this.sortOrder)
		{
		case Unsorted:
			this.sortOrder = SortOrder.Ascending;
			break;
		case Ascending:
			this.sortOrder = SortOrder.Descending;
			break;
		case Descending:
			this.sortOrder = SortOrder.Unsorted;
			break;
		}
	}

	@Override
	public void walk(FacesContext context, DataVisitor visitor, Range range, Object argument)
	{
		SequenceRange sequenceRange = (SequenceRange) range;

		if (this.range == null || sequenceRange.getFirstRow() != this.range.getFirstRow()
				|| sequenceRange.getRows() != this.range.getRows())
		{
			this.range = sequenceRange;
			rows = null;
		}

		if (reinitRows)
		{
			rows = null;
			reinitRows = false;
		}

		if (rows == null)
		{

			List<E> data = getList(sequenceRange.getFirstRow(), sequenceRange.getRows(), sortProperty, sortOrder,
					filters.values());

			rows = new LinkedHashMap<Object, E>(data.size());
			for (E e : data)
			{
				Object k = this.getKey(e);
				rows.put(k, e);
				visitor.process(context, k, argument);
			}
		}
		else
		{
			for (E e : rows.values())
			{
				Object k = this.getKey(e);
				visitor.process(context, k, argument);
			}
		}
	}

}
