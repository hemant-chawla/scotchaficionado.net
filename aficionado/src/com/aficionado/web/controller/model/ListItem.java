package com.aficionado.web.controller.model;

import java.io.Serializable;

public class ListItem implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String id;
	private String text;

	public ListItem(String id, String text)
	{
		this.id = id;
		this.text = text;
	}

	public String getId()
	{
		return id;
	}

	public String getText()
	{
		return text;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setText(String text)
	{
		this.text = text;
	}

}
