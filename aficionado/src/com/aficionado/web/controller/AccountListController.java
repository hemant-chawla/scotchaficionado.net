package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.aficionado.domain.Account;
import com.aficionado.service.AccountService;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.util.Filter;
import com.aficionado.util.FilterType;
import com.aficionado.util.SortOrder;
import com.aficionado.web.controller.exception.ControllerException;
import com.aficionado.web.controller.model.ListDataModel;

@ManagedBean
@ViewScoped
public class AccountListController implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@EJB
	private AccountService accountService;
	
	@ManagedProperty(value = "#{authController}")
	private AuthController authController;
	
	private ListDataModel<Account> list;
	
	private Integer selectedAccountId;

	private int size;

	@PreDestroy
	public void destroy()
	{
		list = null;
	}

	public ListDataModel<Account> getList()
	{
		return list;
	}
	
	public Integer getSelectedAccountId()
	{
		return selectedAccountId;
	}

	public int getSize()
	{
		if (list != null)
		{
			size = list.getRowCount();
		}

		return size;
	}
	
	public void init(ComponentSystemEvent event)
	{
		if (list != null)
		{
			return;
		}
		
		Collection<Filter> filters = new ArrayList<Filter>(5);
		filters.add(new Filter("category", FilterType.Equals));
		filters.add(new Filter("status", FilterType.Equals));
		filters.add(new Filter("email", FilterType.Contains));
		filters.add(new Filter("shortCode", FilterType.Contains));
		filters.add(new Filter("title", FilterType.Contains));
		
		list = new ListDataModel<Account>("createdAt", SortOrder.Descending, filters)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected Long getCount(Collection<Filter> filters)
			{
				try
				{
					return accountService.getCount(filters, authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

			@Override
			protected Object getKey(Account account)
			{
				return account.getId();
			}

			@Override
			protected List<Account> getList(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
			{
				try
				{
					return accountService.getAll(first, pageSize, sortField, sortOrder, filters, authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}
		};

	}

	public void open()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		context.getApplication().getNavigationHandler().handleNavigation(context, null, "edit?faces-redirect=true&id=" + selectedAccountId);
	}
	
	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}

	public void setSelectedAccountId(Integer selectedAccountId)
	{
		this.selectedAccountId = selectedAccountId;
	}


}
