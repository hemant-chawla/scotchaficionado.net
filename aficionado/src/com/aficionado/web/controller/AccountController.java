package com.aficionado.web.controller;

import java.io.Serializable;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import com.aficionado.domain.Account;
import com.aficionado.service.AccountService;
import com.aficionado.service.NotificationService;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.web.controller.exception.ControllerException;

@ManagedBean
@ViewScoped
public class AccountController implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private Account account;
	
	@EJB
	private AccountService accountService;
	
	@ManagedProperty(value = "#{authController}")
	private AuthController authController;
	
	private Integer id;
	
	@EJB
	private NotificationService notificationService;

	@PreDestroy
	public void destroy()
	{
		account = null;
	}

	public Account getAccount()
	{
		return account;
	}

	public Integer getId()
	{
		return id;
	}
	
	public void init(ComponentSystemEvent event)
	{
		try
		{
			account = accountService.get(id, authController.getUser());
		}
		catch (ServiceException e)
		{
			throw new ControllerException(e);
		}
	}

	public void save()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		try
		{
			account = accountService.save(account, authController.getUser());
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Account details have been saved successfully."));
			//notificationService.createAccountCreated(account, authController.getUser());

		}
		catch (ServiceException e)
		{
			switch (e.getExceptionType())
			{
				case EmailAlreadyExists:
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
							"Email already exists for the account."));
					break;
				case ShortCodeAlreadyExists:
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
							"Short code already exists for the account."));
					break;	
				default:
					throw new ControllerException(e);
			}
		}		
	}
	public void setAccount(Account account)
	{
		this.account = account;
	}

	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

}
