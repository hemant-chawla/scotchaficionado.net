package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import com.aficionado.domain.SuperAdministrator;
import com.aficionado.domain.User;
import com.aficionado.domain.type.AccountCategory;
import com.aficionado.domain.type.AccountStatus;
import com.aficionado.domain.type.EntityType;
import com.aficionado.domain.type.LogType;
import com.aficionado.domain.type.NotificationFrequency;
import com.aficionado.domain.type.NotificationType;
import com.aficionado.domain.type.RoleType;
import com.aficionado.domain.type.UserStatus;
import com.aficionado.service.util.RoleHelper;

@ManagedBean
@ApplicationScoped
public class UtilityController implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static SelectItem[] getSelectItems(Enum<?>[] values)
	{
		SelectItem list[] = new SelectItem[values.length];
		int i = 0;
		for (Enum<?> value : values)
		{
			list[i] = new SelectItem(value, value.name());
			i++;
		}

		return list;
	}

	private static SelectItem[] getSelectItemsWithBlank(Enum<?>[] values)
	{
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (Enum<?> value : values)
		{
			list[i] = new SelectItem(value, value.name());
			i++;
		}

		return list;
	}
	
	public SelectItem[] getAccountCategoryValues()
	{
		AccountCategory[] values = AccountCategory.values();
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (AccountCategory value : values)
		{
			list[i] = new SelectItem(value, value.getValue());
			i++;
		}

		return list;
	}
	
	public SelectItem[] getEntityTypeValues()
	{
		EntityType[] values = EntityType.values();
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (EntityType value : values)
		{
			list[i] = new SelectItem(value, value.getValue());
			i++;
		}

		return list;
	}

	public Date getCurrentDate()
	{
		return new Date();
	}

	public SelectItem[] getAccountStatusValues()
	{
		AccountStatus[] values = AccountStatus.values();
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (AccountStatus value : values)
		{
			list[i] = new SelectItem(value, value.getValue());
			i++;
		}

		return list;
	}

	public SelectItem[] getLogTypeValues()
	{
		LogType[] values = LogType.values();
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (LogType value : values)
		{
			list[i] = new SelectItem(value, value.getValue());
			i++;
		}

		return list;
	}
	
	public SelectItem[] getNotificationFrequencyValues()
	{
		NotificationFrequency[] values = NotificationFrequency.values();
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (NotificationFrequency value : values)
		{
			list[i] = new SelectItem(value, value.getValue());
			i++;
		}

		return list;
	}

	public SelectItem[] getNotificationTypeValues()
	{
		NotificationType[] values = NotificationType.values();
		SelectItem list[] = new SelectItem[values.length + 1];
		list[0] = new SelectItem(null, "");

		int i = 1;
		for (NotificationType value : values)
		{
			list[i] = new SelectItem(value, value.getValue());
			i++;
		}

		return list;
	}

	public SelectItem[] getTimeZoneValues()
	{
		String[] timeZoneIds = TimeZone.getAvailableIDs();
		String filter = "^(Africa|America|Asia|Atlantic|Australia|Europe|Indian|Pacific)/.*";
		ArrayList<TimeZone> timeZones = new ArrayList<TimeZone>();

		for (String timeZoneId : timeZoneIds)
		{
			if (timeZoneId.matches(filter))
			{
				timeZones.add(TimeZone.getTimeZone(timeZoneId));
			}
		}

		Collections.sort(timeZones, new Comparator<TimeZone>()
		{
			public int compare(final TimeZone a, final TimeZone b)
			{
				return a.getID().compareTo(b.getID());
			}
		});

		SelectItem list[] = new SelectItem[timeZones.size() + 1];
		list[0] = new SelectItem(null, "Unspecified");

		int i = 1;
		for (TimeZone timeZone : timeZones)
		{
			list[i] = new SelectItem(timeZone.getID(), timeZone.getID().replace('_', ' ') + " - " + timeZone.getDisplayName());
			i++;
		}

		return list;
	}

	public String getUserRoot(User user)
	{
		if (user instanceof SuperAdministrator)
		{
			return "/super";
		}

		return user.getUserType().toLowerCase();
	}

	public SelectItem[] getUserStatusValues(boolean blank)
	{
		if (blank)
		{
			return getSelectItemsWithBlank(UserStatus.values());
		}
		return getSelectItems(UserStatus.values());
	}

	public SelectItem[] getYesNoValues()
	{
		SelectItem list[] = new SelectItem[3];
		list[0] = new SelectItem(null, "");
		list[1] = new SelectItem("true", "Yes");
		list[2] = new SelectItem("false", "No");
		return list;
	}

	public boolean isAccountUser(User user)
	{
		return RoleHelper.isAccountUser(user);
	}

	public boolean isInRoles(User user, String arg)
	{
		String[] arr = arg.split(",");
		RoleType[] roles = new RoleType[arr.length];

		for (int i = 0; i < arr.length; i++)
		{
			roles[i] = RoleType.valueOf(arr[i]);
		}

		return RoleHelper.isUserInRoles(user, roles);
	}

	public boolean isSuperAdministrator(User user)
	{
		return RoleHelper.isSuperAdministrator(user);
	}
}
