package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ComponentSystemEvent;

import com.aficionado.domain.Notification;
import com.aficionado.service.NotificationService;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.util.Filter;
import com.aficionado.util.FilterType;
import com.aficionado.util.SortOrder;
import com.aficionado.web.controller.exception.ControllerException;
import com.aficionado.web.controller.model.ListDataModel;

@ManagedBean
@ViewScoped
public class NotificationListController implements Serializable
{
	private static final Logger log = Logger.getLogger(NotificationListController.class.getName());

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{authController}")
	private AuthController authController;
	
	private ListDataModel<Notification> list;
	
	@EJB
	private NotificationService notificationService;

	private int size;
	
	@PreDestroy
	public void destroy()
	{
		list = null;
	}

	public ListDataModel<Notification> getList()
	{
		return list;
	}
	
	public int getSize()
	{
		if (list != null)
		{
			size = list.getRowCount();
		}

		return size;
	}

	public void init(ComponentSystemEvent event)
	{
		if (list != null)
		{
			return;
		}
		
		Collection<Filter> filters = new ArrayList<Filter>(3);
		filters.add(new Filter("type", FilterType.Equals));
		filters.add(new Filter("user.email", FilterType.Contains));
		filters.add(new Filter("fromUser.email", FilterType.Contains));
		
		list = new ListDataModel<Notification>("createdAt", SortOrder.Descending, filters)
		{
			private static final long serialVersionUID = 1L;

			@Override
			protected Long getCount(Collection<Filter> filters)
			{
				try
				{
					return notificationService.getCount(filters, authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}

			@Override
			protected Object getKey(Notification notification)
			{
				return notification.getId();
			}

			@Override
			protected List<Notification> getList(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
			{
				try
				{
					return notificationService.getAll(first, pageSize, sortField, sortOrder, filters, authController.getUser());
				}
				catch (ServiceException e)
				{
					throw new ControllerException(e);
				}
			}
		};

	}

	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}
}
