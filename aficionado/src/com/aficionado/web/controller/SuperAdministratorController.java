package com.aficionado.web.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.aficionado.domain.User;
import com.aficionado.service.SuperAdministratorService;
import com.aficionado.service.exception.ServiceException;

@ManagedBean
@SessionScoped
public class SuperAdministratorController extends UserController
{

	private static final long serialVersionUID = 1L;
	
	@EJB
	private SuperAdministratorService superAdministratorService;

	@Override
	protected User getById(Integer id) throws ServiceException
	{
		return superAdministratorService.get(id, authController.getUser());
	}

}
