package com.aficionado.web.controller;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.aficionado.service.MailService;
import com.aficionado.web.controller.exception.ControllerException;
import com.aficionado.web.controller.exception.ControllerExceptionType;

@ManagedBean
@RequestScoped
public class ErrorController implements Serializable
{
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{authController}")
	private AuthController authController;

	private Throwable exception;
	
	@EJB
	private MailService mailService;

	public Throwable getException()
	{
		return exception;
	}

	public void initError()
	{
		if (exception != null)
		{
			return;
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
		exception = (Throwable) requestMap.get("javax.servlet.error.exception");

		if (exception == null)
		{
			return;
		}

		if(exception.getCause() instanceof ControllerException)
		{
			exception = exception.getCause();
		}

		if (exception instanceof ControllerException)
		{
			if (((ControllerException) exception).getExceptionType() == ControllerExceptionType.System)
			{
				//mailService.sendError(exception);
				exception.printStackTrace();
			}
		}
		else
		{
			//mailService.sendError(exception);
			exception.printStackTrace();
			exception = null;
		}
	}

	public void setAuthController(AuthController authController)
	{
		this.authController = authController;
	}
}
