package com.aficionado.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.codehaus.jackson.map.ObjectMapper;

import com.aficionado.web.controller.model.ListItem;

public abstract class AbstractItemConverter<E> implements Converter
{
	private static final ObjectMapper mapper = new ObjectMapper();

	protected abstract ListItem getAsListItem(E value);

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value)
	{
		try
		{
			if (value == null || value.isEmpty())
			{
				return null;
			}

			return getAsObject(value);
		}
		catch (Exception e)
		{
			throw new ConverterException(e);
		}
	}

	protected abstract E getAsObject(String value);

	@SuppressWarnings(
		{ "unchecked" })
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value)
	{
		try
		{
			if (value == null)
			{
				return null;
			}

			return mapper.writeValueAsString(getAsListItem((E) value));
		}
		catch (Exception e)
		{
			throw new ConverterException(e);
		}
	}
}