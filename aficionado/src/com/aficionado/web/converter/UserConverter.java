package com.aficionado.web.converter;

import javax.faces.convert.FacesConverter;

import com.aficionado.domain.User;
import com.aficionado.web.controller.model.ListItem;

@FacesConverter(value = "userConverter")
public class UserConverter extends AbstractItemConverter<User>
{

	@Override
	protected ListItem getAsListItem(User value)
	{
		if(value.getId() == null)
		{
			return new ListItem(null, "");						
		}
		
		return new ListItem(value.getId().toString(), value.getFirstName());			
	}

	@Override
	protected User getAsObject(String value)
	{
		User user = new User();
		user.setId(Integer.valueOf(value, 10));
		return user;
	}
}
