package com.aficionado.web.converter;

import java.util.Collection;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.aficionado.domain.Domain;

@FacesConverter(value = "domainConverter")
public class DomainConverter implements Converter
{
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value)
	{
		Object obj = value;
		if(component.getChildren() != null && component.getChildren().size() > 0 && component.getChildren().get(0) instanceof UISelectItems)
		{
			Map<String, Object> attributes = component.getChildren().get(0).getAttributes();
			obj = attributes.get("value");
		}
		else
		{
			Map<String, Object> attributes = component.getAttributes();
			obj = attributes.get("value");
		}

		if (obj instanceof Collection)
		{
			for (Object val : (Collection) obj)
			{
				if (!(val instanceof Domain))
				{
					throw new IllegalArgumentException("The object is of not type Domain");
				}

				if (((Domain) val).getId() != null && value.equals(((Domain) val).getId().toString()))
				{
					return val;
				}
			}
		}

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value)
	{
		if (!(value instanceof Domain))
		{
			throw new IllegalArgumentException("The object is of not type Domain");
		}

		if(((Domain) value).getId() == null)
		{
			return "";
		}

		return ((Domain) value).getId().toString();
	}
}
