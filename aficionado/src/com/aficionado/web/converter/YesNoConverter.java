package com.aficionado.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "yesNoConverter")
public class YesNoConverter implements Converter
{
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value)
	{
		return value.equals("Yes")?Boolean.TRUE:Boolean.FALSE;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value)
	{
		if(value == null)
		{
			return "";
		}

		return (((Boolean)value) == true?"Yes":"No");
	}
}
