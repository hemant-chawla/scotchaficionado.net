package com.aficionado.web.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "integerListConverter")
public class IntegerListConverter implements Converter
{
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value)
	{
		try
		{
			if (value != null && !value.isEmpty())
			{
				String[] arr = value.split("\\s*(,|\\s)\\s*");
				List<Integer> objects = new ArrayList<Integer>(arr.length);
				for (String item : arr)
				{
					item = item.trim();
					if (!item.isEmpty())
					{
						objects.add(Integer.parseInt(item));
					}
				}
				return objects;
			}
		}
		catch (Exception e)
		{
			throw new ConverterException(e);
		}
		return null;
	}

	@SuppressWarnings(
		{ "unchecked" })
	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value)
	{
		if (value == null)
		{
			return "";
		}

		if (!(value instanceof List<?>))
		{
			throw new IllegalArgumentException("The object is not a list");
		}

		List<String> values = (List<String>) value;
		if (values.size() <= 0)
		{
			return "";
		}

		StringBuilder builder = new StringBuilder();
		for (String val : values)
		{
			if (val.isEmpty())
			{
				continue;
			}

			if (builder.length() > 0)
			{
				builder.append(", ");
			}

			builder.append(val);
		}

		return builder.toString();
	}

}
