package com.aficionado.web.validator;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("dateValidator")
public class DateValidator implements Validator
{
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException
	{
		if (value == null || value.toString().isEmpty())
		{
			return;
		}
		
		Date date = (Date) value;
		Date now = new Date();
		
		if (date.after(now))
		{
			FacesMessage msg = new FacesMessage("Invalid Date", "Date cannot be greater than today.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
