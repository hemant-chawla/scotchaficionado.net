package com.aficionado.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.myfaces.custom.fileupload.UploadedFile;

@FacesValidator(value = "fileValidator")
public class FileValidator implements Validator
{
	//private static final long MAX_FILE_SIZE = 10485760L; // 10MB.
	
	private static final long MAX_FILE_SIZE = 2097152L; //2MB

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException
	{
		UploadedFile file = (UploadedFile) value;
		if (file != null)
		{
			if (file.getSize() > MAX_FILE_SIZE)
			{
				throw new ValidatorException(new FacesMessage(String.format("File exceeds maximum permitted size of %d bytes.", MAX_FILE_SIZE)));
			}
			System.out.println("Content Type: "+file.getContentType());
			if (!"application/pdf".equals(file.getContentType()))
			{
				FacesMessage message = new FacesMessage("Error: File type is invalid. Only PDF documents allowed.");
				throw new ValidatorException(message);
			}

		}
	}
}
