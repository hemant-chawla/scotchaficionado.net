package com.aficionado.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


@FacesValidator("selectOneMenuValidator")
public class SelectOneMenuValidator implements Validator
{
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value) throws ValidatorException
	{
		if (value == null || value.toString().isEmpty())
		{
			FacesMessage message = new FacesMessage();  
            message.setSeverity(FacesMessage.SEVERITY_ERROR);  
            message.setSummary("Option not selected.");  
            message.setDetail("Please select an option from the dropdown.");              
           
            throw new ValidatorException(message); 
		}
		
	}

}
