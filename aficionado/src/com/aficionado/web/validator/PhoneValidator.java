package com.aficionado.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("phoneValidator")
public class PhoneValidator implements Validator
{
	private static final String PHONE_PATTERN = "^(\\+?[0-9\\- \\(\\)]*$)";

	private Matcher matcher;
	private Pattern pattern;

	public PhoneValidator()
	{
		pattern = Pattern.compile(PHONE_PATTERN);
	}

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException
	{

		if (value == null || value.toString().isEmpty())
		{
			return;
		}

		matcher = pattern.matcher(value.toString());
		if (!matcher.matches())
		{
			FacesMessage msg = new FacesMessage("Phone validation failed.", "Invalid phone format.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
