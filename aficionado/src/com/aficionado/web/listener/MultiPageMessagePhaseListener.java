package com.aficionado.web.listener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class MultiPageMessagePhaseListener implements PhaseListener
{
	private static final long serialVersionUID = 1L;
	private static final String token = "com.capexil.web.listener.SAVED_FACES_MESSAGES";

	@Override
	public void afterPhase(final PhaseEvent event)
	{
		if (!PhaseId.RENDER_RESPONSE.equals(event.getPhaseId()))
		{
			FacesContext facesContext = event.getFacesContext();
			saveMessages(facesContext, facesContext.getExternalContext().getSessionMap());
		}
	}

	@Override
	public void beforePhase(final PhaseEvent event)
	{
		FacesContext facesContext = event.getFacesContext();
		saveMessages(facesContext, facesContext.getExternalContext().getSessionMap());

		if (PhaseId.RENDER_RESPONSE.equals(event.getPhaseId()))
		{
			if (!facesContext.getResponseComplete())
			{
				restoreMessages(facesContext, facesContext.getExternalContext().getSessionMap());
			}
		}
	}

	@Override
	public PhaseId getPhaseId()
	{
		return PhaseId.ANY_PHASE;
	}

	@SuppressWarnings("unchecked")
	private int restoreMessages(final FacesContext facesContext, final Map<String, Object> source)
	{
		int restoredCount = 0;
		if (FacesContext.getCurrentInstance() != null)
		{
			List<FacesMessage> messages = (List<FacesMessage>) source.remove(token);

			if (messages == null)
			{
				return 0;
			}

			restoredCount = messages.size();
			for (Object element : messages)
			{
				facesContext.addMessage(null, (FacesMessage) element);
			}
		}
		return restoredCount;
	}

	@SuppressWarnings("unchecked")
	private int saveMessages(final FacesContext facesContext, final Map<String, Object> destination)
	{
		int restoredCount = 0;
		if (FacesContext.getCurrentInstance() != null)
		{
			List<FacesMessage> messages = new ArrayList<FacesMessage>();
			for (Iterator<FacesMessage> iter = facesContext.getMessages(null); iter.hasNext();)
			{
				messages.add(iter.next());
				iter.remove();
			}

			if (messages.size() > 0)
			{
				List<FacesMessage> existingMessages = (List<FacesMessage>) destination.get(token);
				if (existingMessages != null)
				{
					existingMessages.addAll(messages);
				}
				else
				{
					destination.put(token, messages);
				}
				restoredCount = messages.size();
			}
		}
		return restoredCount;
	}

}
