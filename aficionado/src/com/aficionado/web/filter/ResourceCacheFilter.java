package com.aficionado.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class ResourceCacheFilter implements Filter
{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		HttpServletResponse servletReponse = (HttpServletResponse) response;
		if(servletReponse.containsHeader("Cache-Control"))
		{
			servletReponse.setHeader("Cache-Control", "public, max-age=8640000");
		}

		if(servletReponse.containsHeader("Pragma"))
		{
			servletReponse.setHeader("Pragma", "");
		}

		chain.doFilter(request, servletReponse);
	}

	@Override
	public void destroy()
	{
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
	}
}
