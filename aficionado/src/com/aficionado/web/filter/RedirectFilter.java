package com.aficionado.web.filter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aficionado.util.Constants;


public class RedirectFilter implements Filter
{

	private static final Logger log = Logger.getLogger(RedirectFilter.class.getName());
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		HttpServletResponse servletReponse = (HttpServletResponse) response;
		HttpServletRequest servletRequest = (HttpServletRequest)request;
		if(servletRequest.getMethod().equals("GET"))
		{
			boolean redirect = false;
			String server = Constants.BASE_APPLICATION_URL;

			String uri = servletRequest.getRequestURI().toLowerCase();
			log.log(Level.SEVERE, "**(A)** com.capexil.web.filter.RedirectFilter,  URI : "+uri);
			if(!servletRequest.isSecure())
			{
				if(uri.startsWith("/api/"))
				{
					redirect = false;
				}
				else
				{
					redirect = false;
				}
			}
			else
			{
				if(servletRequest.getRequestURL().toString().toLowerCase().startsWith("https://www."))
				{
					redirect = true;					
				}				
			}

			if(uri.startsWith("/membership/") && !uri.contains("/faces/"))
			{
				//uri = uri.substring(24);
				log.log(Level.SEVERE, "**(B)** com.capexil.web.filter.RedirectFilter,  URI : "+uri);
				redirect = true;				
			}
			
			if(redirect)
			{
				log.log(Level.SEVERE, "**(C)** com.capexil.web.filter.RedirectFilter, : LOCATION[SERVER+URI] : "+server + uri);
				
				servletReponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
				servletReponse.setHeader("Location", server + uri);
				return;
			}
		}
	
		chain.doFilter(request, servletReponse);
	}

	@Override
	public void destroy()
	{
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException
	{
	}
}
