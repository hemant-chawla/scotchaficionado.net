package com.aficionado.web.model;

import java.io.Serializable;
import java.util.List;

public class Email implements Serializable
{
	private static final long serialVersionUID = 1L;

	private List<String> bcc;

	private List<String> cc;

	private String from;

	private String message;

	private String subject;

	private List<String> to;

	public Email()
	{
	}

	public Email(String from, List<String> to, List<String> cc, List<String> bcc)
	{
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
	}

	public Email(String from, List<String> to, List<String> cc, List<String> bcc, String subject, String message)
	{
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.subject = subject;
		this.message = message;
	}

	public List<String> getBcc()
	{
		return bcc;
	}

	public List<String> getCc()
	{
		return cc;
	}

	public String getFrom()
	{
		return from;
	}

	public String getMessage()
	{
		return message;
	}

	public String getSubject()
	{
		return subject;
	}

	public List<String> getTo()
	{
		return to;
	}

	public void setBcc(List<String> bcc)
	{
		this.bcc = bcc;
	}

	public void setCc(List<String> cc)
	{
		this.cc = cc;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public void setTo(List<String> to)
	{
		this.to = to;
	}

}
