package com.aficionado.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.Role;

/**
 * The Class RoleDao manages Role entity.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class RoleDao extends GenericDao<Integer, Role>
{
	private static final long serialVersionUID = 1L;
}
