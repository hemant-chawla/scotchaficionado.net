package com.aficionado.dao;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.Notification;
import com.aficionado.domain.User;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class NotificationDao extends GenericDao<Integer, Notification>
{

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 */
	public List<Notification> findAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
	{
		String query = "Select n from Notification n";

		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "n", query, null);
	}
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 */
	public List<Notification> findAllApproved(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
	{
		String query = "Select n from Notification n where n.type = com.capexil.domain.type.NotificationType.Annexure2Approved or n.type = com.capexil.domain.type.NotificationType.DeclarationApproved or n.type = com.capexil.domain.type.NotificationType.HCApproved or n.type = com.capexil.domain.type.NotificationType.PACApproved or n.type = com.capexil.domain.type.NotificationType.SCCApproved";

		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "n", query, null);
	}


	/**
	 * 
	 * @return
	 */
	public List<Notification> findByLastSevenDays()
	{
		// Calculate the date one week back
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -7);

		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("date", calendar.getTime());
		
		return findByNamedQuery("Notification.findByLastSevenDays", params);
	}

	/**
	 * Find by user.
	 * 
	 * @param first
	 *            the first
	 * @param pageSize
	 *            the page size
	 * @param sortField
	 *            the sort field
	 * @param sortOrder
	 *            the sort order
	 * @param filters
	 *            the filters
	 * @param rowCount
	 *            the row count
	 * @param user
	 *            the user
	 * @return the list of notification
	 */
	public List<Notification> findByUser(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User user)
	{
		String query = "Select n from Notification n where n.user = :user";
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("user", user);

		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "n", query, params);
	}

	/**
	 * 
	 * @param filters
	 * @return
	 */
	public Long getCount(Collection<Filter> filters)
	{
		return getCount(filters, "n", "Select n from Notification n", null);
	}
	
	/**
	 * 
	 * @param filters
	 * @param user
	 * @return
	 */
	public Long getCount(Collection<Filter> filters, User user)
	{
		String query = "Select n from Notification n where n.user = :user";
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("user", user);

		return getCount(filters, "n", query, params);
	}

	public Long getCountApproved(Collection<Filter> filters)
	{
		String query = "Select n from Notification n where n.type = com.capexil.domain.type.NotificationType.Annexure2Approved or n.type = com.capexil.domain.type.NotificationType.DeclarationApproved or n.type = com.capexil.domain.type.NotificationType.HCApproved or n.type = com.capexil.domain.type.NotificationType.PACApproved or n.type = com.capexil.domain.type.NotificationType.SCCApproved";
		
		return getCount(filters, "n", query, null);
	}
}
