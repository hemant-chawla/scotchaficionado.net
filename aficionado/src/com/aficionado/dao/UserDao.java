package com.aficionado.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.User;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

/**
 * The Class UserDao manages User entity.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class UserDao extends GenericDao<Integer, User>
{

	private static final long serialVersionUID = 1L;

	/**
	 * Count Users by email.
	 * 
	 * @param email the email
	 * @param user the user
	 * @return the long the count
	 */
	public Long countByEmail(String email, User user)
	{
		Map<String, Object> params = new HashMap<String, Object>(2);
		params.put("email", email);
		params.put("user", user == null || user.getId() == null ? 0 : user.getId());

		@SuppressWarnings("unchecked")
		List<Long> result = findByNamedQueryRaw("User.countByEmail", params);
		return result.get(0);
	}
	
	/**
	 * 
	 * @param firstName
	 * @param exporter
	 * @return
	 */
	public Long countByFirstAndLastName(String firstName, String lastName, User user)
	{
		Map<String, Object> params = new HashMap<String, Object>(3);
		params.put("firstName", firstName);
		params.put("lastName", lastName);
		params.put("user", user == null || user.getId() == null ? 0 : user.getId());

		@SuppressWarnings("unchecked")
		List<Long> result = findByNamedQueryRaw("User.countByFirstAndLastName", params);
		return result.get(0);
	}
	
	/**
	 * Find all Users.
	 * 
	 * @param first the first
	 * @param pageSize the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters the filters
	 * @param rowCount the row count
	 * @return the list of Users
	 */
	public List<User> findAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
	{
		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "u", "Select u from User u where u.role is not null", null);
	}

	/**
	 * Find User by email & status enabled.
	 * 
	 * @param email the email
	 * @return the user
	 */
	public User findByEmailEnabled(String email)
	{
		List<User> users = findByNamedQuery("User.findByEmailEnabled", "email", email);
		if (users.size() == 0)
		{
			return null;
		}

		return users.get(0);
	}

	/**
	 * 
	 * @param filters
	 * @return
	 */
	public Long getCount(Collection<Filter> filters)
	{
		return getCount(filters, "u", "Select u from User u where u.role is not null", null);
	}
}
