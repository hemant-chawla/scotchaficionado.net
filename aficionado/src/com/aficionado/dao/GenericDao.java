package com.aficionado.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aficionado.domain.Domain;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

/**
 * The Class GenericDao is a generic class manage the entities, using the entity
 * manager instance associated with a persistence context.
 * 
 * @param <K> the key type
 * @param <E> the element type
 */
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class GenericDao<K, E extends Domain<?>> implements Serializable
{

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	protected EntityManager entityManager;

	/**
	 * Instantiates a new generic dao.
	 */
	public GenericDao()
	{
	}

	private Query buildQuery(String sortField, SortOrder sortOrder, Collection<Filter> filters, String className,
			String baseQuery, Map<String, Object> additionalParams)
	{
		StringBuilder query = new StringBuilder();
		query.append(baseQuery);

		if (filters != null)
		{
			boolean where = baseQuery.toLowerCase().indexOf("where") > -1;

			for (Filter filter : filters)
			{
				if (filter.getValue() == null || filter.getValue() == "")
				{
					continue;
				}

				if (!where)
				{
					query.append(" where ");
					where = true;
				}
				else
				{
					query.append(" and ");
				}
				query.append(className);
				query.append(".");
				query.append(filter.getName());

				switch (filter.getType())
				{
				case Contains:
				case StartsWith:
					query.append(" like :");
					break;
				case Equals:
					query.append(" = :");
					break;
				case NotEquals:
					query.append(" != :");
					break;
				case In:
					query.append(" in :");
					break;
				case NotIn:
					query.append(" not in :");
					break;
				}

				query.append(filter.getName().replace('.', '_'));
			}
		}

		if (sortField != null && sortOrder != SortOrder.Unsorted)
		{
			query.append(" order by ").append(className).append(".");
			query.append(sortField);
			query.append(sortOrder == SortOrder.Ascending ? " asc " : " desc ");
		}

		Query jpaQuery = entityManager.createQuery(query.toString());

		if (filters != null)
		{
			for (Filter filter : filters)
			{
				if (filter.getValue() == null || filter.getValue() == "")
				{
					continue;
				}

				String name = filter.getName().replace('.', '_');

				switch (filter.getType())
				{
				case Contains:
					jpaQuery.setParameter(name, "%" + filter.getValue() + "%");
					break;
				case StartsWith:
					jpaQuery.setParameter(name, filter.getValue() + "%");
					break;
				case Equals:
				case NotEquals:
				case In:
				case NotIn:
					jpaQuery.setParameter(name, filter.getValue());
					break;
				}
			}
		}

		if (additionalParams != null)
		{
			for (Entry<String, Object> param : additionalParams.entrySet())
			{
				jpaQuery.setParameter(param.getKey(), param.getValue());
			}
		}

		return jpaQuery;
	}

	/**
	 * Create an instance of Query for executing a native SQL statement, e.g.,
	 * for update or delete
	 * 
	 * @param query the query
	 * @return the int
	 */
	public int executeUpdateNative(String query)
	{
		Query jpaQuery = entityManager.createNativeQuery(query);
		return jpaQuery.executeUpdate();
	}

	/**
	 * Create an instance of Query for executing a native SQL statement, e.g.,
	 * for update or delete
	 * 
	 * @param query the query
	 * @param params the params
	 * @return the int
	 */
	public int executeUpdateNative(String query, List<Object> params)
	{
		Query jpaQuery = entityManager.createNativeQuery(query);
		for (int i = 0; i < params.size(); i++)
		{
			jpaQuery.setParameter(i + 1, params.get(i));
		}
		return jpaQuery.executeUpdate();
	}

	/**
	 * Find by id.
	 * 
	 * @param id the id
	 * @param klass the klass
	 * @return the e
	 */
	public E findById(K id, Class<E> klass)
	{
		return entityManager.find(klass, id); // class is keyword
	}

	/**
	 * Find by named query.
	 * 
	 * @param name the name
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findByNamedQuery(String name)
	{
		Query query = entityManager.createNamedQuery(name);
		return query.getResultList();
	}

	/**
	 * Find by named query.
	 * 
	 * @param name the name
	 * @param first the first
	 * @param pageSize the page size
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findByNamedQuery(String name, int first, int pageSize)
	{
		Query query = entityManager.createNamedQuery(name);
		query.setFirstResult(first);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	/**
	 * Find by named query.
	 * 
	 * @param name the name
	 * @param params the params
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findByNamedQuery(String name, Map<String, Object> params)
	{
		Query query = entityManager.createNamedQuery(name);

		for (Entry<String, Object> param : params.entrySet())
		{
			query.setParameter(param.getKey(), param.getValue());
		}

		return query.getResultList();
	}

	/**
	 * Find by named query.
	 * 
	 * @param name the name
	 * @param params the params
	 * @param first the first
	 * @param pageSize the page size
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findByNamedQuery(String name, Map<String, Object> params, int first, int pageSize)
	{
		Query query = entityManager.createNamedQuery(name);

		for (Entry<String, Object> param : params.entrySet())
		{
			query.setParameter(param.getKey(), param.getValue());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		return query.getResultList();
	}

	/**
	 * Find by named query.
	 * 
	 * @param name the name
	 * @param param the param
	 * @param value the value
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findByNamedQuery(String name, String param, Object value)
	{
		Query query = entityManager.createNamedQuery(name);
		query.setParameter(param, value);
		return query.getResultList();
	}

	/**
	 * Find by named query.
	 * 
	 * @param name the name
	 * @param param the param
	 * @param value the value
	 * @param first the first
	 * @param pageSize the page size
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findByNamedQuery(String name, String param, Object value, int first, int pageSize)
	{
		Query query = entityManager.createNamedQuery(name);
		query.setParameter(param, value);

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		return query.getResultList();
	}

	/**
	 * Find by named query raw.
	 * 
	 * @param name the name
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findByNamedQueryRaw(String name)
	{
		Query query = entityManager.createNamedQuery(name);
		return query.getResultList();
	}

	/**
	 * Find by named query raw.
	 * 
	 * @param name the name
	 * @param first the first
	 * @param pageSize the page size
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findByNamedQueryRaw(String name, int first, int pageSize)
	{
		Query query = entityManager.createNamedQuery(name);

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		return query.getResultList();
	}

	/**
	 * Find by named query raw.
	 * 
	 * @param name the name
	 * @param params the params
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findByNamedQueryRaw(String name, Map<String, Object> params)
	{
		Query query = entityManager.createNamedQuery(name);

		for (Entry<String, Object> param : params.entrySet())
		{
			query.setParameter(param.getKey(), param.getValue());
		}

		return query.getResultList();
	}

	/**
	 * Find by named query raw.
	 * 
	 * @param name the name
	 * @param params the params
	 * @param first the first
	 * @param pageSize the page size
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findByNamedQueryRaw(String name, Map<String, Object> params, int first, int pageSize)
	{
		Query query = entityManager.createNamedQuery(name);

		for (Entry<String, Object> param : params.entrySet())
		{
			query.setParameter(param.getKey(), param.getValue());
		}

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		return query.getResultList();
	}

	/**
	 * Find by named query raw.
	 * 
	 * @param name the name
	 * @param param the param
	 * @param value the value
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findByNamedQueryRaw(String name, String param, Object value)
	{
		Query query = entityManager.createNamedQuery(name);
		query.setParameter(param, value);
		return query.getResultList();
	}

	/**
	 * Find by named query raw.
	 * 
	 * @param name the name
	 * @param param the param
	 * @param value the value
	 * @param first the first
	 * @param pageSize the page size
	 * @return the list
	 */
	@SuppressWarnings("rawtypes")
	public List findByNamedQueryRaw(String name, String param, Object value, int first, int pageSize)
	{
		Query query = entityManager.createNamedQuery(name);
		query.setParameter(param, value);

		query.setFirstResult(first);
		query.setMaxResults(pageSize);

		return query.getResultList();
	}

	/**
	 * Find by native query.
	 * 
	 * @param qry the qry
	 * @return the list<? extends object>
	 */
	public List<? extends Object> findByNativeQuery(String qry)
	{
		return findByNativeQuery(qry, null);
	}

	/**
	 * Find by native query.
	 * 
	 * @param qry the qry
	 * @param params the params
	 * @return the list<? extends object>
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Object> findByNativeQuery(String qry, List<Object> params)
	{
		Query query = entityManager.createNativeQuery(qry);
		if (params != null && params.size() > 0)
		{
			for (int i = 0; i < params.size(); i++)
			{
				query.setParameter(i + 1, params.get(i));
			}
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<E> findByQuery(String query, Map<String, Object> params)
	{
		Query jpaQuery = entityManager.createQuery(query);

		for (Entry<String, Object> param : params.entrySet())
		{
			jpaQuery.setParameter(param.getKey(), param.getValue());
		}

		return jpaQuery.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<E> findByQuery(String query, Map<String, Object> params, int first, int pageSize)
	{
		Query jpaQuery = entityManager.createQuery(query);
		jpaQuery.setFirstResult(first);
		jpaQuery.setMaxResults(pageSize);

		for (Entry<String, Object> param : params.entrySet())
		{
			jpaQuery.setParameter(param.getKey(), param.getValue());
		}

		return jpaQuery.getResultList();
	}

	@SuppressWarnings("rawtypes")
	public List findByQueryRaw(String query, Map<String, Object> params)
	{
		Query jpaQuery = entityManager.createQuery(query);

		for (Entry<String, Object> param : params.entrySet())
		{
			jpaQuery.setParameter(param.getKey(), param.getValue());
		}

		return jpaQuery.getResultList();
	}

	@SuppressWarnings("rawtypes")
	public List findByQueryRaw(String query, Map<String, Object> params, int first, int pageSize)
	{
		Query jpaQuery = entityManager.createQuery(query);
		jpaQuery.setFirstResult(first);
		jpaQuery.setMaxResults(pageSize);

		for (Entry<String, Object> param : params.entrySet())
		{
			jpaQuery.setParameter(param.getKey(), param.getValue());
		}

		return jpaQuery.getResultList();
	}

	/**
	 * Find by sort and filter.
	 * 
	 * @param first the first
	 * @param pageSize the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters the filters
	 * @param className the class name
	 * @param baseQuery the base query
	 * @param additionalParams the additional params
	 * @param countQuery the count query
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<E> findBySortAndFilter(int first, int pageSize, String sortField, SortOrder sortOrder,
			Collection<Filter> filters, String className, String baseQuery, Map<String, Object> additionalParams)
	{
		Query jpaQuery = buildQuery(sortField, sortOrder, filters, className, baseQuery, additionalParams);
		jpaQuery.setFirstResult(first);
		jpaQuery.setMaxResults(pageSize);
		return jpaQuery.getResultList();
	}

	/**
	 * Synchronize the persistence context to the underlying database.
	 */
	public void flush()
	{
		entityManager.flush();
	}

	public Long getCount(Collection<Filter> filters, String className, String baseQuery,
			Map<String, Object> additionalParams)
	{
		StringBuilder query = new StringBuilder();
		int baseQueryFromIndex = baseQuery.toLowerCase().indexOf("from");
		query.append("Select count(distinct ");
		query.append(className);
		query.append(" ) ");
		query.append(baseQuery.substring(baseQueryFromIndex));

		Query jpaQuery = buildQuery(null, null, filters, className, query.toString(), additionalParams);
		return (Long) jpaQuery.getSingleResult();
	}

	/**
	 * Merge the state of the given entity into the current persistence context.
	 * 
	 * @param entity the entity
	 * @return the e
	 */
	public E merge(E entity)
	{
		return entityManager.merge(entity);
	}

	/**
	 * Make an entity instance managed and persistent.
	 * 
	 * @param entity the entity
	 */
	public void persist(E entity)
	{
		entityManager.persist(entity);
	}

	/**
	 * Refresh the state of the instance from the database, overwriting changes
	 * made to the entity, if any.
	 * 
	 * @param entity the entity
	 */
	public void refresh(E entity)
	{
		entityManager.refresh(entity);
	}

	/**
	 * Removes the entity instance.
	 * 
	 * @param entity the entity
	 */
	public void remove(E entity)
	{
		entityManager.remove(entity);
	}

}