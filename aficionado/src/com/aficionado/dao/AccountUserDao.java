package com.aficionado.dao;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.AccountUser;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AccountUserDao extends GenericDao<Integer, AccountUser>
{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 */
	public List<AccountUser> findAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
	{
		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "a", "Select a from AccountUser a", null);
	}

	/**
	 * 
	 * @param filters
	 * @return
	 */
	public Long getCount(Collection<Filter> filters)
	{
		return getCount(filters, "a", "Select a from AccountUser a", null);
	}

}
