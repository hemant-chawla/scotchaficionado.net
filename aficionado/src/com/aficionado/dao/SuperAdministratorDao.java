package com.aficionado.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.SuperAdministrator;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class SuperAdministratorDao extends GenericDao<Integer, SuperAdministrator>
{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @return
	 */
	public List<SuperAdministrator> findAll()
	{
		return findByNamedQuery("SuperAdministrator.findAll");
	}

}
