package com.aficionado.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.Setting;
import com.aficionado.domain.type.SettingType;

/**
 * The Class SettingDao manages Setting entity.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class SettingDao extends GenericDao<String, Setting>
{
	
	private static final long serialVersionUID = 1L;

	/**
	 * Find all Settings.
	 *
	 * @return the list
	 */
	public List<Setting> findAll()
	{
		return findByNamedQuery("Setting.findAll");
	}

	/**
	 * Find Setting by the setting type.
	 *
	 * @param type the type
	 * @return the setting
	 */
	public Setting findBySettingType(SettingType type)
	{
		return findById(type.name(), Setting.class);
	}

}