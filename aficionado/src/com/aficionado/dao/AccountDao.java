package com.aficionado.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.Account;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AccountDao extends GenericDao<Integer, Account>
{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Count Account by email.
	 * 
	 * @param email the email
	 * @param user the user
	 * @return the long the count
	 */
	public Long countByEmail(String email, Account account)
	{
		Map<String, Object> params = new HashMap<String, Object>(2);
		params.put("email", email);
		params.put("account", account == null || account.getId() == null ? 0 : account.getId());

		@SuppressWarnings("unchecked")
		List<Long> result = findByNamedQueryRaw("Account.countByEmail", params);
		return result.get(0);
	}

	/**
	 * Count Account by short code.
	 * 
	 * @param shortCode the short code
	 * @param account the account
	 * @return the long the count
	 */
	public Long countByShortCode(String shortCode, Account account)
	{
		Map<String, Object> params = new HashMap<String, Object>(2);
		params.put("shortCode", shortCode);
		params.put("account", account == null || account.getId() == null ? 0 : account.getId());

		@SuppressWarnings("unchecked")
		List<Long> result = findByNamedQueryRaw("Account.countByShortCode", params);
		return result.get(0);
	}

	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 */
	public List<Account> findAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
	{
		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "a", "Select a from Account a", null);
	}

	/**
	 * 
	 * @param filters
	 * @return
	 */
	public Long getCount(Collection<Filter> filters)
	{
		return getCount(filters, "a", "Select a from Account a", null);
	}

}
