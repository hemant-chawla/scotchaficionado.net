package com.aficionado.dao;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.Log;
import com.aficionado.domain.User;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;


/**
 * The Class LogDao manages Log entity.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class LogDao extends GenericDao<Integer, Log>
{
	private static final long serialVersionUID = 1L;

	/**
	 * Find all Logs.
	 *
	 * @param first the first
	 * @param pageSize the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters the filters
	 * @param rowCount the row count
	 * @return the list of Logs
	 */
	public List<Log> findAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters)
	{
		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "l", "Select l from Log l", null);
	}
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param entityId
	 * @return
	 */
	public List<Log> findAllByEntityId(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, int entityId)
	{
		String query = "Select l from Log l where l.entityId = :entityId";
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("entityId", entityId);
		
		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "l", query, params);
	}
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param user
	 * @return
	 */
	public List<Log> findAllByUser(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User user)
	{
		String query = "Select l from Log l where l.user = :user";
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("user", user);
		
		return findBySortAndFilter(first, pageSize, sortField, sortOrder, filters, "l", query, params);
	}

	
	/**
	 * Find logs of last three days. 
	 * 
	 * @return list of logs
	 */
	public List<Log> findByLastThreeDays()
	{
		// Calculate the date three days back
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -3);

		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("date", calendar.getTime());
		
		return findByNamedQuery("Log.findByLastThreeDays", params);
	}

	/**
	 * 
	 * @param filters
	 * @return
	 */
	public Long getCount(Collection<Filter> filters)
	{
		return getCount(filters, "l", "Select l from Log l", null);
	}
	
	/**
	 * 
	 * @param filters
	 * @param entityId
	 * @return
	 */
	public Long getCount(Collection<Filter> filters, int entityId)
	{
		String query = "Select l from Log l where l.entityId = :entityId";
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("entityId", entityId);
		
		return getCount(filters, "l", query, params);
	}
	
	/**
	 * 
	 * @param filters
	 * @param user
	 * @return
	 */
	public Long getCount(Collection<Filter> filters, User user)
	{
		String query = "Select l from Log l where l.user = :user";
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("user", user);
		
		return getCount(filters, "l", query, params);
	}
}
