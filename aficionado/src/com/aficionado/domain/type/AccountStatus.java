package com.aficionado.domain.type;

public enum AccountStatus
{
	Active("Active"), Closed("Joined"), Confirmed("Confirmed"), Created("Created"), Inactive("Inactive");

	private final String value;

	AccountStatus(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
