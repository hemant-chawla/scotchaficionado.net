package com.aficionado.domain.type;

public enum AccountCategory
{
	Patron("Patron"),
	Friend("Friend"),
	Prospect("Prospect"),
	None("None");
	
	private final String value;

	AccountCategory(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
