package com.aficionado.domain.type;

public enum LogType
{
	Created("Created"), Modified("Modified");
	
	private final String value;

	LogType(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
