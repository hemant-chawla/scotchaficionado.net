package com.aficionado.domain.type;

public enum RoleType
{
	SuperAdministrator(0), AccountUser(1);

	private int id;

	RoleType(int id)
	{
		this.id = id;
	}

	public int getId()
	{
		return id;
	}
}