package com.aficionado.domain.type;

public enum UserStatus
{
	Enabled, Disabled;
}
