package com.aficionado.domain.type;

public enum NotificationType
{
	PasswordChanged("Password Changed"),
	AccountCreated("Account Created");
	
	private final String value;

	NotificationType(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

}
