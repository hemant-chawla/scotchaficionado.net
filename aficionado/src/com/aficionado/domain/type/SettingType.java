package com.aficionado.domain.type;

public enum SettingType
{
	AdditionalPanelFee, EnableNetBanking, EnableAutomatedEmailSending, LargeScaleAdmissionFee, LargeScaleAnnualFee, ManufacturerCumMerchantExporterFee, MembershipSeries, ReferenceSeries, SmallScaleAdmissionFee, SmallScaleAnnualFee;
}