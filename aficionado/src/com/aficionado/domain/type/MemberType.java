package com.aficionado.domain.type;

public enum MemberType
{
	ManufacturerCumMerchantExporter("Manufacturer cum Merchant Exporter"),
	ManufacturerExporter("Manufacturer Exporter"),
	MerchantExporter("Merchant Exporter");
	
	private final String value;

	MemberType(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
