package com.aficionado.domain.type;

public enum EntityType
{
	Account("Account"),
	User("User");

	private final String value;

	EntityType(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
