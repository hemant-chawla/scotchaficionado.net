package com.aficionado.domain.type;

public enum SettingDataType
{
	Date, Integer, Double, DateTime, String, Boolean;
}
