package com.aficionado.domain.type;

public enum NotificationFrequency
{
	Instant ("Yes, instantly"),
	Daily ("Yes, as a daily aggregate"),
	Weekly ("Yes, as a weekly aggregate"),
	None("No, never email me");

	private final String value;

	NotificationFrequency(String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
