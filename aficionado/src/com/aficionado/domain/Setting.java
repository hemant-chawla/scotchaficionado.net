package com.aficionado.domain;

import java.text.DateFormat;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.aficionado.domain.type.SettingDataType;

/**
 * The persistent class for the setting database table.
 * 
 */

@NamedQueries(
{ @NamedQuery(name = "Setting.findAll", query = "Select s from Setting s") })
@Entity
@Cacheable
@Table(name = "setting")
public class Setting extends Domain<String>
{
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Enumerated(EnumType.STRING)
	private SettingDataType type;

	private String value;

	private transient Object objValue;

	public Setting()
	{
	}

	@Override
	public String getId()
	{
		return this.id;
	}

	public SettingDataType getType()
	{
		return type;
	}

	public Object getValue()
	{
		if (objValue != null)
		{
			return objValue;
		}

		switch (type)
		{
			case Integer:
				objValue = new Integer(value);
				break;
			case Date:
				DateFormat dateformat = DateFormat.getDateInstance();
				try
				{
					objValue = dateformat.parse(value);
				}
				catch (Exception e)
				{
					objValue = null;
				}
				break;
			case DateTime:
				DateFormat dateTimeformat = DateFormat.getDateTimeInstance();
				try
				{
					objValue = dateTimeformat.parse(value);
				}
				catch (Exception e)
				{
					objValue = null;
				}
				break;				
			case Double:
				objValue = Double.parseDouble(value);
				break;
			case String:
				objValue = value;
				break;	
			case Boolean:
				objValue = Boolean.parseBoolean(value);
				break;
			default:
				objValue = value;
		}

		return objValue;
	}

	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	public void setType(SettingDataType type)
	{
		this.type = type;
	}

	public void setValue(Object value)
	{
		if (!value.equals(this.objValue))
		{
			if (type == SettingDataType.Date)
			{
				DateFormat format = DateFormat.getDateInstance();
				this.value = format.format(value);
			}
			else if(type == SettingDataType.DateTime)
			{
				DateFormat format = DateFormat.getDateTimeInstance();
				this.value = format.format(value);
			}
			else
			{
				this.value = value.toString();
			}

			this.objValue = value;
		}
	}

}