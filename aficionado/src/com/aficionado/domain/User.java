package com.aficionado.domain;

import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.aficionado.domain.type.UserStatus;

/**
 * The persistent class for the user database table.
 * 
 */
@NamedQueries(
{ @NamedQuery(name = "User.findByEmailEnabled", query = "Select u from User u where u.email = :email and u.status = com.aficionado.domain.type.UserStatus.Enabled"),
		@NamedQuery(name = "User.countByEmail", query = "Select count(u.id) from User u where u.id <> :user and u.email = :email")
})

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Cacheable
@Table(name = "user")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "user_type", discriminatorType = DiscriminatorType.STRING)
public class User extends Domain<Integer>
{
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "first_name")
	private String firstName;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login")
	private Date lastLogin;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "password")
	private String password;
	
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private UserStatus status;

	@Transient
	private TimeZone timeZone;

	@Column(name = "time_zone")
	private String timeZoneId;
	
	@Column(name = "user_type")
	private String userType;
	
	public User()
	{
	}
	
	public Account getAccount()
	{
		return account;
	}
	
	public Date getCreatedAt()
	{
		return createdAt;
	}

	public String getEmail()
	{
		return this.email;
	}

	public String getFirstName()
	{
		return firstName;
	}

	@Override
	public Integer getId()
	{
		return this.id;
	}

	public Date getLastLogin()
	{
		return this.lastLogin;
	}
	
	public String getLastName()
	{
		return lastName;
	}

	public String getPassword()
	{
		return this.password;
	}

	public Role getRole()
	{
		return this.role;
	}
	
	public UserStatus getStatus()
	{
		return status;
	}
	
	public TimeZone getTimeZone()
	{
		if (timeZone == null)
		{
			if (timeZoneId != null)
			{
				timeZone = TimeZone.getTimeZone(timeZoneId);
			}
			else
			{
				timeZone = TimeZone.getTimeZone("IST");
			}
		}

		return timeZone;
	}
	
	public String getTimeZoneId()
	{
		return timeZoneId;
	}

	public String getUserType()
	{
		return userType;
	}

	public void setAccount(Account account)
	{
		this.account = account;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	@Override
	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setLastLogin(Date lastLogin)
	{
		this.lastLogin = lastLogin;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void setRole(Role role)
	{
		this.role = role;
	}
	
	public void setStatus(UserStatus state)
	{
		this.status = state;
	}

	public void setTimeZoneId(String timeZoneId)
	{
		if (timeZoneId != null)
		{
			timeZone = TimeZone.getTimeZone(timeZoneId);
		}
		else
		{
			timeZone = TimeZone.getTimeZone("IST");
		}
		this.timeZoneId = timeZoneId;
	}

	public void setUserType(String userType)
	{
		this.userType = userType;
	}
}