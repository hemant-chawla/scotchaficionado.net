package com.aficionado.domain;

import javax.persistence.Cacheable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries(
{
	@NamedQuery(name = "SuperAdministrator.findAll", query = "Select s from SuperAdministrator s")	
})

@Entity
@Cacheable
@DiscriminatorValue("SuperAdministrator")
@Table(name = "super_administrator")
public class SuperAdministrator extends User
{
	private static final long serialVersionUID = 1L;
	
	public SuperAdministrator()
	{		
	}
}
