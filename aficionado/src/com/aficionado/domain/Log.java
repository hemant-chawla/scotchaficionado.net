package com.aficionado.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.aficionado.domain.type.EntityType;
import com.aficionado.domain.type.LogType;

@NamedQueries(
{
	@NamedQuery(name = "Log.findByLastThreeDays", query = "Select l from Log l where l.createdAt < :date"), 
	@NamedQuery(name = "Log.findByEntityId", query = "Select l from Log l where l.entityId = :date")
})

/**
 * The persistent class for the log database table.
 * 
 */
@Entity
@Table(name = "log")
public class Log extends Domain<Integer>
{
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "entity_id")
	private int entityId;

	@Enumerated(EnumType.STRING)
	@Column(name = "entity_type")
	private EntityType entityType;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	private LogType type;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Log()
	{
	}

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public int getEntityId()
	{
		return entityId;
	}

	public EntityType getEntityType()
	{
		return entityType;
	}

	@Override
	public Integer getId()
	{
		return id;
	}

	public LogType getType()
	{
		return type;
	}

	public User getUser()
	{
		return user;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public void setEntityId(int entityId)
	{
		this.entityId = entityId;
	}

	public void setEntityType(EntityType entityType)
	{
		this.entityType = entityType;
	}

	@Override
	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setType(LogType type)
	{
		this.type = type;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

}
