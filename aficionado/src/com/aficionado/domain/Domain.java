package com.aficionado.domain;

import java.io.Serializable;

public abstract class Domain<K> implements Serializable
{
	protected static final long serialVersionUID = 1L;

	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object object)
	{
		if (object == this)
		{
			return true;
		}

		if (object == null || object.getClass() != this.getClass())
		{
			return false;
		}

		if (getId() == null)
		{
			return false;
		}

		return getId().equals(((Domain<K>) object).getId());
	}

	public abstract K getId();

	public abstract void setId(K id);
}
