package com.aficionado.domain;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.aficionado.domain.type.NotificationFrequency;
import com.aficionado.domain.type.NotificationType;

@NamedQueries(
{ @NamedQuery(name = "Notification.findByLastSevenDays", query = "Select n from Notification n where n.createdAt < :date") })

@Entity
@Cacheable
@Table(name = "notification")
public class Notification extends Domain<Integer>
{
	private static final long serialVersionUID = 1L;

	private String content;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "entity_id")
	private int entityId;

	@ManyToOne
	@JoinColumn(name = "from_user_id")
	private User fromUser;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(name = "notify_cycle")
	private NotificationFrequency notifyCycle;

	private boolean sent;

	@Enumerated(EnumType.STRING)
	private NotificationType type;
	
	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Notification()
	{
	}

	public String getContent()
	{
		return content;
	}

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public int getEntityId()
	{
		return entityId;
	}

	public User getFromUser()
	{
		return fromUser;
	}

	@Override
	public Integer getId()
	{
		return this.id;
	}

	public NotificationFrequency getNotifyCycle()
	{
		return notifyCycle;
	}

	public NotificationType getType()
	{
		return type;
	}

	public User getUser()
	{
		return user;
	}

	public boolean isSent()
	{
		return sent;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public void setEntityId(int entityId)
	{
		this.entityId = entityId;
	}


	public void setFromUser(User fromUser)
	{
		this.fromUser = fromUser;
	}

	@Override
	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setNotifyCycle(NotificationFrequency notifyCycle)
	{
		this.notifyCycle = notifyCycle;
	}

	public void setSent(boolean sent)
	{
		this.sent = sent;
	}

	public void setType(NotificationType type)
	{
		this.type = type;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

}
