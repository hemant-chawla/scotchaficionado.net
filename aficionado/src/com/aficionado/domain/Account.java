package com.aficionado.domain;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.aficionado.domain.type.AccountCategory;
import com.aficionado.domain.type.AccountStatus;

/**
 * The persistent class for the account database table.
 * 
 */

@NamedQueries(
{ 	
		@NamedQuery(name = "Account.countByEmail", query = "Select count(a.id) from Account a where a.id <> :account and a.email = :email"),
		@NamedQuery(name = "Account.countByShortCode", query = "Select count(a.id) from Account a where a.id <> :account and a.shortCode = :shortCode")
})

@Entity
@Cacheable
@Table(name = "account")
public class Account extends Domain<Integer>
{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "category")
	@Enumerated(EnumType.STRING)
	private AccountCategory category;
	
	@Column(name = "contact_information")
	private String contactInformation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "email")
	private String email;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "notes")
	private String notes;
	
	@Column(name = "short_code")
	private String shortCode;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private AccountStatus status;

	@Column(name = "title")
	private String title;

	@Column(name = "url")
	private String url;

	public Account()
	{
	}

	public AccountCategory getCategory()
	{
		return category;
	}

	public String getContactInformation()
	{
		return contactInformation;
	}

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public String getEmail()
	{
		return email;
	}

	@Override
	public Integer getId()
	{
		return this.id;
	}

	public String getNotes()
	{
		return notes;
	}

	public String getShortCode()
	{
		return shortCode;
	}

	public AccountStatus getStatus()
	{
		return status;
	}

	public String getTitle()
	{
		return title;
	}

	public String getUrl()
	{
		return url;
	}

	public void setCategory(AccountCategory category)
	{
		this.category = category;
	}

	public void setContactInformation(String contactInformation)
	{
		this.contactInformation = contactInformation;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	@Override
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	
	public void setShortCode(String shortCode)
	{
		this.shortCode = shortCode;
	}
	
	public void setStatus(AccountStatus status)
	{
		this.status = status;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}
}
