package com.aficionado.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.aficionado.domain.type.RoleType;

/**
 * The persistent class for the role database table.
 *
 */
@NamedQueries(
{ @NamedQuery(name = "Role.findByIds", query = "Select r from Role r WHERE r.id IN :ids") })

@Entity
@Cacheable
@Table(name = "role")
public class Role extends Domain<Integer>
{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "name", nullable = false)
	private String name;

	public Role()
	{
	}

	public Role(int id)
	{
		this.id = id;
	}

	public Role(RoleType roleType)
	{
		this.id = roleType.getId();
	}

	@Override
	public Integer getId()
	{
		return this.id;
	}

	public String getName()
	{
		return this.name;
	}

	@Override
	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}