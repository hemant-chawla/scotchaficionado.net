package com.aficionado.service;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.dao.SuperAdministratorDao;
import com.aficionado.domain.SuperAdministrator;
import com.aficionado.domain.User;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class SuperAdministratorService implements Serializable
{
	private static final long serialVersionUID = 1L;

	@EJB
	private SuperAdministratorDao superAdministratorDao;

	/**
	 * Gets the Super Administrator by Id.
	 * 
	 * @param id
	 *            the id
	 * @param principal
	 *            the principal
	 * @return the administrator
	 * @throws ServiceException
	 *             the service exception
	 */
	public SuperAdministrator get(Integer id, User principal) throws ServiceException
	{
		SuperAdministrator administrator = superAdministratorDao.findById(id, SuperAdministrator.class);
		if (administrator == null)
		{
			throw new ServiceException(ServiceExceptionType.InvalidParam);
		}

		return administrator;
	}

}
