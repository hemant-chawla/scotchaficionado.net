package com.aficionado.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.dao.NotificationDao;
import com.aficionado.dao.SuperAdministratorDao;
import com.aficionado.dao.UserDao;
import com.aficionado.domain.Account;
import com.aficionado.domain.Notification;
import com.aficionado.domain.SuperAdministrator;
import com.aficionado.domain.User;
import com.aficionado.domain.type.NotificationFrequency;
import com.aficionado.domain.type.NotificationType;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.service.util.RoleHelper;
import com.aficionado.util.Constants;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;
import com.aficionado.web.model.Email;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class NotificationService implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@EJB
	private MailService mailService;

	@EJB
	private NotificationDao notificationDao;

	@EJB
	private SuperAdministratorDao superAdminDao;

	@EJB
	private UserDao userDao;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createNewPassword(User user) throws ServiceException
	{
		Map<String, Object> templateArgs = new HashMap<String, Object>(1);
		templateArgs.put("email", user.getEmail());

		List<String> toMail = new ArrayList<String>();
		toMail.add(user.getEmail());

		Email email = new Email(Constants.CAPEXIL_FROM_EMAIL, toMail, null, null);
		String content[] = mailService.send(email, "password-changed.xslt", templateArgs);

		Notification notification = new Notification();
		notification.setEntityId(user.getId());
		notification.setUser(user);
		notification.setNotifyCycle(NotificationFrequency.Instant);
		notification.setType(NotificationType.PasswordChanged);
		notification.setCreatedAt(new Date());
		notification.setContent(content[1]);
		notification.setSent(true);
		notification.setFromUser(user);
		notificationDao.persist(notification);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createAccountCreated(Account account, User principal) throws ServiceException
	{
		Map<String, Object> templateArgs = new HashMap<String, Object>(1);
		templateArgs.put("email", account.getEmail());
		templateArgs.put("title", account.getTitle());

		List<String> toMail = new ArrayList<String>();
		toMail.add(account.getEmail());

		List<SuperAdministrator> superAdmins = superAdminDao.findAll();
		List<String> bccMail = new ArrayList<String>();
		for (SuperAdministrator superAdmin : superAdmins)
		{
			bccMail.add(superAdmin.getEmail());
		}

		Email email = new Email(Constants.CAPEXIL_FROM_EMAIL, toMail, null, bccMail);
		String content[] = mailService.send(email, "account-created.xslt", templateArgs);

		Notification notification = new Notification();
		notification.setEntityId(account.getId());
		notification.setUser(principal);
		notification.setNotifyCycle(NotificationFrequency.Instant);
		notification.setType(NotificationType.AccountCreated);
		notification.setCreatedAt(new Date());
		notification.setContent(content[1]);
		notification.setSent(true);
		notification.setFromUser(principal);
		notificationDao.persist(notification);
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public List<Notification> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User principal)
			throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			return notificationDao.findAll(first, pageSize, sortField, sortOrder, filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	
	/**
	 * 
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public Long getCount(Collection<Filter> filters, User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			return notificationDao.getCount(filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
}
