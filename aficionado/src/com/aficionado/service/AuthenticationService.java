package com.aficionado.service;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.domain.User;
import com.aficionado.domain.type.RoleType;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.service.util.RoleHelper;

/**
 * The Class AuthenticationService.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AuthenticationService implements Serializable
{

	private static final long serialVersionUID = 1L;

	public void canAccessAccount(User principal) throws ServiceException
	{
		boolean authorized = false;

		if (RoleHelper.isSuperAdministrator(principal))
		{
			authorized = true;
		}

		if (!authorized)
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	public void canAccessUser(User user, User principal) throws ServiceException
	{
		if (user.equals(principal))
		{
			return;
		}

		if (!RoleHelper.isUserInRoles(principal, new RoleType[]
		{ RoleType.SuperAdministrator }))
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	public void canCreateAccount(User principal) throws ServiceException
	{
		boolean authorized = false;

		if (RoleHelper.isSuperAdministrator(principal))
		{
			authorized = true;
		}

		if (!authorized)
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	public void canCreateAdministrator(User principal) throws ServiceException
	{
		boolean authorized = false;

		if (RoleHelper.isSuperAdministrator(principal))
		{
			authorized = true;
		}

		if (!authorized)
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}

	}

	public void canCreateUser(User principal) throws ServiceException
	{
		if (!RoleHelper.isUserInRoles(principal, new RoleType[]
		{ RoleType.SuperAdministrator }))
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	public void canModifyAccount(User principal) throws ServiceException
	{
		boolean authorized = false;

		if (RoleHelper.isSuperAdministrator(principal))
		{
			authorized = true;
		}

		if (!authorized)
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	public void canModifyUser(User user, User principal) throws ServiceException
	{
		if (user.equals(principal))
		{
			return;
		}

		if (!RoleHelper.isUserInRoles(principal, new RoleType[]
		{ RoleType.SuperAdministrator }))
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
}
