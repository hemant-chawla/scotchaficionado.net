package com.aficionado.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.dao.LogDao;
import com.aficionado.domain.Account;
import com.aficionado.domain.Log;
import com.aficionado.domain.User;
import com.aficionado.domain.type.EntityType;
import com.aficionado.domain.type.LogType;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.service.util.RoleHelper;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

/**
 * The Class LogService records 'create' & 'modify' activity on an entity.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class LogService implements Serializable
{
	private static final long serialVersionUID = 1L;

	@EJB
	private LogDao logDao;
	
	/**
	 * 
	 * @param id
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public Log get(Integer id, User principal) throws ServiceException
	{
		Log log = logDao.findById(id, Log.class);

		if (log == null)
		{
			throw new ServiceException(ServiceExceptionType.InvalidParam);
		}

		return log;
	}
	/**
	 * Gets all Logs.
	 *
	 * @param first the first
	 * @param pageSize the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters the filters
	 * @param rowCount the row count
	 * @param brief the brief
	 * @param principal the principal
	 * @return the list of Logs
	 * 
	 */
	public List<Log> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal) || RoleHelper.isAccountUser(principal))
		{
			return logDao.findAll(first, pageSize, sortField, sortOrder, filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param principal
	 * @param entityId
	 * @return
	 * @throws ServiceException
	 */
	public List<Log> getAllByEntityId(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User principal, int entityId) throws ServiceException
	{
		if (RoleHelper.isAccountUser(principal) || RoleHelper.isSuperAdministrator(principal))
		{
			return logDao.findAllByEntityId(first, pageSize, sortField, sortOrder, filters, entityId);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param principal
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	public List<Log> getAllByUser(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User principal, User user) throws ServiceException
	{
		if (RoleHelper.isAccountUser(principal) || RoleHelper.isSuperAdministrator(principal))
		{
			return logDao.findAllByUser(first, pageSize, sortField, sortOrder, filters, user);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * 
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public Long getCount(Collection<Filter> filters, User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal) || RoleHelper.isAccountUser(principal))
		{
			return logDao.getCount(filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * 
	 * @param filters
	 * @param principal
	 * @param entityId
	 * @return
	 * @throws ServiceException
	 */
	public Long getCount(Collection<Filter> filters, User principal, int entityId) throws ServiceException
	{
		if (RoleHelper.isAccountUser(principal) || RoleHelper.isSuperAdministrator(principal))
		{
			return logDao.getCount(filters, entityId);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * 
	 * @param filters
	 * @param principal
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	public Long getCount(Collection<Filter> filters, User principal, User user) throws ServiceException
	{
		if (RoleHelper.isAccountUser(principal) || RoleHelper.isSuperAdministrator(principal))
		{
			return logDao.getCount(filters, user);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void logAccountCreated(Account account, User principal)
	{
		Log log = new Log();
		log.setEntityId(account.getId());
		log.setEntityType(EntityType.Account);
		log.setUser(principal);
		log.setCreatedAt(new Date());
		log.setType(LogType.Created);		
		
		logDao.persist(log);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void logAccountModified(Account account, User principal)
	{
		Log log = new Log();
		log.setEntityId(account.getId());
		log.setEntityType(EntityType.Account);
		log.setUser(principal);
		log.setCreatedAt(new Date());
		log.setType(LogType.Modified);		
		
		logDao.persist(log);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void logUserCreated(User user, User principal)
	{
		Log log = new Log();
		log.setEntityId(user.getId());
		log.setEntityType(EntityType.User);
		log.setUser(principal);
		log.setCreatedAt(new Date());
		log.setType(LogType.Created);		
		
		logDao.persist(log);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void logUserModified(User user, User principal)
	{
		Log log = new Log();
		log.setEntityId(user.getId());
		log.setEntityType(EntityType.User);
		log.setUser(principal);
		log.setCreatedAt(new Date());
		log.setType(LogType.Modified);		
		
		logDao.persist(log);
	}
}
