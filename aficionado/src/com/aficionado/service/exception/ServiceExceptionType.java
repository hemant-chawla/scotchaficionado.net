package com.aficionado.service.exception;

public enum ServiceExceptionType
{
	AccountAlreadyExists, EmailAlreadyExists, InvalidParam, InvalidPassword, System, Unauthorized, UserAlreadyExists, UserAlreadyExistsForAccount, ShortCodeAlreadyExists;
}
