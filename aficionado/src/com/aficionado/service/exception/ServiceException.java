package com.aficionado.service.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class ServiceException extends Exception
{
	private static final long serialVersionUID = 1L;

	private ServiceExceptionType exceptionType;

	public ServiceException(ServiceExceptionType exceptionType)
	{
		super();
		this.exceptionType = exceptionType;
	}

	public ServiceException(ServiceExceptionType exceptionType, String messsage)
	{
		super(messsage);
		this.exceptionType = exceptionType;
	}

	public ServiceException(ServiceExceptionType exceptionType, Throwable cause)
	{
		super(cause);
		this.exceptionType = exceptionType;
	}

	public ServiceExceptionType getExceptionType()
	{
		return exceptionType;
	}

}
