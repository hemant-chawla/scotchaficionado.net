package com.aficionado.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.dao.UserDao;
import com.aficionado.domain.User;
import com.aficionado.domain.type.UserStatus;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.service.util.RoleHelper;
import com.aficionado.util.Constants;
import com.aficionado.util.Filter;
import com.aficionado.util.MessageDigesterHelper;
import com.aficionado.util.PasswordGenerator;
import com.aficionado.util.SortOrder;
import com.aficionado.web.model.Email;

/**
 * The Class UserService.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class UserService implements Serializable
{

	private static final Logger log = Logger.getLogger(UserService.class.getName());
	
	private static final long serialVersionUID = 1L;

	@EJB
	private AuthenticationService authenticationService;

	@EJB
	private LogService logService;

	@EJB
	private MailService mailService;

	@EJB
	private NotificationService notificationService;
	
	@EJB
	private UserDao userDao;
	

	/**
	 * Gets User by Id.
	 *
	 * @param id the id
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	public User get(Integer id, User principal) throws ServiceException
	{
		User user;
		if (id == null)
		{
			authenticationService.canCreateUser(principal);
			user = new User();
			user.setStatus(UserStatus.Enabled);
		}
		else
		{
			user = userDao.findById(id, User.class);
	
			if (user == null)
			{
				throw new ServiceException(ServiceExceptionType.InvalidParam);
			}
			
			authenticationService.canAccessUser(user, principal);
		}
		
		return user;
	}

	public User get(String email)
	{
		return userDao.findByEmailEnabled(email);
	}
	
	/**
	 * Gets list of all users
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public List<User> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters,
			User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			return userDao.findAll(first, pageSize, sortField, sortOrder, filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	/**
	 * Gets the count
	 * 
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public Long getCount(Collection<Filter> filters, User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			return userDao.getCount(filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

	/**
	 * Sets the last login date as current date of an existing user.
	 *
	 * @param email the email
	 * @return the user
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User login(String email)
	{
		User user = userDao.findByEmailEnabled(email);
		user.setLastLogin(new Date());
		
		return user;
	}

	/**
	 * Refreshes the user by finding it by the Id.
	 *
	 * @param user the user
	 * @return the user
	 */
	public User refresh(User user)
	{
		return userDao.findById(user.getId(), User.class);
	}
	
	/**
	 * Resets the user password by sending a forgot-password link at user's
	 * registered email address.
	 *
	 * @param email the email
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void resetPassword(String email) throws ServiceException
	{
		User user = userDao.findByEmailEnabled(email);
		if (user == null)
		{
			throw new ServiceException(ServiceExceptionType.InvalidParam);
		}
		
		String password = PasswordGenerator.generate();
		user.setPassword(MessageDigesterHelper.md5DigestAndHex(password));

		Map<String, Object> params = new HashMap<String, Object>(2);
		params.put("password", password);
		params.put("email", email);
		
		List<String> toMail = new ArrayList<String>();
		toMail.add(user.getEmail());

		mailService.send(new Email(Constants.CAPEXIL_FROM_EMAIL, toMail, null, null), "forgot-password.xslt", params);
	}

	/**
	 * Saves the user.
	 *
	 * @param user the user
	 * @param updatePassword the update password
	 * @param newPassword the new password
	 * @param password the password
	 * @param principal the principal
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User save(User user, boolean updatePassword, String newPassword, String password, User principal)
			throws ServiceException
	{
		boolean isNew = user.getId() == null;

		if (userDao.countByEmail(user.getEmail(), user) != 0)
		{
			throw new ServiceException(ServiceExceptionType.EmailAlreadyExists);
		}
		
		if (updatePassword)
		{
			if (!isNew && !user.getPassword().equals(password))
			{
				password = MessageDigesterHelper.md5DigestAndHex(password);
				if (!user.getPassword().equals(password))
				{
					throw new ServiceException(ServiceExceptionType.InvalidPassword);
				}
			}

			newPassword = MessageDigesterHelper.md5DigestAndHex(newPassword);
			user.setPassword(newPassword);
			notificationService.createNewPassword(user);
		}
				
		if (isNew)
		{
			authenticationService.canCreateUser(principal);
			userDao.persist(user);
			userDao.flush();
			logService.logUserCreated(user, principal);
		}
		else
		{
			authenticationService.canModifyUser(user, principal);
			user = userDao.merge(user);
			logService.logUserModified(user, principal);
		}

		return user;
	}
	
	/**
	 * Toggles between enabled or disabled state of the user
	 *
	 * @param user the user
	 * @param principal the principal
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User toggleEnabled(User user, User principal) throws ServiceException
	{
		if (!RoleHelper.isSuperAdministrator(principal))
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}

		boolean enabled = user.getStatus() != UserStatus.Enabled;

		user = userDao.merge(user);
		user.setStatus(enabled ? UserStatus.Enabled : UserStatus.Disabled);

		return user;
	}
}
