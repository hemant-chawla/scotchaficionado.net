package com.aficionado.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.dao.AccountDao;
import com.aficionado.domain.Account;
import com.aficionado.domain.User;
import com.aficionado.domain.type.AccountStatus;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.service.util.RoleHelper;
import com.aficionado.util.Filter;
import com.aficionado.util.SortOrder;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AccountService implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private AccountDao accountDao;
	
	@EJB
	private AuthenticationService authenticationService;

	@EJB
	private NotificationService notificationService;

	@EJB
	private LogService logService;

	/**
	 * 
	 * @param id
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public Account get(Integer id, User principal) throws ServiceException
	{
		Account account;
		if (id == null)
		{
			authenticationService.canCreateAccount(principal);
			account = new Account();
			account.setCreatedAt(new Date());
			account.setStatus(AccountStatus.Created);
		}
		else
		{
			account = accountDao.findById(id, Account.class);

			if (account == null)
			{
				throw new ServiceException(ServiceExceptionType.InvalidParam);
			}

			authenticationService.canAccessAccount(principal);
		}

		return account;
	}
	
	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public List<Account> getAll(int first, int pageSize, String sortField, SortOrder sortOrder, Collection<Filter> filters, User principal)
			throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			return accountDao.findAll(first, pageSize, sortField, sortOrder, filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * 
	 * @param filters
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	public Long getCount(Collection<Filter> filters, User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			return accountDao.getCount(filters);
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}
	
	/**
	 * Save an account
	 * 
	 * @param account
	 * @param principal
	 * @return
	 * @throws ServiceException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Account save(Account account, User principal) throws ServiceException
	{
		if (accountDao.countByEmail(account.getEmail(), account) != 0)
		{
			throw new ServiceException(ServiceExceptionType.EmailAlreadyExists);
		}

		if (accountDao.countByShortCode(account.getShortCode(), account) != 0)
		{
			throw new ServiceException(ServiceExceptionType.ShortCodeAlreadyExists);
		}
		
		boolean isNew = account.getId() == null;

		if (isNew)
		{
			authenticationService.canCreateAccount(principal);
			accountDao.persist(account);
			accountDao.flush();
			logService.logAccountCreated(account, principal);
		}
		else
		{
			authenticationService.canAccessAccount(principal);
			account = accountDao.merge(account);
			logService.logAccountModified(account, principal);
		}

		return account;
	}
	
}
