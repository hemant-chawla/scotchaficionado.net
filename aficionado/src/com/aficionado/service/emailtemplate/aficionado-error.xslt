<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="error"/>
	<xsl:template match="/">
		<html>
			<head>
				<title>
				Scotch Aficionado | Error or Exception  
				</title>
			</head>
			<body>
				<div>
					<h3 style="color:#2774B0">
						Scotch Aficionado | Error or Exception 
					</h3>
					<p style="background-color:#F1F2F4; border-radius:4px; display:inline-block; padding:8px 15px;">
						<strong><xsl:value-of select="$error"/></strong>
					</p>
					
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>