<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="companyName"/>
	<xsl:template match="/">
	<html>
			<head>
				<title>
					 Missing Email Address of <xsl:value-of select="$companyName"/>
				</title>
			</head>
			<body>
				<div>
								<table style="width:100%">
  								<tr>
    								<td bgcolor="#193F75" >
    								<font color="#F7F7F7">
    								<h2>
										Missing email address of <xsl:value-of select="$companyName"/>
									</h2>
									</font>
									</td>
  								</tr>
  								<tr>
    								<td bgcolor="#F7F7F7">
    								<font color="#193F75">
    								<hr/>
    								<p>
										The system found a missing email address of the company - <xsl:value-of select="$companyName"/>).
									</p>
									<p>
										The administrator must make a note and add the email address in the system at the earliest. 
									</p>
									<hr/>
									</font>
    								</td>
  								</tr>
  							</table>	
					<p>
						This is an Automated email from Scotch Aficionado. Do not reply to this email.
					</p>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>