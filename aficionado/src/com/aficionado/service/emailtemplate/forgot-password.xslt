﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="password" />
	<xsl:param name="email" />
	<xsl:template match="/">
		<html>
			<head>
				<title>
					User (<xsl:value-of select="$email"/>), your new password for Scotch Aficionado!
				</title>
			</head>
			<body>
				<div>
				<table style="width:100%">
  								<tr>
    								<td bgcolor="#193F75" >
    								<font color="#F7F7F7">
    								<h3>
										User (<xsl:value-of select="$email"/>), your new password has been generated!
									</h3>
									</font>
									</td>
  								</tr>
  								<tr>
    								<td bgcolor="#F7F7F7">
    								<font color="#193F75">
    								<hr/>
					    			<p>Dear User (<xsl:value-of select="$email"/>),</p>
									
									<p>Thank you for choosing Scotch Aficionado!</p>
									
									<p>Your new password has been generated. Please login using the below.</p>
									
										<h3 style="background-color:#2ECC71; border-radius:10px; display:inline-block; padding:8px 15px;">
											<strong><xsl:value-of select="$password"/></strong>
										</h3>
										<p>
											<a href="/account/login" style="color:#1C76BA;">Go to Scotch Aficionado</a>
										</p>
										
										<p>Please note that your email Id for login remains the same (<xsl:value-of select="$email"/>).</p>
									<hr/>
									</font>
    								</td>
  								</tr>
  							</table>
  							<p>
							<small>This is an Automated response from Scotch Aficionado. Do not reply to this email.</small>
							</p>	
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>