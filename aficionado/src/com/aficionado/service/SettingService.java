package com.aficionado.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.aficionado.dao.SettingDao;
import com.aficionado.domain.Setting;
import com.aficionado.domain.User;
import com.aficionado.domain.type.SettingType;
import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.service.util.RoleHelper;

/**
 * The Class SettingService.
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class SettingService implements Serializable
{
	private static final long serialVersionUID = 1L;

	@EJB
	private SettingDao settingDao;
	
	@EJB
	private NotificationService notificationService;

	/**
	 * Gets all the Settings.
	 *
	 * @param principal the principal
	 * @return map of settings
	 * @throws ServiceException the service exception
	 */
	public Map<String, Setting> getAll(User principal) throws ServiceException
	{
		/*if (!RoleHelper.isAdministrator(principal))
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}*/

		List<Setting> settings = settingDao.findAll();

		Map<String, Setting> map = new HashMap<String, Setting>(settings.size());
		for (Setting setting : settings)
		{
			map.put(setting.getId(), setting);
		}

		return map;
	}

	/**
	 * Gets the Setting by type.
	 *
	 * @param type the type
	 * @return the setting
	 */
	public Setting get(SettingType type)
	{
		return settingDao.findBySettingType(type);
	}

	/**
	 * Save the setting.
	 *
	 * @param settings the settings
	 * @param principal the principal
	 * @throws ServiceException the service exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void save(Map<String, Setting> settings, User principal) throws ServiceException
	{
		if (RoleHelper.isSuperAdministrator(principal))
		{
			for (Setting setting : settings.values())
			{
				settingDao.merge(setting);
			}		
		}
		else
		{
			throw new ServiceException(ServiceExceptionType.Unauthorized);
		}
	}

}
