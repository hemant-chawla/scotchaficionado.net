package com.aficionado.service.util;

import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.util.Constants;

public class TextHelper
{
	private final static Pattern NUMBER_PATTERN = Pattern.compile("[+-]?\\d*\\.?\\d+");
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";


	public static boolean empty(String s)
	{
		if (s == null || s.trim().equals(""))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static String formatTwoDecimal(Double number)
	{
		return String.format( "%.2f", number);
	}
	
	public static boolean isValidEmail(String email)
	{
		boolean isValidEmail = true;
		
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		
		if(email == null || email.toString().isEmpty())
		{
			isValidEmail = false;
		}

		Matcher matcher = pattern.matcher(email);
		if (!matcher.matches())
		{
			isValidEmail = false;
		}
		
		return isValidEmail;
	}
	
	public static String[] getContent(String templateName, Map<String, Object> templateArgs) throws ServiceException
	{
		try
		{
			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer transformer = tfactory.newTransformer(new StreamSource(TextHelper.class.getClassLoader().getResource(
					"/com/capexil/service/emailtemplate/" + templateName).toString()));

			if (templateArgs != null)
			{
				for (Entry<String, Object> entry : templateArgs.entrySet())
				{
					transformer.setParameter(entry.getKey(), entry.getValue());
				}
			}

			DOMResult result = new DOMResult();
			transformer.transform(new DOMSource(), result);
			Node root = result.getNode();

			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();

			Node subjectNode = (Node) xpath.evaluate("//title/text()", root, XPathConstants.NODE);
			if (subjectNode == null)
			{
				throw new ServiceException(ServiceExceptionType.System, "title node not found in template:" + templateName);
			}

			Node bodyNode = (Node) xpath.evaluate("//body/div", root, XPathConstants.NODE);

			if (bodyNode == null)
			{
				throw new ServiceException(ServiceExceptionType.System, "body node not found in template:" + templateName);
			}

			NodeList links = (NodeList) xpath.evaluate("//a", bodyNode, XPathConstants.NODESET);
			for (int i = 0; i < links.getLength(); i++)
			{
				Element link = (Element) links.item(i);
				String href = link.getAttribute("href");

				if (!href.startsWith("http://"))
				{
					href = Constants.BASE_APPLICATION_URL + href;
					link.setAttribute("href", href);
				}
			}

			NodeList images = (NodeList) xpath.evaluate("//img", bodyNode, XPathConstants.NODESET);
			for (int i = 0; i < images.getLength(); i++)
			{
				Element image = (Element) images.item(i);
				String src = image.getAttribute("src");

				if (!src.startsWith("http://"))
				{
					src = Constants.BASE_APPLICATION_URL + src;
					image.setAttribute("href", src);
				}
			}

			StringWriter stringWriter = new StringWriter();
			Transformer bodyTransformer = tfactory.newTransformer();
			bodyTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			bodyTransformer.transform(new DOMSource(bodyNode), new StreamResult(stringWriter));

			return new String[]
			{ subjectNode.getTextContent().trim(), stringWriter.getBuffer().toString() };
		}
		catch (Exception e)
		{
			throw new ServiceException(ServiceExceptionType.System, e);
		}
	}

	public static String incrementSeries(String number, String delimiter)
	{
		if (number != null)
		{
			String[] split = number.split(delimiter);
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < split.length; i++)
			{
				String value = split[i];
				if (isNumber(value))
				{
					Integer appended = Integer.valueOf(value) + 1;
					value = String.valueOf(appended);
				}

				builder.append(value);
				if (i < split.length - 1)
				{
					builder.append("/");
				}
			}

			return builder.toString();
		}

		return null;
	}
	
	public static boolean isNotNullOrEmpty(final String string)
	{
		return string != null && !string.isEmpty() && !string.trim().isEmpty();
	}
	
	public static boolean isNumber(String input)
	{
		Matcher m = NUMBER_PATTERN.matcher(input);

		return m.matches();
	}
	
	public static String trimNewline(String line)
	{
		return line.replaceAll("(\\t|\\r?\\n)+", " ");
	}

}