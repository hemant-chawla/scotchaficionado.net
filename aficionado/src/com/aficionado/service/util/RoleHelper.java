package com.aficionado.service.util;

import com.aficionado.domain.User;
import com.aficionado.domain.type.RoleType;

public class RoleHelper
{

	public static boolean isAccountUser(User user)
	{
		return user.getRole() != null && user.getRole().getId() == RoleType.AccountUser.getId();
	}

	public static boolean isSuperAdministrator(User user)
	{
		return user.getRole() != null && user.getRole().getId() == RoleType.SuperAdministrator.getId();
	}

	public static boolean isUserInRoles(User user, RoleType[] roles)
	{
		if (user == null || user.getRole() == null)
		{
			return false;
		}

		for (RoleType role : roles)
		{
			if (user.getRole().getId() == role.getId())
			{
				return true;
			}
		}
		return false;
	}
}
