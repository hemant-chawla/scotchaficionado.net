package com.aficionado.service.util;

import java.util.Random;

public class PasswordGenerator
{

	private static final int PASSWORD_LENGTH = 10;

	public static String generate()
	{
		char pass[] = new char[PASSWORD_LENGTH];
		Random rnd = new Random();

		int j;
		for (int i = 0; i < PASSWORD_LENGTH; i++)
		{
			j = rnd.nextInt() + 97;
			if (j < 0)
			{
				j = 0 - j;
			}

			j = (j % 26) + 97;

			pass[i] = (char) j;
		}

		return new String(pass);
	}
}
