package com.aficionado.service.util;

import java.io.File;

import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;

public class FileHelper
{
	public static void mkdirs(File destFolder) throws ServiceException
	{
		if (!destFolder.exists())
		{
			if (!destFolder.mkdirs())
			{
				throw new ServiceException(ServiceExceptionType.System);
			}
		}
	}
}
