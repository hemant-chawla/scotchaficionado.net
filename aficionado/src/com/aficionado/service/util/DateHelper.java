package com.aficionado.service.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateHelper
{
	public static Date getExpiryDate(int years)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, years);
		
		return calendar.getTime();
	}
	
	/**
	 * 
	 * @return
	 */
	public static Date getFinancialYearExpiryDate(int duration)
	{
		int year = Calendar.getInstance().get(Calendar.YEAR);
		Calendar calendar = new GregorianCalendar();

		calendar.set(Calendar.YEAR, year + duration);
		calendar.set(Calendar.MONTH, 2);
		calendar.set(Calendar.DAY_OF_MONTH, 31);

		return calendar.getTime();
	}
	
	public static String getIndianDateFormat(Date date, String patternString)
	{
		DateFormat indianFormat = new SimpleDateFormat(patternString);
		if (TimeZone.getTimeZone("Asia/Kolkata") != null)
		{	
			indianFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		}
		
	    return indianFormat.format(date); 
	}

	/** 
     * Returns the maximum of two dates. A null date is treated as being less
     * than any non-null date. 
     */
    public static Date max(Date d1, Date d2) {
		if (d1 == null && d2 == null)
			return null;
		if (d1 == null)
			return d2;
		if (d2 == null)
			return d1;
		return (d1.after(d2)) ? d1 : d2;
	}
}
