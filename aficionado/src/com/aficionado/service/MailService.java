package com.aficionado.service;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.core.MediaType;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections.CollectionUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aficionado.service.exception.ServiceException;
import com.aficionado.service.exception.ServiceExceptionType;
import com.aficionado.util.Constants;
import com.aficionado.web.model.Email;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

@Stateless
public class MailService implements Serializable
{

	private static final Logger logger = Logger.getLogger(MailService.class.getName());

	private static final long serialVersionUID = 1L;

	private String mailgunApiKey = "key-06a1e1b471889447f0a32d2eacda2418";
	
	private String mailgunHost = "scotchaficionado.net";

	/**
	 * Gets content of the email.
	 * 
	 * @param templateName
	 *            the template name
	 * @param templateArgs
	 *            the template args
	 * @return the content
	 * @throws ServiceException
	 *             the service exception
	 */
	public String[] getContent(String templateName, Map<String, Object> templateArgs) throws ServiceException
	{
		try
		{
			TransformerFactory tfactory = TransformerFactory.newInstance();
			Transformer transformer = tfactory.newTransformer(new StreamSource(getClass().getResource(
					"/com/capexil/service/emailtemplate/" + templateName).toString()));

			if (templateArgs != null)
			{
				for (Entry<String, Object> entry : templateArgs.entrySet())
				{
					transformer.setParameter(entry.getKey(), entry.getValue());
				}
			}

			DOMResult result = new DOMResult();
			transformer.transform(new DOMSource(), result);
			Node root = result.getNode();

			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();

			Node subjectNode = (Node) xpath.evaluate("//title/text()", root, XPathConstants.NODE);
			if (subjectNode == null)
			{
				throw new ServiceException(ServiceExceptionType.System, "title node not found in template:" + templateName);
			}

			Node bodyNode = (Node) xpath.evaluate("//body/div", root, XPathConstants.NODE);

			if (bodyNode == null)
			{
				throw new ServiceException(ServiceExceptionType.System, "body node not found in template:" + templateName);
			}

			NodeList links = (NodeList) xpath.evaluate("//a", bodyNode, XPathConstants.NODESET);
			for (int i = 0; i < links.getLength(); i++)
			{
				Element link = (Element) links.item(i);
				String href = link.getAttribute("href");

				if (!href.startsWith("http://"))
				{
					href = Constants.BASE_APPLICATION_URL + href;
					link.setAttribute("href", href);
				}
			}

			NodeList images = (NodeList) xpath.evaluate("//img", bodyNode, XPathConstants.NODESET);
			for (int i = 0; i < images.getLength(); i++)
			{
				Element image = (Element) images.item(i);
				String src = image.getAttribute("src");

				if (!src.startsWith("http://"))
				{
					src = Constants.BASE_APPLICATION_URL + src;
					image.setAttribute("href", src);
				}
			}

			StringWriter stringWriter = new StringWriter();
			Transformer bodyTransformer = tfactory.newTransformer();
			bodyTransformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			bodyTransformer.transform(new DOMSource(bodyNode), new StreamResult(stringWriter));

			return new String[]
			{ subjectNode.getTextContent().trim(), stringWriter.getBuffer().toString() };
		}
		catch (Exception e)
		{
			throw new ServiceException(ServiceExceptionType.System, e);
		}
	}

	public String[] send(Email email, String templateName, Map<String, Object> templateArgs) throws ServiceException
	{
		Client client = Client.create();

		client.addFilter(new HTTPBasicAuthFilter("api", mailgunApiKey));

		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + mailgunHost + "/messages");

		String content[] = getContent(templateName, templateArgs);

		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("from", email.getFrom());
		List<String> mailIDs = email.getTo();
		for (String to : mailIDs)
		{
			formData.add("to", to);
		}

		if (!CollectionUtils.isEmpty(email.getBcc()))
		{
			List<String> bccMailIDs = email.getBcc();
			for (String bcc : bccMailIDs)
			{
				formData.add("bcc", bcc);
			}
		}
		
		if (!CollectionUtils.isEmpty(email.getCc()))
		{
			List<String> ccMailIDs = email.getCc();
			for (String cc : ccMailIDs)
			{
				formData.add("cc", cc);
			}
		}
		
		formData.add("subject", content[0]);
		formData.add("html", content[1]);
	
		ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
		String output = clientResponse.getEntity(String.class);

		logger.info("Email sent successfully : " + output);

		return content;
	}

	/**
	 * Sends email as an error report.
	 * 
	 * @param error
	 *            the error
	 */
	public void sendError(Throwable error)
	{
		StringBuilder message = new StringBuilder();
		
		try
		{
			message.append(error.toString());
			message.append("\n");

			for (StackTraceElement element : error.getStackTrace())
			{
				message.append(element);
				message.append("\n");
			}
			
			StringWriter sw = new StringWriter();
			error.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			
			message.append("\n\n");
			message.append("-------- Additionally ---------------------------");
			message.append("\n");
			message.append(exceptionAsString);
			
			List<String> toMail = new ArrayList<String>();
			toMail.add(Constants.ERROR_RECEIVER_EMAIL);
			Email email = new Email(Constants.CAPEXIL_FROM_EMAIL, toMail, null, null);
			Map<String, Object> templateArgs = new HashMap<String, Object>(1);
			templateArgs.put("error", message.toString());
			
			send(email, "capexil-error.xslt", templateArgs);
		}
		catch (Throwable t)
		{
			logger.log(Level.SEVERE, "System Error", t);
			logger.info("CATCHING exception : "+message.toString());
		}
	}
}
