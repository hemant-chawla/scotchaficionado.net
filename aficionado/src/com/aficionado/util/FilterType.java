package com.aficionado.util;

public enum FilterType 
{
	Contains,
	StartsWith,
	Equals,
	NotEquals,
	In,
	NotIn
}
