package com.aficionado.util;

import java.util.Random;

public class PasswordGenerator
{
	public static String generate()
	{
		char pass[] = new char[Constants.PASSWORD_LENGTH];
		Random rnd = new Random();

		int j;
		for (int i = 0; i < Constants.PASSWORD_LENGTH; i++)
		{
			j = rnd.nextInt() + 97;
			if (j < 0)
			{
				j = 0 - j;
			}

			j = (j % 26) + 97;

			pass[i] = (char) j;
		}

		return new String(pass);
	}
}
