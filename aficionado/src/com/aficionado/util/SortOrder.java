package com.aficionado.util;

public enum SortOrder
{
	Ascending, Descending, Unsorted
}
