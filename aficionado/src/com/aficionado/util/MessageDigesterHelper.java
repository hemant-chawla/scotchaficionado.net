package com.aficionado.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MessageDigesterHelper
{
	public static String hashCal(String type, String str)
	{
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try
		{
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();

			for (int i = 0; i < messageDigest.length; i++)
			{
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
				{
					hexString.append("0");
				}
				hexString.append(hex);
			}

		}
		catch (NoSuchAlgorithmException nsae)
		{
		}

		return hexString.toString();
	}
	
	public static boolean isEqual(byte[] digesta, byte[] digestb)
	{
		return MessageDigest.isEqual(digesta, digestb);
	}
	
	public static String md5DigestAndHex(String text)
	{
		try
		{
			MessageDigest m = MessageDigest.getInstance("MD5");
			byte[] data = text.getBytes();
			m.update(data, 0, data.length);
			BigInteger i = new BigInteger(1, m.digest());
			return String.format("%1$032x", i);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}
}
