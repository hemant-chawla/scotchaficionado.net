package com.aficionado.util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class Filter implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String name;

	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	private FilterType type;

	private Object value;
	
	public Filter(String name)
	{
		this.name = name;
		this.type = FilterType.Contains;
	}

	public Filter(String name, FilterType type)
	{
		this.name = name;
		this.type = type;
	}
	
	public Filter(String name, FilterType type, Object value)
	{
		this.name = name;
		this.type = type;
		this.value = value;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener)
	{
		this.pcs.addPropertyChangeListener(listener);
	}

	public String getName()
	{
		return name;
	}

	public FilterType getType()
	{
		return type;
	}

	public Object getValue()
	{
		return value;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener)
	{
		this.pcs.removePropertyChangeListener(listener);
	}

	public void setValue(Object value)
	{
		if(value == null && this.value == null)
		{
			return;
		}

		if(value == null || !value.equals(this.value))
		{
			Object oldValue = this.value;
			this.value = value;
			this.pcs.firePropertyChange("value", oldValue, value);
		}
	}
}
