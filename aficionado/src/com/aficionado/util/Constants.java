package com.aficionado.util;

import java.text.SimpleDateFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Constants
{
	private static final String BUNDLE_NAME = "com.aficionado.config.constants";

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

	private static String getString(String key)
	{
		try
		{
			return RESOURCE_BUNDLE.getString(key);
		}
		catch (MissingResourceException e)
		{
			return '!' + key + '!';
		}
	}
	
	public static final long DAYS_REMAINING_TO_EXPIRE_PAC = 90L; 
	
	public static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	
	public static final String CAPEXIL_FROM_EMAIL = "alert@scotchaficionado.net";
	public static final String REGISTRATION_HASH_SEED = "fd34c328c09bf652a6f82870692ec821";
	public static final String FILENAME_HASH_SEED = "1aa236bdc9bead8f1ce7dff9a73f5c6b";
	
	public static final String BASE_APPLICATION_URL = Constants.getString("BASE_APPLICATION_URL");

	public static final String BASE_FOLDER_PATH = Constants.getString("BASE_FOLDER_PATH");
	
	public static final String CONTENT_FOLDER_PATH = BASE_FOLDER_PATH + "/includes";

	public static final int FILE_COPY_BUFFER_SIZE = 4028;
	public static final long USER_CACHE_INTERVAL = 30000;
	public static final int PASSWORD_LENGTH = 10;
	
	public static final String TIME_FORMAT = "dd/MM/yyyy hh:mm:ss aa";
	
	public static final String DATE_FORMAT = "dd MMM yyyy";
	
	public static final int REGISTRATION_EXPIRY_DAYS = 1;
	
	public static final String ERROR_RECEIVER_EMAIL = "hemant16apr@gmail.com";
	public static final String FROM_EMAIL_NAME = "Scotch Aficionado";

	public static final String PLAIN_TRANSCRIPT_REGEX = "(.*?)?(\\d{2}).(\\d{2}).?(\\d{2})?";
	public static final String SRT_TRANSCRIPT_REGEX = "(\\d+)" + "\r?\n" + "(\\d{1,2}):(\\d{1,2}):(\\d{1,2}),(\\d{1,3})";
	public static final String TIME_REGEX = "(\\d+?)\\s*(\\d+?:\\d+?:\\d+?,\\d+?)\\s+-->\\s+(\\d+?:\\d+?:\\d+?,\\d+?)\\s+(.+)";
	public static final String TEXT_ONLY_REGEX = "^[a-z A-Z]";
	public static final String NUMBERS_SPECIAL_CHAR_REGEX = "[0-9+^:\\[\\]]";
}