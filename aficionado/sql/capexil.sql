-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2015 at 06:40 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `capexil`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
`id` int(10) unsigned NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `phone`) VALUES
(1, '989987');

-- --------------------------------------------------------

--
-- Table structure for table `annexure2`
--

CREATE TABLE IF NOT EXISTS `annexure2` (
`id` int(10) unsigned NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipment_id` int(10) unsigned NOT NULL,
  `plant_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE IF NOT EXISTS `certificate` (
`id` int(10) unsigned NOT NULL,
  `approval_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approver_id` int(10) unsigned DEFAULT NULL,
  `date_expiry` datetime DEFAULT NULL,
  `date_issued` datetime DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`id`, `approval_number`, `approver_id`, `date_expiry`, `date_issued`, `description`, `name`, `reference_number`, `status`) VALUES
(30, 'CAPEXIL/NR/23465', 1, '2017-01-24 11:31:05', '2015-01-24 11:31:05', NULL, 'PlantApprovalCertificate', 'CAPEXIL/REF/NR/20025', '1'),
(31, 'CAPEXIL/NR/12523', 1, '2017-02-07 17:33:42', '2015-02-07 17:33:42', NULL, 'ShipmentClearanceCertificate', 'CAPEXIL/REF/SR/11442', '1'),
(32, 'CAPEXIL/NR/12523', NULL, NULL, NULL, NULL, 'Annexure2', NULL, '3'),
(33, 'CAPEXIL/NR/23466', 3, '2017-02-13 10:41:32', '2015-02-13 10:41:32', NULL, 'PlantApprovalCertificate', 'CAPEXIL/REF/NR/20027', '3'),
(34, 'CAPEXIL/NR/12524', 1, '2017-03-03 13:34:49', '2015-03-03 13:34:49', NULL, 'ShipmentClearanceCertificate', 'CAPEXIL/REF/SR/11443', '1'),
(35, 'CAPEXIL/NR/12525', 1, '2017-03-05 12:59:11', '2015-03-05 12:59:11', NULL, 'ShipmentClearanceCertificate', 'CAPEXIL/REF/SR/11444', '1');

-- --------------------------------------------------------

--
-- Table structure for table `continents`
--

CREATE TABLE IF NOT EXISTS `continents` (
  `code` char(2) NOT NULL COMMENT 'Continent code',
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `continents`
--

INSERT INTO `continents` (`code`, `name`) VALUES
('AF', 'Africa'),
('AN', 'Antarctica'),
('AS', 'Asia'),
('EU', 'Europe'),
('NA', 'North America'),
('OC', 'Oceania'),
('SA', 'South America');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(2) NOT NULL COMMENT 'Two-letter country code (ISO 3166-1 alpha-2)',
  `name` varchar(64) NOT NULL COMMENT 'English country name',
  `full_name` varchar(128) NOT NULL COMMENT 'Full English country name',
  `iso3` char(3) NOT NULL COMMENT 'Three-letter country code (ISO 3166-1 alpha-3)',
  `continent_code` varchar(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `full_name`, `iso3`, `continent_code`) VALUES
(1, 'AD', 'Andorra', 'Principality of Andorra', 'AND', 'EU'),
(2, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', 'AS'),
(3, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', 'AS'),
(4, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', 'NA'),
(5, 'AI', 'Anguilla', 'Anguilla', 'AIA', 'NA'),
(6, 'AL', 'Albania', 'Republic of Albania', 'ALB', 'EU'),
(7, 'AM', 'Armenia', 'Republic of Armenia', 'ARM', 'AS'),
(8, 'AN', 'Netherlands Antilles', 'Netherlands Antilles', 'ANT', 'NA'),
(9, 'AO', 'Angola', 'Republic of Angola', 'AGO', 'AF'),
(10, 'AQ', 'Antarctica', 'Antarctica (the territory South of 60 deg S)', 'ATA', 'AN'),
(11, 'AR', 'Argentina', 'Argentine Republic', 'ARG', 'SA'),
(12, 'AS', 'American Samoa', 'American Samoa', 'ASM', 'OC'),
(13, 'AT', 'Austria', 'Republic of Austria', 'AUT', 'EU'),
(14, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', 'OC'),
(15, 'AW', 'Aruba', 'Aruba', 'ABW', 'NA'),
(16, 'AX', 'Åland', 'Åland Islands', 'ALA', 'EU'),
(17, 'AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', 'AS'),
(18, 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', 'EU'),
(19, 'BB', 'Barbados', 'Barbados', 'BRB', 'NA'),
(20, 'BD', 'Bangladesh', 'People''s Republic of Bangladesh', 'BGD', 'AS'),
(21, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', 'EU'),
(22, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', 'AF'),
(23, 'BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', 'EU'),
(24, 'BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', 'AS'),
(25, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', 'AF'),
(26, 'BJ', 'Benin', 'Republic of Benin', 'BEN', 'AF'),
(27, 'BL', 'Saint Barthélemy', 'Saint Barthelemy', 'BLM', 'NA'),
(28, 'BM', 'Bermuda', 'Bermuda', 'BMU', 'NA'),
(29, 'BN', 'Brunei Darussalam', 'Brunei Darussalam', 'BRN', 'AS'),
(30, 'BO', 'Bolivia', 'Republic of Bolivia', 'BOL', 'SA'),
(31, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', 'SA'),
(32, 'BS', 'Bahamas', 'Commonwealth of the Bahamas', 'BHS', 'NA'),
(33, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', 'AS'),
(34, 'BV', 'Bouvet Island', 'Bouvet Island (Bouvetoya)', 'BVT', 'AN'),
(35, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', 'AF'),
(36, 'BY', 'Belarus', 'Republic of Belarus', 'BLR', 'EU'),
(37, 'BZ', 'Belize', 'Belize', 'BLZ', 'NA'),
(38, 'CA', 'Canada', 'Canada', 'CAN', 'NA'),
(39, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', 'AS'),
(40, 'CD', 'Congo (Kinshasa)', 'Democratic Republic of the Congo', 'COD', 'AF'),
(41, 'CF', 'Central African Republic', 'Central African Republic', 'CAF', 'AF'),
(42, 'CG', 'Congo (Brazzaville)', 'Republic of the Congo', 'COG', 'AF'),
(43, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', 'EU'),
(44, 'CI', 'Côte d''Ivoire', 'Republic of Cote d''Ivoire', 'CIV', 'AF'),
(45, 'CK', 'Cook Islands', 'Cook Islands', 'COK', 'OC'),
(46, 'CL', 'Chile', 'Republic of Chile', 'CHL', 'SA'),
(47, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', 'AF'),
(48, 'CN', 'China', 'People''s Republic of China', 'CHN', 'AS'),
(49, 'CO', 'Colombia', 'Republic of Colombia', 'COL', 'SA'),
(50, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', 'NA'),
(51, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', 'NA'),
(52, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', 'AF'),
(53, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', 'AS'),
(54, 'CY', 'Cyprus', 'Republic of Cyprus', 'CYP', 'AS'),
(55, 'CZ', 'Czech Republic', 'Czech Republic', 'CZE', 'EU'),
(56, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', 'EU'),
(57, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', 'AF'),
(58, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', 'EU'),
(59, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', 'NA'),
(60, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', 'NA'),
(61, 'DZ', 'Algeria', 'People''s Democratic Republic of Algeria', 'DZA', 'AF'),
(62, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', 'SA'),
(63, 'EE', 'Estonia', 'Republic of Estonia', 'EST', 'EU'),
(64, 'EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', 'AF'),
(65, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', 'AF'),
(66, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', 'AF'),
(67, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', 'EU'),
(68, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', 'AF'),
(69, 'FI', 'Finland', 'Republic of Finland', 'FIN', 'EU'),
(70, 'FJ', 'Fiji', 'Republic of the Fiji Islands', 'FJI', 'OC'),
(71, 'FK', 'Falkland Islands', 'Falkland Islands (Malvinas)', 'FLK', 'SA'),
(72, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', 'OC'),
(73, 'FO', 'Faroe Islands', 'Faroe Islands', 'FRO', 'EU'),
(74, 'FR', 'France', 'French Republic', 'FRA', 'EU'),
(75, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', 'AF'),
(76, 'GB', 'United Kingdom', 'United Kingdom of Great Britain & Northern Ireland', 'GBR', 'EU'),
(77, 'GD', 'Grenada', 'Grenada', 'GRD', 'NA'),
(78, 'GE', 'Georgia', 'Georgia', 'GEO', 'AS'),
(79, 'GF', 'French Guiana', 'French Guiana', 'GUF', 'SA'),
(80, 'GG', 'Guernsey', 'Bailiwick of Guernsey', 'GGY', 'EU'),
(81, 'GH', 'Ghana', 'Republic of Ghana', 'GHA', 'AF'),
(82, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', 'EU'),
(83, 'GL', 'Greenland', 'Greenland', 'GRL', 'NA'),
(84, 'GM', 'Gambia', 'Republic of the Gambia', 'GMB', 'AF'),
(85, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', 'AF'),
(86, 'GP', 'Guadeloupe', 'Guadeloupe', 'GLP', 'NA'),
(87, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', 'AF'),
(88, 'GR', 'Greece', 'Hellenic Republic Greece', 'GRC', 'EU'),
(89, 'GS', 'South Georgia and South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', 'AN'),
(90, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', 'NA'),
(91, 'GU', 'Guam', 'Guam', 'GUM', 'OC'),
(92, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', 'AF'),
(93, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', 'SA'),
(94, 'HK', 'Hong Kong', 'Hong Kong Special Administrative Region of China', 'HKG', 'AS'),
(95, 'HM', 'Heard and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', 'AN'),
(96, 'HN', 'Honduras', 'Republic of Honduras', 'HND', 'NA'),
(97, 'HR', 'Croatia', 'Republic of Croatia', 'HRV', 'EU'),
(98, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', 'NA'),
(99, 'HU', 'Hungary', 'Republic of Hungary', 'HUN', 'EU'),
(100, 'ID', 'Indonesia', 'Republic of Indonesia', 'IDN', 'AS'),
(101, 'IE', 'Ireland', 'Ireland', 'IRL', 'EU'),
(102, 'IL', 'Israel', 'State of Israel', 'ISR', 'AS'),
(103, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', 'EU'),
(104, 'IN', 'India', 'Republic of India', 'IND', 'AS'),
(105, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory (Chagos Archipelago)', 'IOT', 'AS'),
(106, 'IQ', 'Iraq', 'Republic of Iraq', 'IRQ', 'AS'),
(107, 'IR', 'Iran', 'Islamic Republic of Iran', 'IRN', 'AS'),
(108, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', 'EU'),
(109, 'IT', 'Italy', 'Italian Republic', 'ITA', 'EU'),
(110, 'JE', 'Jersey', 'Bailiwick of Jersey', 'JEY', 'EU'),
(111, 'JM', 'Jamaica', 'Jamaica', 'JAM', 'NA'),
(112, 'JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', 'AS'),
(113, 'JP', 'Japan', 'Japan', 'JPN', 'AS'),
(114, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', 'AF'),
(115, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', 'KGZ', 'AS'),
(116, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', 'AS'),
(117, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', 'OC'),
(118, 'KM', 'Comoros', 'Union of the Comoros', 'COM', 'AF'),
(119, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Kitts and Nevis', 'KNA', 'NA'),
(120, 'KP', 'Korea, North', 'Democratic People''s Republic of Korea', 'PRK', 'AS'),
(121, 'KR', 'Korea, South', 'Republic of Korea', 'KOR', 'AS'),
(122, 'KW', 'Kuwait', 'State of Kuwait', 'KWT', 'AS'),
(123, 'KY', 'Cayman Islands', 'Cayman Islands', 'CYM', 'NA'),
(124, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', 'AS'),
(125, 'LA', 'Laos', 'Lao People''s Democratic Republic', 'LAO', 'AS'),
(126, 'LB', 'Lebanon', 'Lebanese Republic', 'LBN', 'AS'),
(127, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', 'NA'),
(128, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', 'EU'),
(129, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', 'AS'),
(130, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', 'AF'),
(131, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', 'AF'),
(132, 'LT', 'Lithuania', 'Republic of Lithuania', 'LTU', 'EU'),
(133, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', 'EU'),
(134, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', 'EU'),
(135, 'LY', 'Libya', 'Libyan Arab Jamahiriya', 'LBY', 'AF'),
(136, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', 'AF'),
(137, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', 'EU'),
(138, 'MD', 'Moldova', 'Republic of Moldova', 'MDA', 'EU'),
(139, 'ME', 'Montenegro', 'Republic of Montenegro', 'MNE', 'EU'),
(140, 'MF', 'Saint Martin (French part)', 'Saint Martin', 'MAF', 'NA'),
(141, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', 'AF'),
(142, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', 'OC'),
(143, 'MK', 'Macedonia', 'Republic of Macedonia', 'MKD', 'EU'),
(144, 'ML', 'Mali', 'Republic of Mali', 'MLI', 'AF'),
(145, 'MM', 'Myanmar', 'Union of Myanmar', 'MMR', 'AS'),
(146, 'MN', 'Mongolia', 'Mongolia', 'MNG', 'AS'),
(147, 'MO', 'Macau', 'Macao Special Administrative Region of China', 'MAC', 'AS'),
(148, 'MP', 'Northern Mariana Islands', 'Commonwealth of the Northern Mariana Islands', 'MNP', 'OC'),
(149, 'MQ', 'Martinique', 'Martinique', 'MTQ', 'NA'),
(150, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', 'AF'),
(151, 'MS', 'Montserrat', 'Montserrat', 'MSR', 'NA'),
(152, 'MT', 'Malta', 'Republic of Malta', 'MLT', 'EU'),
(153, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', 'AF'),
(154, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', 'AS'),
(155, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', 'AF'),
(156, 'MX', 'Mexico', 'United Mexican States', 'MEX', 'NA'),
(157, 'MY', 'Malaysia', 'Malaysia', 'MYS', 'AS'),
(158, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', 'AF'),
(159, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', 'AF'),
(160, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', 'OC'),
(161, 'NE', 'Niger', 'Republic of Niger', 'NER', 'AF'),
(162, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', 'OC'),
(163, 'NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', 'AF'),
(164, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', 'NA'),
(165, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', 'EU'),
(166, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', 'EU'),
(167, 'NP', 'Nepal', 'State of Nepal', 'NPL', 'AS'),
(168, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', 'OC'),
(169, 'NU', 'Niue', 'Niue', 'NIU', 'OC'),
(170, 'NZ', 'New Zealand', 'New Zealand', 'NZL', 'OC'),
(171, 'OM', 'Oman', 'Sultanate of Oman', 'OMN', 'AS'),
(172, 'PA', 'Panama', 'Republic of Panama', 'PAN', 'NA'),
(173, 'PE', 'Peru', 'Republic of Peru', 'PER', 'SA'),
(174, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', 'OC'),
(175, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', 'OC'),
(176, 'PH', 'Philippines', 'Republic of the Philippines', 'PHL', 'AS'),
(177, 'PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', 'AS'),
(178, 'PL', 'Poland', 'Republic of Poland', 'POL', 'EU'),
(179, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', 'NA'),
(180, 'PN', 'Pitcairn', 'Pitcairn Islands', 'PCN', 'OC'),
(181, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', 'NA'),
(182, 'PS', 'Palestine', 'Occupied Palestinian Territory', 'PSE', 'AS'),
(183, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', 'EU'),
(184, 'PW', 'Palau', 'Republic of Palau', 'PLW', 'OC'),
(185, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', 'SA'),
(186, 'QA', 'Qatar', 'State of Qatar', 'QAT', 'AS'),
(187, 'RE', 'Reunion', 'Reunion', 'REU', 'AF'),
(188, 'RO', 'Romania', 'Romania', 'ROU', 'EU'),
(189, 'RS', 'Serbia', 'Republic of Serbia', 'SRB', 'EU'),
(190, 'RU', 'Russian Federation', 'Russian Federation', 'RUS', 'EU'),
(191, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', 'AF'),
(192, 'SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', 'AS'),
(193, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', 'OC'),
(194, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', 'AF'),
(195, 'SD', 'Sudan', 'Republic of Sudan', 'SDN', 'AF'),
(196, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', 'EU'),
(197, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', 'AS'),
(198, 'SH', 'Saint Helena', 'Saint Helena', 'SHN', 'AF'),
(199, 'SI', 'Slovenia', 'Republic of Slovenia', 'SVN', 'EU'),
(200, 'SJ', 'Svalbard and Jan Mayen Islands', 'Svalbard & Jan Mayen Islands', 'SJM', 'EU'),
(201, 'SK', 'Slovakia', 'Slovakia (Slovak Republic)', 'SVK', 'EU'),
(202, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', 'AF'),
(203, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', 'EU'),
(204, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', 'AF'),
(205, 'SO', 'Somalia', 'Somali Republic', 'SOM', 'AF'),
(206, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', 'SA'),
(207, 'ST', 'Sao Tome and Principe', 'Democratic Republic of Sao Tome and Principe', 'STP', 'AF'),
(208, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', 'NA'),
(209, 'SY', 'Syria', 'Syrian Arab Republic', 'SYR', 'AS'),
(210, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', 'AF'),
(211, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', 'NA'),
(212, 'TD', 'Chad', 'Republic of Chad', 'TCD', 'AF'),
(213, 'TF', 'French Southern Lands', 'French Southern Territories', 'ATF', 'AN'),
(214, 'TG', 'Togo', 'Togolese Republic', 'TGO', 'AF'),
(215, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', 'AS'),
(216, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', 'AS'),
(217, 'TK', 'Tokelau', 'Tokelau', 'TKL', 'OC'),
(218, 'TL', 'Timor-Leste', 'Democratic Republic of Timor-Leste', 'TLS', 'AS'),
(219, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', 'AS'),
(220, 'TN', 'Tunisia', 'Tunisian Republic', 'TUN', 'AF'),
(221, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', 'OC'),
(222, 'TR', 'Turkey', 'Republic of Turkey', 'TUR', 'AS'),
(223, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', 'NA'),
(224, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', 'OC'),
(225, 'TW', 'Taiwan', 'Taiwan', 'TWN', 'AS'),
(226, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', 'AF'),
(227, 'UA', 'Ukraine', 'Ukraine', 'UKR', 'EU'),
(228, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', 'AF'),
(229, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', 'OC'),
(230, 'US', 'United States of America', 'United States of America', 'USA', 'NA'),
(231, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', 'SA'),
(232, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', 'AS'),
(233, 'VA', 'Vatican City', 'Holy See (Vatican City State)', 'VAT', 'EU'),
(234, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', 'NA'),
(235, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', 'SA'),
(236, 'VG', 'Virgin Islands, British', 'British Virgin Islands', 'VGB', 'NA'),
(237, 'VI', 'Virgin Islands, U.S.', 'United States Virgin Islands', 'VIR', 'NA'),
(238, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', 'AS'),
(239, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', 'OC'),
(240, 'WF', 'Wallis and Futuna Islands', 'Wallis and Futuna', 'WLF', 'OC'),
(241, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', 'OC'),
(242, 'YE', 'Yemen', 'Yemen', 'YEM', 'AS'),
(243, 'YT', 'Mayotte', 'Mayotte', 'MYT', 'AF'),
(244, 'ZA', 'South Africa', 'Republic of South Africa', 'ZAF', 'AF'),
(245, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', 'AF'),
(246, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', 'AF');

-- --------------------------------------------------------

--
-- Table structure for table `declaration`
--

CREATE TABLE IF NOT EXISTS `declaration` (
`id` int(10) unsigned NOT NULL,
  `plant_id` int(10) unsigned NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipment_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(8) NOT NULL DEFAULT '0',
  `state_id` int(8) DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `state_id`, `name`) VALUES
(1, 1, 'North Andaman'),
(2, 1, 'South Andaman'),
(3, 1, 'Nicobar'),
(4, 2, 'Adilabad'),
(5, 2, 'Anantapur'),
(6, 2, 'Chittoor'),
(7, 2, 'East Godavari'),
(8, 2, 'Guntur'),
(9, 2, 'Hyderabad'),
(10, 2, 'Karimnagar'),
(11, 2, 'Khammam'),
(12, 2, 'Krishna'),
(13, 2, 'Kurnool'),
(14, 2, 'Mahbubnagar'),
(15, 2, 'Medak'),
(16, 2, 'Nalgonda'),
(17, 2, 'Nizamabad'),
(18, 2, 'Prakasam'),
(19, 2, 'Ranga Reddy'),
(20, 2, 'Srikakulam'),
(21, 2, 'Sri Potti Sri Ramulu Nellore'),
(22, 2, 'Vishakhapatnam'),
(23, 2, 'Vizianagaram'),
(24, 2, 'Warangal'),
(25, 2, 'West Godavari'),
(26, 2, 'Cudappah'),
(27, 3, 'Anjaw'),
(28, 3, 'Changlang'),
(29, 3, 'East Siang'),
(30, 3, 'East Kameng'),
(31, 3, 'Kurung Kumey'),
(32, 3, 'Lohit'),
(33, 3, 'Lower Dibang Valley'),
(34, 3, 'Lower Subansiri'),
(35, 3, 'Papum Pare'),
(36, 3, 'Tawang'),
(37, 3, 'Tirap'),
(38, 3, 'Dibang Valley'),
(39, 3, 'Upper Siang'),
(40, 3, 'Upper Subansiri'),
(41, 3, 'West Kameng'),
(42, 3, 'West Siang'),
(43, 4, 'Baksa'),
(44, 4, 'Barpeta'),
(45, 4, 'Bongaigaon'),
(46, 4, 'Cachar'),
(47, 4, 'Chirang'),
(48, 4, 'Darrang'),
(49, 4, 'Dhemaji'),
(50, 4, 'Dima Hasao'),
(51, 4, 'Dhubri'),
(52, 4, 'Dibrugarh'),
(53, 4, 'Goalpara'),
(54, 4, 'Golaghat'),
(55, 4, 'Hailakandi'),
(56, 4, 'Jorhat'),
(57, 4, 'Kamrup'),
(58, 4, 'Kamrup Metropolitan'),
(59, 4, 'Karbi Anglong'),
(60, 4, 'Karimganj'),
(61, 4, 'Kokrajhar'),
(62, 4, 'Lakhimpur'),
(63, 4, 'Morigaon'),
(64, 4, 'Nagaon'),
(65, 4, 'Nalbari'),
(66, 4, 'Sivasagar'),
(67, 4, 'Sonitpur'),
(68, 4, 'Tinsukia'),
(69, 4, 'Udalguri'),
(70, 5, 'Araria'),
(71, 5, 'Arwal'),
(72, 5, 'Aurangabad'),
(73, 5, 'Banka'),
(74, 5, 'Begusarai'),
(75, 5, 'Bhagalpur'),
(76, 5, 'Bhojpur'),
(77, 5, 'Buxar'),
(78, 5, 'Darbhanga'),
(79, 5, 'East Champaran'),
(80, 5, 'Gaya'),
(81, 5, 'Gopalganj'),
(82, 5, 'Jamui'),
(83, 5, 'Jehanabad'),
(84, 5, 'Kaimur'),
(85, 5, 'Katihar'),
(86, 5, 'Khagaria'),
(87, 5, 'Kishanganj'),
(88, 5, 'Lakhisarai'),
(89, 5, 'Madhepura'),
(90, 5, 'Madhubani'),
(91, 5, 'Munger'),
(92, 5, 'Muzaffarpur'),
(93, 5, 'Nalanda'),
(94, 5, 'Nawada'),
(95, 5, 'Patna'),
(96, 5, 'Purnia'),
(97, 5, 'Rohtas'),
(98, 5, 'Saharsa'),
(99, 5, 'Samastipur'),
(100, 5, 'Saran'),
(101, 5, 'Sheikhpura'),
(102, 5, 'Sheohar'),
(103, 5, 'Sitamarhi'),
(104, 5, 'Siwan'),
(105, 5, 'Supaul'),
(106, 6, 'Chandigarh'),
(107, 7, 'Bastar'),
(108, 7, 'Bijapur'),
(109, 7, 'Bilaspur'),
(110, 7, 'Dantewada'),
(111, 7, 'Dhamtari'),
(112, 7, 'Durg'),
(113, 7, 'Jashpur'),
(114, 7, 'Janjgir-Champa'),
(115, 7, 'Korba'),
(116, 7, 'Koriya'),
(117, 7, 'Kanker'),
(118, 7, 'Kabirdham (formerly Kawardha)'),
(119, 7, 'Mahasamund'),
(120, 7, 'Narayanpur'),
(121, 7, 'Raigarh'),
(122, 7, 'Rajnandgaon'),
(123, 7, 'Raipur'),
(124, 7, 'Surguja'),
(125, 8, 'Dadra and Nagar Haveli'),
(126, 9, 'Daman'),
(127, 9, 'Diu'),
(128, 10, 'Central Delhi'),
(129, 10, 'East Delhi'),
(130, 10, 'New Delhi'),
(131, 10, 'North Delhi'),
(132, 10, 'North East Delhi'),
(133, 10, 'North West Delhi'),
(134, 10, 'South Delhi'),
(135, 10, 'South West Delhi'),
(136, 10, 'West Delhi'),
(137, 11, 'North Goa'),
(138, 11, 'South Goa'),
(139, 12, 'Ahmedabad'),
(140, 12, 'Amreli district'),
(141, 12, 'Anand'),
(142, 12, 'Banaskantha'),
(143, 12, 'Bharuch'),
(144, 12, 'Bhavnagar'),
(145, 12, 'Dahod'),
(146, 12, 'The Dangs'),
(147, 12, 'Gandhinagar'),
(148, 12, 'Jamnagar'),
(149, 12, 'Junagadh'),
(150, 12, 'Kutch'),
(151, 12, 'Kheda'),
(152, 12, 'Mehsana'),
(153, 12, 'Narmada'),
(154, 12, 'Navsari'),
(155, 12, 'Patan'),
(156, 12, 'Panchmahal'),
(157, 12, 'Porbandar'),
(158, 12, 'Rajkot'),
(159, 12, 'Sabarkantha'),
(160, 12, 'Surendranagar'),
(161, 12, 'Surat'),
(162, 12, 'Tapi'),
(163, 12, 'Vadodara'),
(164, 12, 'Valsad'),
(165, 13, 'Ambala'),
(166, 13, 'Bhiwani'),
(167, 13, 'Faridabad'),
(168, 13, 'Fatehabad'),
(169, 13, 'Gurgaon'),
(170, 13, 'Hissar'),
(171, 13, 'Jhajjar'),
(172, 13, 'Jind'),
(173, 13, 'Karnal'),
(174, 13, 'Kaithal'),
(175, 13, 'Kurukshetra'),
(176, 13, 'Mahendragarh'),
(177, 13, 'Mewat'),
(178, 13, 'Palwal'),
(179, 13, 'Panchkula'),
(180, 13, 'Panipat'),
(181, 13, 'Rewari'),
(182, 13, 'Rohtak'),
(183, 13, 'Sirsa'),
(184, 13, 'Sonipat'),
(185, 13, 'Yamuna Nagar'),
(186, 14, 'Bilaspur'),
(187, 14, 'Chamba'),
(188, 14, 'Hamirpur'),
(189, 14, 'Kangra'),
(190, 14, 'Kinnaur'),
(191, 14, 'Kullu'),
(192, 14, 'Lahaul and Spiti'),
(193, 14, 'Mandi'),
(194, 14, 'Shimla'),
(195, 14, 'Sirmaur'),
(196, 14, 'Solan'),
(197, 14, 'Una'),
(198, 15, 'Anantnag'),
(199, 15, 'Badgam'),
(200, 15, 'Bandipora'),
(201, 15, 'Baramulla'),
(202, 15, 'Doda'),
(203, 15, 'Ganderbal'),
(204, 15, 'Jammu'),
(205, 15, 'Kargil'),
(206, 15, 'Kathua'),
(207, 15, 'Kishtwar'),
(208, 15, 'Kupwara'),
(209, 15, 'Kulgam'),
(210, 15, 'Leh'),
(211, 15, 'Poonch'),
(212, 15, 'Pulwama'),
(213, 15, 'Rajouri'),
(214, 15, 'Ramban'),
(215, 15, 'Reasi'),
(216, 15, 'Samba'),
(217, 15, 'Shopian'),
(218, 15, 'Srinagar'),
(219, 15, 'Udhampur'),
(220, 16, 'Bokaro'),
(221, 16, 'Chatra'),
(222, 16, 'Deoghar'),
(223, 16, 'Dhanbad'),
(224, 16, 'Dumka'),
(225, 16, 'East Singhbhum'),
(226, 16, 'Garhwa'),
(227, 16, 'Giridih'),
(228, 16, 'Godda'),
(229, 16, 'Gumla'),
(230, 16, 'Hazaribag'),
(231, 16, 'Jamtara'),
(232, 16, 'Khunti'),
(233, 16, 'Koderma'),
(234, 16, 'Latehar'),
(235, 16, 'Lohardaga'),
(236, 16, 'Pakur'),
(237, 16, 'Palamu'),
(238, 16, 'Ramgarh'),
(239, 16, 'Ranchi'),
(240, 16, 'Sahibganj'),
(241, 16, 'Seraikela Kharsawan'),
(242, 16, 'Simdega'),
(243, 16, 'West Singhbhum'),
(244, 17, 'Bagalkot'),
(245, 17, 'Bangalore Rural'),
(246, 17, 'Bangalore Urban'),
(247, 17, 'Belgaum'),
(248, 17, 'Bellary'),
(249, 17, 'Bidar'),
(250, 17, 'Bijapur'),
(251, 17, 'Chamarajnagar'),
(252, 17, 'Chikkamagaluru'),
(253, 17, 'Chikkaballapur'),
(254, 17, 'Chitradurga'),
(255, 17, 'Davanagere'),
(256, 17, 'Dharwad'),
(257, 17, 'Dakshina Kannada'),
(258, 17, 'Gadag'),
(259, 17, 'Gulbarga'),
(260, 17, 'Hassan'),
(261, 17, 'Haveri district'),
(262, 17, 'Kodagu'),
(263, 17, 'Kolar'),
(264, 17, 'Koppal'),
(265, 17, 'Mandya'),
(266, 17, 'Mysore'),
(267, 17, 'Raichur'),
(268, 17, 'Shimoga'),
(269, 17, 'Tumkur'),
(270, 17, 'Udupi'),
(271, 17, 'Uttara Kannada'),
(272, 17, 'Ramanagara'),
(273, 17, 'Yadgir'),
(274, 18, 'Alappuzha'),
(275, 18, 'Ernakulam'),
(276, 18, 'Idukki'),
(277, 18, 'Kannur'),
(278, 18, 'Kasaragod'),
(279, 18, 'Kollam'),
(280, 18, 'Kottayam'),
(281, 18, 'Kozhikode'),
(282, 18, 'Malappuram'),
(283, 18, 'Palakkad'),
(284, 18, 'Pathanamthitta'),
(285, 18, 'Thrissur'),
(286, 18, 'Thiruvananthapuram'),
(287, 18, 'Wayanad'),
(288, 19, 'Lakshadweep'),
(289, 20, 'Agar'),
(290, 20, 'Alirajpur'),
(291, 20, 'Anuppur'),
(292, 20, 'Ashok Nagar'),
(293, 20, 'Balaghat'),
(294, 20, 'Barwani'),
(295, 20, 'Betul'),
(296, 20, 'Bhind'),
(297, 20, 'Bhopal'),
(298, 20, 'Burhanpur'),
(299, 20, 'Chhatarpur'),
(300, 20, 'Chhindwara'),
(301, 20, 'Damoh'),
(302, 20, 'Datia'),
(303, 20, 'Dewas'),
(304, 20, 'Dhar'),
(305, 20, 'Dindori'),
(306, 20, 'Guna'),
(307, 20, 'Gwalior'),
(308, 20, 'Harda'),
(309, 20, 'Hoshangabad'),
(310, 20, 'Indore'),
(311, 20, 'Jabalpur'),
(312, 20, 'Jhabua'),
(313, 20, 'Katni'),
(314, 20, 'Khandwa (East Nimar)'),
(315, 20, 'Khargone (West Nimar)'),
(316, 20, 'Mandla'),
(317, 20, 'Mandsaur'),
(318, 20, 'Morena'),
(319, 20, 'Narsinghpur'),
(320, 20, 'Neemuch'),
(321, 20, 'Panna'),
(322, 20, 'Raisen'),
(323, 20, 'Rajgarh'),
(324, 20, 'Ratlam'),
(325, 20, 'Rewa'),
(326, 20, 'Sagar'),
(327, 20, 'Satna'),
(328, 20, 'Sehore'),
(329, 20, 'Seoni'),
(330, 20, 'Shahdol'),
(331, 20, 'Shajapur'),
(332, 20, 'Sheopur'),
(333, 20, 'Shivpuri'),
(334, 20, 'Sidhi'),
(335, 20, 'Singrauli'),
(336, 20, 'Tikamgarh'),
(337, 20, 'Ujjain'),
(338, 20, 'Umaria'),
(339, 20, 'Vidisha'),
(340, 21, 'Ahmednagar'),
(341, 21, 'Akola'),
(342, 21, 'Amravati'),
(343, 21, 'Aurangabad'),
(344, 21, 'Beed'),
(345, 21, 'Bhandara'),
(346, 21, 'Buldhana'),
(347, 21, 'Chandrapur'),
(348, 21, 'Dhule'),
(349, 21, 'Gadchiroli'),
(350, 21, 'Gondia'),
(351, 21, 'Hingoli'),
(352, 21, 'Jalgaon'),
(353, 21, 'Jalna'),
(354, 21, 'Kolhapur'),
(355, 21, 'Latur'),
(356, 21, 'Mumbai City'),
(357, 21, 'Mumbai suburban'),
(358, 21, 'Nanded'),
(359, 21, 'Nandurbar'),
(360, 21, 'Nagpur'),
(361, 21, 'Nashik'),
(362, 21, 'Osmanabad'),
(363, 21, 'Parbhani'),
(364, 21, 'Pune'),
(365, 21, 'Raigad'),
(366, 21, 'Ratnagiri'),
(367, 21, 'Sangli'),
(368, 21, 'Satara'),
(369, 21, 'Sindhudurg'),
(370, 21, 'Solapur'),
(371, 21, 'Thane'),
(372, 21, 'Wardha'),
(373, 21, 'Washim'),
(374, 21, 'Yavatmal'),
(375, 22, 'Bishnupur'),
(376, 22, 'Churachandpur'),
(377, 22, 'Chandel'),
(378, 22, 'Imphal East'),
(379, 22, 'Senapati'),
(380, 22, 'Tamenglong'),
(381, 22, 'Thoubal'),
(382, 22, 'Ukhrul'),
(383, 22, 'Imphal West'),
(384, 23, 'East Garo Hills'),
(385, 23, 'East Khasi Hills'),
(386, 23, 'Jaintia Hills'),
(387, 23, 'Ri Bhoi'),
(388, 23, 'South Garo Hills'),
(389, 23, 'West Garo Hills'),
(390, 23, 'West Khasi Hills'),
(391, 24, 'Aizawl'),
(392, 24, 'Champhai'),
(393, 24, 'Kolasib'),
(394, 24, 'Lawngtlai'),
(395, 24, 'Lunglei'),
(396, 24, 'Mamit'),
(397, 24, 'Saiha'),
(398, 24, 'Serchhip'),
(399, 25, 'Dimapur'),
(400, 25, 'Kiphire'),
(401, 25, 'Kohima'),
(402, 25, 'Longleng'),
(403, 25, 'Mokokchung'),
(404, 25, 'Mon'),
(405, 25, 'Peren'),
(406, 25, 'Phek'),
(407, 25, 'Tuensang'),
(408, 25, 'Wokha'),
(409, 25, 'Zunheboto'),
(410, 26, 'Angul'),
(411, 26, 'Boudh (Bauda)'),
(412, 26, 'Bhadrak'),
(413, 26, 'Balangir'),
(414, 26, 'Bargarh (Baragarh)'),
(415, 26, 'Balasore'),
(416, 26, 'Cuttack'),
(417, 26, 'Debagarh (Deogarh)'),
(418, 26, 'Dhenkanal'),
(419, 26, 'Ganjam'),
(420, 26, 'Gajapati'),
(421, 26, 'Jharsuguda'),
(422, 26, 'Jajpur'),
(423, 26, 'Jagatsinghpur'),
(424, 26, 'Khordha'),
(425, 26, 'Kendujhar (Keonjhar)'),
(426, 26, 'Kalahandi'),
(427, 26, 'Kandhamal'),
(428, 26, 'Koraput'),
(429, 26, 'Kendrapara'),
(430, 26, 'Malkangiri'),
(431, 26, 'Mayurbhanj'),
(432, 26, 'Nabarangpur'),
(433, 26, 'Nuapada'),
(434, 26, 'Nayagarh'),
(435, 26, 'Puri'),
(436, 26, 'Rayagada'),
(437, 26, 'Sambalpur'),
(438, 26, 'Subarnapur (Sonepur)'),
(439, 26, 'Sundergarh'),
(440, 27, 'Karaikal'),
(441, 27, 'Mahe'),
(442, 27, 'Pondicherry'),
(443, 27, 'Yanam'),
(444, 28, 'Amritsar'),
(445, 28, 'Barnala'),
(446, 28, 'Bathinda'),
(447, 28, 'Firozpur'),
(448, 28, 'Faridkot'),
(449, 28, 'Fatehgarh Sahib'),
(450, 28, 'Fazilka[6]'),
(451, 28, 'Gurdaspur'),
(452, 28, 'Hoshiarpur'),
(453, 28, 'Jalandhar'),
(454, 28, 'Kapurthala'),
(455, 28, 'Ludhiana'),
(456, 28, 'Mansa'),
(457, 28, 'Moga'),
(458, 28, 'Sri Muktsar Sahib'),
(459, 28, 'Pathankot'),
(460, 28, 'Patiala'),
(461, 28, 'Rupnagar'),
(462, 28, 'Ajitgarh (Mohali)'),
(463, 28, 'Sangrur'),
(464, 28, 'Shahid Bhagat Singh Nagar'),
(465, 28, 'Tarn Taran'),
(466, 29, 'Ajmer'),
(467, 29, 'Alwar'),
(468, 29, 'Bikaner'),
(469, 29, 'Barmer'),
(470, 29, 'Banswara'),
(471, 29, 'Bharatpur'),
(472, 29, 'Baran'),
(473, 29, 'Bundi'),
(474, 29, 'Bhilwara'),
(475, 29, 'Churu'),
(476, 29, 'Chittorgarh'),
(477, 29, 'Dausa'),
(478, 29, 'Dholpur'),
(479, 29, 'Dungapur'),
(480, 29, 'Ganganagar'),
(481, 29, 'Hanumangarh'),
(482, 29, 'Jhunjhunu'),
(483, 29, 'Jalore'),
(484, 29, 'Jodhpur'),
(485, 29, 'Jaipur'),
(486, 29, 'Jaisalmer'),
(487, 29, 'Jhalawar'),
(488, 29, 'Karauli'),
(489, 29, 'Kota'),
(490, 29, 'Nagaur'),
(491, 29, 'Pali'),
(492, 29, 'Pratapgarh'),
(493, 29, 'Rajsamand'),
(494, 29, 'Sikar'),
(495, 29, 'Sawai Madhopur'),
(496, 29, 'Sirohi'),
(497, 29, 'Tonk'),
(498, 29, 'Udaipur'),
(499, 30, 'East Sikkim'),
(500, 30, 'North Sikkim'),
(501, 30, 'South Sikkim'),
(502, 30, 'West Sikkim'),
(503, 31, 'Ariyalur'),
(504, 31, 'Chennai'),
(505, 31, 'Coimbatore'),
(506, 31, 'Cuddalore'),
(507, 31, 'Dharmapuri'),
(508, 31, 'Dindigul'),
(509, 31, 'Erode'),
(510, 31, 'Kanchipuram'),
(511, 31, 'Kanyakumari'),
(512, 31, 'Karur'),
(513, 31, 'Krishnagiri'),
(514, 31, 'Madurai'),
(515, 31, 'Nagapattinam'),
(516, 31, 'Nilgiris'),
(517, 31, 'Namakkal'),
(518, 31, 'Perambalur'),
(519, 31, 'Pudukkottai'),
(520, 31, 'Ramanathapuram'),
(521, 31, 'Salem'),
(522, 31, 'Sivaganga'),
(523, 31, 'Tirupur'),
(524, 31, 'Tiruchirappalli'),
(525, 31, 'Theni'),
(526, 31, 'Tirunelveli'),
(527, 31, 'Thanjavur'),
(528, 31, 'Thoothukudi'),
(529, 31, 'Tiruvallur'),
(530, 31, 'Tiruvarur'),
(531, 31, 'Tiruvannamalai'),
(532, 31, 'Vellore'),
(533, 31, 'Viluppuram'),
(534, 31, 'Virudhunagar'),
(535, 32, 'Dhalai'),
(536, 32, 'North Tripura'),
(537, 32, 'South Tripura'),
(538, 32, 'Khowai[7]'),
(539, 32, 'West Tripura'),
(540, 33, 'Agra'),
(541, 33, 'Aligarh'),
(542, 33, 'Allahabad'),
(543, 33, 'Ambedkar Nagar'),
(544, 33, 'Auraiya'),
(545, 33, 'Azamgarh'),
(546, 33, 'Bagpat'),
(547, 33, 'Bahraich'),
(548, 33, 'Ballia'),
(549, 33, 'Balrampur'),
(550, 33, 'Banda'),
(551, 33, 'Barabanki'),
(552, 33, 'Bareilly'),
(553, 33, 'Basti'),
(554, 33, 'Bijnor'),
(555, 33, 'Budaun'),
(556, 33, 'Bulandshahr'),
(557, 33, 'Chandauli'),
(558, 33, 'Chhatrapati Shahuji Maharaj Nagar[8]'),
(559, 33, 'Chitrakoot'),
(560, 33, 'Deoria'),
(561, 33, 'Etah'),
(562, 33, 'Etawah'),
(563, 33, 'Faizabad'),
(564, 33, 'Farrukhabad'),
(565, 33, 'Fatehpur'),
(566, 33, 'Firozabad'),
(567, 33, 'Gautam Buddh Nagar'),
(568, 33, 'Ghaziabad'),
(569, 33, 'Ghazipur'),
(570, 33, 'Gonda'),
(571, 33, 'Gorakhpur'),
(572, 33, 'Hamirpur'),
(573, 33, 'Hardoi'),
(574, 33, 'Hathras'),
(575, 33, 'Jalaun'),
(576, 33, 'Jaunpur district'),
(577, 33, 'Jhansi'),
(578, 33, 'Jyotiba Phule Nagar'),
(579, 33, 'Kannauj'),
(580, 33, 'Kanpur'),
(581, 33, 'Kanshi Ram Nagar'),
(582, 33, 'Kaushambi'),
(583, 33, 'Kushinagar'),
(584, 33, 'Lakhimpur Kheri'),
(585, 33, 'Lalitpur'),
(586, 33, 'Lucknow'),
(587, 33, 'Maharajganj'),
(588, 33, 'Mahoba'),
(589, 33, 'Mainpuri'),
(590, 33, 'Mathura'),
(591, 33, 'Mau'),
(592, 33, 'Meerut'),
(593, 33, 'Mirzapur'),
(594, 33, 'Moradabad'),
(595, 33, 'Muzaffarnagar'),
(596, 33, 'Panchsheel Nagar district (Hapur)'),
(597, 33, 'Pilibhit'),
(598, 33, 'Pratapgarh'),
(599, 33, 'Raebareli'),
(600, 33, 'Ramabai Nagar (Kanpur Dehat)'),
(601, 33, 'Rampur'),
(602, 33, 'Saharanpur'),
(603, 33, 'Sant Kabir Nagar'),
(604, 33, 'Sant Ravidas Nagar'),
(605, 33, 'Shahjahanpur'),
(606, 33, 'Shamli[9]'),
(607, 33, 'Shravasti'),
(608, 33, 'Siddharthnagar'),
(609, 33, 'Sitapur'),
(610, 33, 'Sonbhadra'),
(611, 33, 'Sultanpur'),
(612, 33, 'Unnao'),
(613, 33, 'Varanasi'),
(614, 34, 'Almora'),
(615, 34, 'Bageshwar'),
(616, 34, 'Chamoli'),
(617, 34, 'Champawat'),
(618, 34, 'Dehradun'),
(619, 34, 'Haridwar'),
(620, 34, 'Nainital'),
(621, 34, 'Pauri Garhwal'),
(622, 34, 'Pithoragarh'),
(623, 34, 'Rudraprayag'),
(624, 34, 'Tehri Garhwal'),
(625, 34, 'Udham Singh Nagar'),
(626, 34, 'Uttarkashi'),
(627, 35, 'Bankura'),
(628, 35, 'Bardhaman'),
(629, 35, 'Birbhum'),
(630, 35, 'Cooch Behar'),
(631, 35, 'Dakshin Dinajpur'),
(632, 35, 'Darjeeling'),
(633, 35, 'Hooghly'),
(634, 35, 'Howrah'),
(635, 35, 'Jalpaiguri'),
(636, 35, 'Kolkata'),
(637, 35, 'Maldah'),
(638, 35, 'Murshidabad'),
(639, 35, 'Nadia'),
(640, 35, 'North 24 Parganas'),
(641, 35, 'Paschim Medinipur'),
(642, 35, 'Purba Medinipur'),
(643, 35, 'Purulia'),
(644, 35, 'South 24 Parganas'),
(645, 35, 'Uttar Dinajpur');

-- --------------------------------------------------------

--
-- Table structure for table `health_certificate`
--

CREATE TABLE IF NOT EXISTS `health_certificate` (
  `id` int(10) unsigned NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipment_id` int(10) unsigned NOT NULL,
  `plant_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `aqcs_certificate_reference_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `central_competent_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `local_competent_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_bip_eu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commodity_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commodity_certified_for` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commodity_species` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_temperature` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aqcs_registration_date` datetime DEFAULT NULL,
  `aqcs_certificate_reference_date` datetime DEFAULT NULL,
  `aqcs_registration_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cities` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
`id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `created_at`, `entity_id`, `entity_type`, `type`, `user_id`) VALUES
(1, '2015-03-03 12:55:54', 4, 'Product', 'Modified', 1),
(2, '2015-03-03 12:56:58', 1, 'Product', 'Modified', 1),
(3, '2015-03-03 12:59:03', 19, 'Product', 'Modified', 1),
(4, '2015-03-03 13:15:16', 1, 'Shipment', 'Created', 2),
(5, '2015-03-03 13:15:16', 34, 'SCC', 'Created', 2),
(6, '2015-03-03 13:24:03', 34, 'SCC', 'Modified', 2),
(7, '2015-03-03 13:24:03', 1, 'Product', 'Modified', 2),
(8, '2015-03-03 13:24:14', 34, 'SCC', 'Modified', 2),
(9, '2015-03-03 13:34:49', 34, 'SCC', 'Modified', 1),
(10, '2015-03-03 16:11:22', 23, 'Product', 'Modified', 1),
(11, '2015-03-05 12:14:03', 2, 'Shipment', 'Created', 2),
(12, '2015-03-05 12:14:03', 35, 'SCC', 'Created', 2),
(13, '2015-03-05 12:57:56', 35, 'SCC', 'Modified', 2),
(14, '2015-03-05 12:59:11', 35, 'SCC', 'Modified', 1),
(15, '2015-03-05 15:57:20', 2, 'Product', 'Modified', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
`id` int(10) unsigned NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industrial_user_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regional_office` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `address`, `district`, `type`, `name`, `phone`, `fax`, `industrial_user_type`, `state`, `regional_office`) VALUES
(2, 'Subhash Nagar, New Delhi', NULL, 'Approved', '', '', NULL, 'LME', NULL, NULL),
(4, 'Uttam Nagar', NULL, 'Guest', '', '', NULL, 'LME', NULL, NULL),
(5, 'PV', NULL, 'Guest', '', '', NULL, 'LME', NULL, NULL),
(13, 'New Delhi', NULL, 'Guest', 'Hemant Chawla', '09899788697', '090909090909', 'LME', NULL, NULL),
(16, 'Kaveri', 'Bastar', 'Guest', 'Kamal', '89669999999', '888888888888', 'MSMEExporter', 'Chhattisgarh (CG)', 'CAPEXIL, WESTERN  REGION, Commerce Centre 4th Floor, Block No. D-17, Tardeo Road,  MUMBAI – 400034, Phones: 022-23523410, 23520084, Fax: 011-23314486');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
`id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `notify_cycle` enum('Instant','Daily','Weekly','None') COLLATE utf8_unicode_ci NOT NULL,
  `sent` tinyint(1) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_user_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `created_at`, `content`, `entity_id`, `notify_cycle`, `sent`, `type`, `from_user_id`, `user_id`) VALUES
(31, '2015-01-01 13:57:02', NULL, 22, 'Instant', 0, 'PACApplied', 4, 4),
(32, '2015-01-01 13:57:57', NULL, 22, 'Instant', 0, 'PACApproved', 1, 4),
(33, '2015-01-02 08:49:30', 'Hey, hemant16apr@gmail.com! Confirm your Registration at Capexil Online Certification System', 15, 'Instant', 1, 'NewUserRegistrationConfirmationRequest', NULL, NULL),
(34, '2015-01-02 08:49:56', '<div><h3 style="color:#0e76bc; margin:0 0 1em 0;">\r\n						Registration of user hemant16apr@gmail.com has been confirmed at Capexil Online Certification System.\r\n					</h3></div>', 15, 'Instant', 1, 'NewUserRegistrationConfirmed', NULL, NULL),
(35, '2015-01-02 15:02:17', 'Hey, hemant16apr@yahoo.com! Confirm your Registration at Capexil Online Certification System', 16, 'Instant', 1, 'NewUserRegistrationConfirmationRequest', NULL, NULL),
(36, '2015-01-02 15:06:19', '<div><h3 style="color:#0e76bc; margin:0 0 1em 0;">\r\n						Registration of user hemant16apr@yahoo.com has been confirmed at Capexil Online Certification System.\r\n					</h3></div>', 16, 'Instant', 1, 'NewUserRegistrationConfirmed', NULL, NULL),
(37, '2015-01-02 16:04:02', NULL, 24, 'Instant', 0, 'PACApplied', 13, 13),
(38, '2015-01-02 16:54:28', NULL, 24, 'Instant', 0, 'PACApproved', 1, 13),
(39, '2015-01-06 17:12:19', NULL, 24, 'Instant', 0, 'PACNew', 1, 13),
(40, '2015-01-06 17:12:55', NULL, 24, 'Instant', 0, 'PACApplied', 13, 13),
(41, '2015-01-06 17:23:47', NULL, 24, 'Instant', 0, 'PACHold', 1, 13),
(42, '2015-01-07 17:51:17', NULL, 25, 'Instant', 0, 'PACApplied', 2, 2),
(43, '2015-01-07 17:52:21', NULL, 25, 'Instant', 0, 'PACApproved', 1, 2),
(44, '2015-01-07 18:09:21', NULL, 27, 'Instant', 0, 'SCCApplied', 2, 2),
(45, '2015-01-09 09:22:31', NULL, 29, 'Instant', 0, 'PACApplied', 2, 2),
(46, '2015-01-09 10:17:39', NULL, 30, 'Instant', 0, 'PACApplied', 2, 2),
(47, '2015-01-09 10:19:07', NULL, 30, 'Instant', 0, 'PACNew', 1, 2),
(48, '2015-01-09 11:32:42', NULL, 30, 'Instant', 0, 'PACApplied', 2, 2),
(49, '2015-01-09 11:33:08', NULL, 30, 'Instant', 0, 'PACApproved', 1, 2),
(50, '2015-01-12 18:18:53', 'Hey, hemant16apr@gmail.com! Confirm your Registration as Administrator at Capexil Online Certification System', 17, 'Instant', 1, 'NewUserRegistrationConfirmationRequest', NULL, NULL),
(51, '2015-01-12 18:19:33', '<div><h3 style="color:#0e76bc; margin:0 0 1em 0;">\r\n						Registration of user hemant16apr@gmail.com has been confirmed at Capexil Online Certification System.\r\n					</h3></div>', 17, 'Instant', 1, 'NewUserRegistrationConfirmed', NULL, NULL),
(52, '2015-01-12 18:55:27', 'Hey, hemant16apr@gmail.com! Confirm your Registration at Capexil Online Certification System', 18, 'Instant', 1, 'NewUserRegistrationConfirmationRequest', NULL, NULL),
(53, '2015-01-12 19:31:21', 'Hey, hemant16apr@gmail.com! Confirm your Registration at Capexil Online Certification System', 19, 'Instant', 1, 'NewUserRegistrationConfirmationRequest', NULL, NULL),
(54, '2015-01-12 19:33:50', '<div><h3 style="color:#0e76bc; margin:0 0 1em 0;">\r\n						Registration of user hemant16apr@gmail.com has been confirmed at Capexil Online Certification System.\r\n					</h3></div>', 19, 'Instant', 1, 'NewUserRegistrationConfirmed', NULL, NULL),
(55, '2015-01-19 19:55:44', '<div><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20013</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(56, '2015-01-19 21:56:25', '<div><p>Application Status</p><p style="background-color:#F1F2F4; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20014</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(57, '2015-01-19 22:02:26', '<div><p>Application Status</p><p style="background-color:#F1F2F4; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: </p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(58, '2015-01-20 11:51:15', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Hold</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23454</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACHold', 1, 2),
(59, '2015-01-20 11:52:01', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23455</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(60, '2015-01-22 15:32:58', 'Hey, mailtohemant@rediffmail.com! Confirm your Registration at Capexil Online Certification System', 20, 'Instant', 1, 'NewUserRegistrationConfirmationRequest', NULL, NULL),
(61, '2015-01-22 15:36:14', '<div><h3 style="color:#0e76bc; margin:0 0 1em 0;">\r\n						Registration of user mailtohemant@rediffmail.com has been confirmed at Capexil Online Certification System.\r\n					</h3></div>', 20, 'Instant', 1, 'NewUserRegistrationConfirmed', NULL, NULL),
(62, '2015-01-24 07:11:56', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20014</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(63, '2015-01-24 07:13:01', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20015</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(64, '2015-01-24 07:13:43', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23456</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(65, '2015-01-24 07:14:45', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20015</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(66, '2015-01-24 07:16:11', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20016</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(67, '2015-01-24 07:45:36', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23457</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(68, '2015-01-24 08:04:19', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20016</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(69, '2015-01-24 08:05:10', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20017</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(70, '2015-01-24 08:06:33', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23458</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(71, '2015-01-24 08:28:22', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20017</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(72, '2015-01-24 08:29:22', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20018</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(73, '2015-01-24 08:31:07', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23459</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(74, '2015-01-24 09:03:29', '<div><p>Application Status</p><p style="background-color:#59d9f4; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20018</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(75, '2015-01-24 09:11:56', '<div><p>Application Status</p><p style="background-color:#59d9f4; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20019</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(76, '2015-01-24 09:13:22', '<div><p>Application Status</p><p style="background-color:#d4421b; border-radius:4px; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23460</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(77, '2015-01-24 09:24:33', '<div><p>Application Status</p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20019</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(78, '2015-01-24 09:41:29', '<div><p>Application Status</p><p style="background-color:#056B9C; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20020</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(79, '2015-01-24 09:55:30', '<div><p>Application Status</p><p style="background-color:#056B9C; foreground-color:#FFFFFF; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20020</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(80, '2015-01-24 10:15:37', '<div><p>Application Status</p><p style="background-color:#056B9C; foreground-color:#FFFFFF; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20021</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(81, '2015-01-24 10:30:26', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23461</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(82, '2015-01-24 10:31:00', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20021</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(83, '2015-01-24 10:32:52', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20022</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(84, '2015-01-24 10:34:10', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23462</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(85, '2015-01-24 10:34:44', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20022</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(86, '2015-01-24 10:35:32', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20023</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(87, '2015-01-24 10:37:09', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23463</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(88, '2015-01-24 11:25:14', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Hold</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23463</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACHold', 1, 2),
(89, '2015-01-24 11:25:33', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20023</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(90, '2015-01-24 11:26:11', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20024</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(91, '2015-01-24 11:27:45', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23464</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(92, '2015-01-24 11:28:21', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20024</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACNew', 1, 2),
(93, '2015-01-24 11:28:52', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20025</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApplied', 2, 2),
(94, '2015-01-24 11:31:06', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23465</p><p>Plant Location: zamrudpur</p><p>Establishment: Mon Nov 03 00:00:00 IST 2014</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 30, 'Instant', 0, 'PACApproved', 1, 2),
(95, '2015-02-07 17:32:35', NULL, 31, 'Instant', 0, 'SCCApplied', 2, 2),
(96, '2015-02-07 17:33:42', NULL, 31, 'Instant', 0, 'SCCApproved', 1, 2),
(97, '2015-02-13 10:01:29', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20026</p><p>Plant Location: Kabir nagar Delhi</p><p>Establishment: Tue Sep 17 00:00:00 IST 2013</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 33, 'Instant', 0, 'PACApplied', 2, 2),
(98, '2015-02-13 10:03:40', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23466</p><p>Plant Location: Kabir nagar Delhi</p><p>Establishment: Tue Sep 17 00:00:00 IST 2013</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 33, 'Instant', 0, 'PACApproved', 1, 2),
(99, '2015-02-13 10:19:32', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20026</p><p>Plant Location: Kabir nagar Delhi</p><p>Establishment: Tue Sep 17 00:00:00 IST 2013</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 33, 'Instant', 0, 'PACNew', 1, 2),
(100, '2015-02-13 10:19:56', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Applied</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20027</p><p>Plant Location: Kabir nagar Delhi</p><p>Establishment: Tue Sep 17 00:00:00 IST 2013</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 33, 'Instant', 0, 'PACApplied', 2, 2),
(101, '2015-02-13 10:41:34', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>Approved</strong></p><p>Industrial User Email: member@capexil.com</p><p>Approval Number: CAPEXIL/NR/23466</p><p>Plant Location: Kabir nagar Delhi</p><p>Establishment: Tue Sep 17 00:00:00 IST 2013</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 33, 'Instant', 0, 'PACApproved', 1, 2),
(102, '2015-02-25 21:11:05', '<div><p>Application Status</p><p style="background-color:#A4AFEA; border-radius:25px; border:2px solid; box-shadow: 10px 10px 5px #888888; display:inline-block; padding:8px 15px;"><strong>New</strong></p><p>Industrial User Email: member@capexil.com</p><p>Application Reference Number: CAPEXIL/REF/NR/20027</p><p>Plant Location: Kabir nagar Delhi</p><p>Establishment: Tue Sep 17 00:00:00 IST 2013</p><p>Company Address: Subhash Nagar, New Delhi</p><p><a href="http://localhost:8080/capexil-certifications/account/login" style="color:#1C76BA;">Go to Capexil Online Certifications</a></p></div>', 33, 'Instant', 0, 'PACNew', 3, 2),
(103, '2015-03-03 13:24:14', NULL, 34, 'Instant', 0, 'SCCApplied', 2, 2),
(104, '2015-03-03 13:34:49', NULL, 34, 'Instant', 0, 'SCCApproved', 1, 2),
(105, '2015-03-05 12:57:56', NULL, 35, 'Instant', 0, 'SCCApplied', 2, 2),
(106, '2015-03-05 12:59:11', NULL, 35, 'Instant', 0, 'SCCApproved', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
`id` int(10) unsigned NOT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `certificate_id` int(10) unsigned NOT NULL,
  `total_amount` double NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cheque_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payment_mode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `fee` double NOT NULL,
  `service_tax` double NOT NULL,
  `service_tax_percentage` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `currency`, `certificate_id`, `total_amount`, `bank_name`, `member_id`, `status`, `cheque_number`, `payment_type`, `payment_mode`, `transaction_id`, `date`, `cheque_date`, `fee`, `service_tax`, `service_tax_percentage`) VALUES
(13, 'INR', 30, 5618, NULL, 2, 'New', NULL, 'Renewal', 'NetBanking', NULL, '2015-01-09 09:43:28', NULL, 5000, 618, 12.36),
(14, 'INR', 31, 1685, NULL, 2, 'New', NULL, 'New', 'NetBanking', NULL, '2015-01-09 11:37:59', NULL, 1500, 185, 0),
(15, 'INR', 33, 0, NULL, 2, 'New', NULL, 'New', 'NetBanking', NULL, '2015-02-13 10:01:12', NULL, 0, 0, 0),
(16, 'INR', 34, 1685, NULL, 2, 'New', NULL, 'New', 'NetBanking', NULL, '2015-03-03 13:15:08', NULL, 1500, 185, 0),
(17, 'INR', 35, 1685, NULL, 2, 'New', NULL, 'New', 'NetBanking', NULL, '2015-03-05 12:14:00', NULL, 1500, 185, 0);

-- --------------------------------------------------------

--
-- Table structure for table `plant`
--

CREATE TABLE IF NOT EXISTS `plant` (
`id` int(10) unsigned NOT NULL,
  `approval_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `establishment_date` datetime NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital_investment_land` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital_investment_building` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital_investment_machinery` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `factory_licence_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ssi_registration_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `iec_issuing_authority` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tin_issuing_authority` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `iec_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tin_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `capexil_membership_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `financial_institution` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plant`
--

INSERT INTO `plant` (`id`, `approval_number`, `establishment_date`, `member_id`, `location`, `product_code`, `capital_investment_land`, `capital_investment_building`, `capital_investment_machinery`, `factory_licence_number`, `ssi_registration_number`, `iec_issuing_authority`, `tin_issuing_authority`, `iec_number`, `tin_number`, `capexil_membership_number`, `financial_institution`) VALUES
(1, NULL, '2014-11-10 00:00:00', 4, 'Surat', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(2, NULL, '2014-11-02 00:00:00', 4, 'Sonipat Haryana', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(3, 'CAPEXIL/NR/23465', '2014-11-03 00:00:00', 2, 'zamrudpur', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(4, 'CAPEXIL/NR/23466', '2013-09-17 00:00:00', 2, 'Kabir nagar Delhi', 'ABPFSB', NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(5, NULL, '2014-11-11 00:00:00', 4, 'Mumbai', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(11, NULL, '2008-07-03 00:00:00', 13, 'Plant at Delhi', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(12, NULL, '2014-10-06 00:00:00', 16, 'Jamnagar', NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL),
(13, NULL, '2015-02-01 00:00:00', 2, 'Palam', 'ABPPET', NULL, NULL, NULL, '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plant_approval_certificate`
--

CREATE TABLE IF NOT EXISTS `plant_approval_certificate` (
  `id` int(10) unsigned NOT NULL,
  `decision_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inspection_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plant_id` int(10) unsigned NOT NULL,
  `remarks_on_certificate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inspection_report_file_path` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_revalidation` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plant_approval_certificate`
--

INSERT INTO `plant_approval_certificate` (`id`, `decision_notes`, `inspection_notes`, `plant_id`, `remarks_on_certificate`, `inspection_report_file_path`, `date_revalidation`) VALUES
(30, 'Changing to new', 'Changing to new', 3, NULL, 'Carousel-Bootstrap_5714973459381090443.pdf', NULL),
(33, 'ok', 'kk', 4, 'Good', 'Update_Capexil Project_8041839629915314634.pdf', '2015-05-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `plant_approval_committee`
--

CREATE TABLE IF NOT EXISTS `plant_approval_committee` (
`id` int(10) unsigned NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plant_approval_certificate_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plant_approval_committee`
--

INSERT INTO `plant_approval_committee` (`id`, `designation`, `name`, `plant_approval_certificate_id`) VALUES
(14, '3', 'Surya Kumar', 30),
(16, '1', 'Surya Kumar', 30),
(17, '3', 'Major Tam', 33),
(18, '1', 'Ram', 33);

-- --------------------------------------------------------

--
-- Table structure for table `plant_capacity`
--

CREATE TABLE IF NOT EXISTS `plant_capacity` (
`id` int(10) unsigned NOT NULL,
  `capacity` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `plant_id` int(10) unsigned NOT NULL,
  `unit` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plant_capacity`
--

INSERT INTO `plant_capacity` (`id`, `capacity`, `frequency`, `plant_id`, `unit`, `product_id`) VALUES
(1, '233', 'PM', 3, 'Tonnes', 2),
(2, '56', 'PM', 3, 'Tonnes', 5),
(3, '5636', 'PM', 3, 'Tonnes', 10),
(4, '230', 'PM', 4, 'KG', 3),
(5, '230', 'PM', 4, 'KG', 3),
(6, '5636', 'PM', 1, 'KG', 5),
(7, '230', 'PM', 1, 'Tonnes', 15),
(8, '2233', 'PM', 1, 'KG', 14),
(9, '236', 'PM', 2, 'KG', 4),
(11, '230', 'PM', 11, 'Tonnes', 2),
(12, '23', 'PM', 4, 'Tonnes', 3),
(13, '25', 'PM', 13, 'Tonnes', 21),
(14, '125', 'PM', 4, 'Tonnes', 24);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` int(10) unsigned NOT NULL,
  `declaration_required` tinyint(1) DEFAULT NULL,
  `annexure_required` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hs_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `chapter_eu` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `declaration_required`, `annexure_required`, `name`, `hs_code`, `chapter_eu`, `description`, `product_category`) VALUES
(1, NULL, NULL, 'Pig Bristles &  Hair', '05021010', 'C7A', 'Slaughter  Houses,   Meat   Processing   Plants   and and   Licenced   Butcher   shops for   domestic   pigs.', 'ABPFSB'),
(2, NULL, NULL, 'Silkworm Pupae', '051199', 'C8', 'Cocoon  Farms   / Silk   industry', 'ABPPET'),
(3, NULL, NULL, 'Bone Grist', '05061029', NULL, '', 'ABPFSB'),
(4, 1, 1, 'Bones, including horn-cores, crushed for technical use', '05061019', 'C6A', '', 'ABPFSB'),
(5, NULL, NULL, 'Ossein', '05061039', NULL, 'Ossein', 'GEL'),
(10, NULL, NULL, 'Dog or Cat Food put up for retail sale', '23091000', NULL, '', 'ABPPET'),
(11, NULL, NULL, 'Hemoglobin blood globulins and serum globulins', '30021020', NULL, '', 'ABPIP'),
(12, NULL, NULL, 'Gelatine, edible grade and not elsewhere specified or included', '35030020', NULL, '', 'GEL'),
(13, NULL, NULL, 'Glues derived from bones, hides and similar item, fish glues', '35030030', NULL, 'Glues derived from bones, hides and similar item, fish glues', 'ABPFSB'),
(14, NULL, NULL, 'Peptones', '35040010', NULL, '', 'ABPIP'),
(15, NULL, NULL, 'Empty Gelatine Capsules for Animal consumption', '96020030', NULL, '', 'ABPPET'),
(17, NULL, NULL, 'Gelatine, not elsewhere specified or included, for technical use', '35030020', NULL, '', 'ABPFSB'),
(18, NULL, NULL, 'Empty Gelatine Capsules for Human consumption', '96020030', NULL, '', 'GEL'),
(19, 1, 1, 'Bones, including horn-cores, crushed for human consumption', '05061019', 'C6A', '', 'ABPCBM'),
(20, NULL, NULL, 'Button Blanks', '96063010', NULL, '', 'ABPFSB'),
(21, NULL, NULL, 'Buffalo Horns for Dog Chew', '05079050', NULL, '', 'ABPPET'),
(22, NULL, NULL, 'Buffalo Horns for other use', '05079050', NULL, '', 'ABPFSB'),
(23, NULL, NULL, 'Crushed Hooves (Dried Hoof Products)', '05079030', 'C18', '', 'ABPSOIL'),
(24, NULL, NULL, 'Crushed Hooves (Dried Hoof Products)', '05079030', NULL, '', 'ABPFSB'),
(25, NULL, NULL, 'Steamed Horns & Hooves Grist (Dried Horn and Hoof Products) as Organic Fertilizer', '05079090', NULL, '', 'ABPSOIL'),
(26, NULL, NULL, 'Steamed Horns & Hooves Grist (Dried Horn and Hoof Products) for Technical Use', '05079090', NULL, '', 'ABPFSB'),
(27, NULL, NULL, 'Animal Fertilizers', '310100', NULL, '', 'ABPSOIL'),
(28, NULL, NULL, 'Degreased Gel Bone Chips (Crushed Bones) – For Human Consumption', '05061019', NULL, '', 'ABPCBM'),
(29, NULL, NULL, 'Buffalo Tallow', '15021090', NULL, '', 'ABPFSB'),
(30, NULL, NULL, 'Pet Treats', '23091000', NULL, '', 'ABPPET'),
(31, NULL, NULL, 'Pet Chews', '23091000', NULL, '', 'ABPPET'),
(32, NULL, NULL, 'Dog Chews', '23091000', NULL, '', 'ABPPET'),
(33, NULL, NULL, 'Meat Jerkey', '23091000', NULL, '', 'ABPPET'),
(34, NULL, NULL, 'Pizzle', '23091000', NULL, '', 'ABPPET'),
(35, NULL, NULL, 'Tripe', '23091000', NULL, '', 'ABPPET'),
(36, NULL, NULL, 'Gullet', '23091000', NULL, '', 'ABPPET'),
(37, NULL, NULL, 'Ear', '23091000', NULL, '', 'ABPPET'),
(38, NULL, NULL, 'Raw hide Chips', '23091000', NULL, '', 'ABPPET'),
(39, NULL, NULL, 'Twisted Bladder', '23091000', NULL, '', 'ABPPET'),
(40, NULL, NULL, 'Heart', '23091000', NULL, '', 'ABPPET'),
(41, NULL, NULL, 'Meat-cum-Bone Meal – Concentrates for Compound Animal Feed', '23099020', NULL, '', 'ABPPET'),
(42, NULL, NULL, 'Blood Meal – Concentrates for Compound Animal Feed', '23099020', NULL, '', 'ABPPET'),
(43, NULL, NULL, 'Bovine Hide Cutting, raw and salted, only as raw material for Gelatine Industry', '41012020', NULL, '', 'ABPHC'),
(44, NULL, NULL, 'Worked Bones excluding Whale Bones and Articles thereof', '96019030', NULL, '', 'ABPFSB'),
(45, NULL, NULL, 'Worked Horns & articles', '96019040', NULL, '', 'ABPFSB'),
(46, NULL, NULL, 'Buttons', '96062990', NULL, '', 'ABPFSB');

-- --------------------------------------------------------

--
-- Table structure for table `regional_office`
--

CREATE TABLE IF NOT EXISTS `regional_office` (
`id` int(8) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `regional_office`
--

INSERT INTO `regional_office` (`id`, `address`, `phones`, `fax`) VALUES
(1, 'CAPEXIL, NORTHERN REGION, 4B, 4th Floor, Vandana Building, 11, Tolstoy Marg, NEW DELHI-110001', '011-23356703, 23752282', '011-23314486'),
(2, 'CAPEXIL, WESTERN  REGION, Commerce Centre 4th Floor, Block No. D-17, Tardeo Road,  MUMBAI – 400034', '022-23523410, 23520084', '011-23314486'),
(3, 'CAPEXIL, EASTERN REGION, Vanijya Bhavan (3rd  Floor), International Trade Facilitation Centre, 1/1, Wood Street, KOLKATA-700016', '033-22891721, 22, 23', ''),
(4, 'CAPEXIL, SOUTHERN REGION, Rasheed Mansion, 408 (Old no. 622), Anna Salai, CHENNAI – 600006', '044-28292310, 28294713', '044-28295386');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
`id` int(10) unsigned NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `district_id` int(8) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires_on` datetime NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firm_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firm_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plant_establishment_date` datetime DEFAULT NULL,
  `plant_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state_id` int(8) DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `verifying_authority_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industrial_user_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `contact_person`, `country_id`, `district_id`, `email`, `expires_on`, `fax`, `firm_address`, `firm_name`, `password`, `phone`, `plant_establishment_date`, `plant_location`, `registration_type`, `state_id`, `status`, `verifying_authority_type`, `industrial_user_type`) VALUES
(20, 'Kamal', NULL, 107, 'mailtohemant@rediffmail.com', '2015-02-06 15:36:12', '888888888888', 'Kaveri', 'Ashok Kumar', '7be2828394ff6429ac339281a75fe3ce', '89669999999', '2014-10-06 00:00:00', 'Jamnagar', 'IndustrialUser', 7, 'Confirmed', NULL, 'MSMEExporter');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(0, 'SuperAdministrator'),
(1, 'Administrator'),
(2, 'Member'),
(3, 'VerifyingAuthority');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `value`, `type`) VALUES
('PACExpiryDate', 'May 24, 2014 12:00:00 AM', 'DateTime'),
('SCCExpiryDate', 'Jan 25, 2015 12:00:00 AM', 'DateTime'),
('HCExpiryDate', 'May 15, 2014 12:00:00 AM', 'DateTime'),
('PACApprovalNumberSeries', 'CAPEXIL/NR/23467', 'String'),
('HCApprovalNumberSeries', 'CAPEXIL/SR/4646', 'String'),
('SCCApprovalNumberSeries', 'CAPEXIL/NR/12526', 'String'),
('CertificateAmount', '10000', 'Integer'),
('PACReferralNumberSeries', 'CAPEXIL/REF/NR/20028', 'String'),
('SCCReferralNumberSeries', 'CAPEXIL/REF/SR/11445', 'String');

-- --------------------------------------------------------

--
-- Table structure for table `shipment`
--

CREATE TABLE IF NOT EXISTS `shipment` (
`id` int(10) unsigned NOT NULL,
  `consignee_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `container_receipt_place` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `container_load` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `container_size` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_id` int(10) unsigned NOT NULL,
  `gross_weight` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_amount` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_currency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `import_permit_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `import_admission_eu` tinyint(1) DEFAULT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `lading_bill_date` datetime DEFAULT NULL,
  `lading_bill_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loading_port` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `destination_port` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `marks_on_bag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `origin_id` int(10) unsigned NOT NULL,
  `shipment_date` datetime DEFAULT NULL,
  `total_containers` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `vessel_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight_unit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `packaging_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iso_code_region_destination` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iso_code_region_origin` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_origin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_bags` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_cargo_carton_pieces` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consignee_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_warehouse` tinyint(1) DEFAULT NULL,
  `departure_date` datetime DEFAULT NULL,
  `means_of_transport` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `means_of_transport_identification` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `means_of_transport_documentation_reference` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_responsible_for_load_in_europe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `third_country_transit` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipment`
--

INSERT INTO `shipment` (`id`, `consignee_address`, `container_receipt_place`, `container_load`, `container_size`, `destination_id`, `gross_weight`, `invoice_amount`, `invoice_currency`, `invoice_number`, `invoice_date`, `import_permit_number`, `import_admission_eu`, `member_id`, `lading_bill_date`, `lading_bill_number`, `loading_port`, `destination_port`, `marks_on_bag`, `origin_id`, `shipment_date`, `total_containers`, `vessel_name`, `weight_unit`, `packaging_type`, `iso_code_region_destination`, `iso_code_region_origin`, `region_destination`, `region_origin`, `total_bags`, `total_cargo_carton_pieces`, `consignee_name`, `custom_warehouse`, `departure_date`, `means_of_transport`, `means_of_transport_identification`, `means_of_transport_documentation_reference`, `person_responsible_for_load_in_europe`, `third_country_transit`) VALUES
(1, NULL, 'Tughlakabad', 'FCL', NULL, 56, '123', '12345', 'AUD', '12345', '2015-03-01 00:00:00', NULL, 0, 2, NULL, NULL, 'PIPAVAV', 'PIPAVAV', NULL, 104, NULL, 'LTO', NULL, 'KG', 'Bags', NULL, NULL, NULL, NULL, '13', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, 'Tughlakabad', NULL, 'TwentyFeet', 19, '123', '12345', 'AUD', '12345', '2015-02-02 00:00:00', NULL, 0, 2, NULL, NULL, 'PIPAVAV', 'PIPAVAV', NULL, 1, NULL, 'Two', NULL, 'KG', 'Bags', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipment_clearance_certificate`
--

CREATE TABLE IF NOT EXISTS `shipment_clearance_certificate` (
  `id` int(10) unsigned NOT NULL,
  `decision_notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plant_id` int(10) unsigned NOT NULL,
  `shipment_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipment_clearance_certificate`
--

INSERT INTO `shipment_clearance_certificate` (`id`, `decision_notes`, `plant_id`, `shipment_id`) VALUES
(34, 'Approved.', 3, 1),
(35, 'Approved', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `shipment_container`
--

CREATE TABLE IF NOT EXISTS `shipment_container` (
`id` int(10) unsigned NOT NULL,
  `container_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `c_seal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `l_seal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `shipment_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipment_product`
--

CREATE TABLE IF NOT EXISTS `shipment_product` (
  `shipment_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipment_product`
--

INSERT INTO `shipment_product` (`shipment_id`, `product_id`) VALUES
(2, 2),
(2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
`id` int(8) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `regional_office_id` int(8) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `regional_office_id`) VALUES
(1, 'Andaman and Nicobar (AN)', 3),
(2, 'Andhra Pradesh (AP)', 4),
(3, 'Arunachal Pradesh (AR)', 3),
(4, 'Assam (AS)', 3),
(5, 'Bihar (BR)', 3),
(6, 'Chandigarh (CH)', 1),
(7, 'Chhattisgarh (CG)', 2),
(8, 'Dadra and Nagar Haveli (DN)', 2),
(9, 'Daman and Diu (DD)', 4),
(10, 'Delhi (DL)', 1),
(11, 'Goa (GA)', 2),
(12, 'Gujarat (GJ)', 2),
(13, 'Haryana (HR)', 1),
(14, 'Himachal Pradesh (HP)', 1),
(15, 'Jammu and Kashmir (JK)', 1),
(16, 'Jharkhand (JH)', 3),
(17, 'Karnataka (KA)', 4),
(18, 'Kerala (KL)', 4),
(19, 'Lakshdweep (LD)', 4),
(20, 'Madhya Pradesh (MP)', 2),
(21, 'Maharashtra (MH)', 2),
(22, 'Manipur (MN)', 3),
(23, 'Meghalaya (ML)', 3),
(24, 'Mizoram (MZ)', 3),
(25, 'Nagaland (NL)', 3),
(26, 'Odisha (OD)', 2),
(27, 'Puducherry (PY)', 4),
(28, 'Punjab (PB)', 1),
(29, 'Rajasthan (RJ)', 1),
(30, 'Sikkim (SK)', 3),
(31, 'Tamil Nadu (TN)', 4),
(32, 'Tripura (TR)', 3),
(33, 'Uttar Pradesh (UP)', 1),
(34, 'Uttarakhand (UK)', 1),
(35, 'West Bengal (WB)', 3);

-- --------------------------------------------------------

--
-- Table structure for table `super_administrator`
--

CREATE TABLE IF NOT EXISTS `super_administrator` (
`id` int(10) unsigned NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `super_administrator`
--

INSERT INTO `super_administrator` (`id`, `phone`) VALUES
(3, '98989899');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(10) unsigned NOT NULL,
  `user_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firm_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `time_zone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_type`, `email`, `firm_name`, `last_login`, `password`, `status`, `role_id`, `time_zone`) VALUES
(1, 'Administrator', 'admin@capexil.com', 'Admin Firm New', '2015-03-05 15:57:05', '21232f297a57a5a743894a0e4a801fc3', 'Enabled', 1, NULL),
(2, 'Member', 'member@capexil.com', 'Approved Member Firm', '2015-03-05 16:10:00', 'b0ab0254bd58eb87eaee3172ba49fefb', 'Enabled', 2, NULL),
(3, 'SuperAdministrator', 'super@capexil.com', 'Super Admin Firm', '2015-02-25 22:57:07', '1b3231655cebb7a1f783eddf27d254ca', 'Enabled', 0, NULL),
(4, 'Member', 'guest@capexil.com', 'Guest Firm', '2015-01-01 14:11:19', '084e0343a0486ff05530df6c705c8bb4', 'Enabled', 2, NULL),
(5, 'Member', 'hemant@capexil.com', 'HC Limited', '2014-12-02 17:16:16', '81dc9bdb52d04dc20036dbd8313ed055', 'Enabled', 2, NULL),
(13, 'Member', 'hemant16apr@yahoo.com', 'Hemant Chawla Limited', '2015-01-07 18:52:33', '20a836a18b3508d66fd63abfd924ace4', 'Enabled', 2, NULL),
(16, 'Member', 'mailtohemant@rediffmail.com', 'Ashok Kumar', '2015-01-22 15:36:51', '7be2828394ff6429ac339281a75fe3ce', 'Enabled', 2, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `user_auth`
--
CREATE TABLE IF NOT EXISTS `user_auth` (
`email` varchar(255)
,`password` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `user_role_auth`
--
CREATE TABLE IF NOT EXISTS `user_role_auth` (
`email` varchar(255)
,`role` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `verifying_authority`
--

CREATE TABLE IF NOT EXISTS `verifying_authority` (
`id` int(10) unsigned NOT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure for view `user_auth`
--
DROP TABLE IF EXISTS `user_auth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_auth` AS select `user`.`email` AS `email`,`user`.`password` AS `password` from `user` where (`user`.`status` = 'Enabled');

-- --------------------------------------------------------

--
-- Structure for view `user_role_auth`
--
DROP TABLE IF EXISTS `user_role_auth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_role_auth` AS select `user`.`email` AS `email`,`role`.`name` AS `role` from (`user` join `role` on((`user`.`role_id` = `role`.`id`))) where (`user`.`status` = 'Enabled');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `annexure2`
--
ALTER TABLE `annexure2`
 ADD PRIMARY KEY (`id`), ADD KEY `shipment_id` (`shipment_id`), ADD KEY `fk_annexure2_plant_id` (`plant_id`);

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
 ADD PRIMARY KEY (`id`), ADD KEY `approver_id` (`approver_id`);

--
-- Indexes for table `continents`
--
ALTER TABLE `continents`
 ADD PRIMARY KEY (`code`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_code` (`code`) USING BTREE, ADD KEY `idx_continent_code` (`continent_code`) USING BTREE;

--
-- Indexes for table `declaration`
--
ALTER TABLE `declaration`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_ship_id` (`shipment_id`), ADD KEY `fk_declaration_plant_id` (`plant_id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_certificate`
--
ALTER TABLE `health_certificate`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_hc_shipment_id` (`shipment_id`), ADD KEY `fk_health_certi_plant_id` (`plant_id`), ADD KEY `fk_health_to_product` (`product_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_log_user_id` (`user_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_noti_from_user_id` (`from_user_id`), ADD KEY `fk_noti_user_id` (`user_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_payment_certi_id` (`certificate_id`), ADD KEY `fk_payment_member_id` (`member_id`);

--
-- Indexes for table `plant`
--
ALTER TABLE `plant`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_plant_member` (`member_id`);

--
-- Indexes for table `plant_approval_certificate`
--
ALTER TABLE `plant_approval_certificate`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_pac_plant` (`plant_id`);

--
-- Indexes for table `plant_approval_committee`
--
ALTER TABLE `plant_approval_committee`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_plant_approval_cert_id` (`plant_approval_certificate_id`);

--
-- Indexes for table `plant_capacity`
--
ALTER TABLE `plant_capacity`
 ADD PRIMARY KEY (`id`), ADD KEY `product_id` (`product_id`), ADD KEY `fk_capacity_plant_id` (`plant_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_product_product_category_id` (`product_category`);

--
-- Indexes for table `regional_office`
--
ALTER TABLE `regional_office`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_countries_registration` (`country_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipment`
--
ALTER TABLE `shipment`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_shipment_member_id` (`member_id`), ADD KEY `fk_countries_shipment_destination` (`destination_id`), ADD KEY `fk_countries_shipment_origin` (`origin_id`), ADD KEY `fk_shipment_third_country` (`third_country_transit`);

--
-- Indexes for table `shipment_clearance_certificate`
--
ALTER TABLE `shipment_clearance_certificate`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_scc_shipment_id` (`shipment_id`), ADD KEY `fk_shipment_certi_plant_id` (`plant_id`);

--
-- Indexes for table `shipment_container`
--
ALTER TABLE `shipment_container`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_sc_shipment_id` (`shipment_id`);

--
-- Indexes for table `shipment_product`
--
ALTER TABLE `shipment_product`
 ADD KEY `fk_shipment_product_shipment` (`shipment_id`), ADD KEY `fk_shipment_product_product` (`product_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `super_administrator`
--
ALTER TABLE `super_administrator`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_user_role_id` (`role_id`);

--
-- Indexes for table `verifying_authority`
--
ALTER TABLE `verifying_authority`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_countries_verifying_auth` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `annexure2`
--
ALTER TABLE `annexure2`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `declaration`
--
ALTER TABLE `declaration`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `plant`
--
ALTER TABLE `plant`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `plant_approval_committee`
--
ALTER TABLE `plant_approval_committee`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `plant_capacity`
--
ALTER TABLE `plant_capacity`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `regional_office`
--
ALTER TABLE `regional_office`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `shipment`
--
ALTER TABLE `shipment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shipment_container`
--
ALTER TABLE `shipment_container`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `super_administrator`
--
ALTER TABLE `super_administrator`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `verifying_authority`
--
ALTER TABLE `verifying_authority`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `administrator`
--
ALTER TABLE `administrator`
ADD CONSTRAINT `fk_admin_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `annexure2`
--
ALTER TABLE `annexure2`
ADD CONSTRAINT `fk_annexure2_plant_id` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`),
ADD CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `certificate` (`id`),
ADD CONSTRAINT `shipment_id` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`);

--
-- Constraints for table `certificate`
--
ALTER TABLE `certificate`
ADD CONSTRAINT `approver_id` FOREIGN KEY (`approver_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `declaration`
--
ALTER TABLE `declaration`
ADD CONSTRAINT `fk_cert_id` FOREIGN KEY (`id`) REFERENCES `certificate` (`id`),
ADD CONSTRAINT `fk_declaration_plant_id` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`),
ADD CONSTRAINT `fk_ship_id` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`);

--
-- Constraints for table `health_certificate`
--
ALTER TABLE `health_certificate`
ADD CONSTRAINT `fk_hc_id` FOREIGN KEY (`id`) REFERENCES `certificate` (`id`),
ADD CONSTRAINT `fk_hc_shipment_id` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`),
ADD CONSTRAINT `fk_health_certi_plant_id` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`),
ADD CONSTRAINT `fk_health_to_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `log`
--
ALTER TABLE `log`
ADD CONSTRAINT `fk_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `member`
--
ALTER TABLE `member`
ADD CONSTRAINT `FK_exporter_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `fk_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
ADD CONSTRAINT `fk_noti_from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `fk_noti_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
ADD CONSTRAINT `fk_payment_certi_id` FOREIGN KEY (`certificate_id`) REFERENCES `certificate` (`id`),
ADD CONSTRAINT `fk_payment_member_id` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`);

--
-- Constraints for table `plant`
--
ALTER TABLE `plant`
ADD CONSTRAINT `fk_plant_member` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`);

--
-- Constraints for table `plant_approval_certificate`
--
ALTER TABLE `plant_approval_certificate`
ADD CONSTRAINT `fk_pac_id` FOREIGN KEY (`id`) REFERENCES `certificate` (`id`),
ADD CONSTRAINT `fk_pac_plant` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`);

--
-- Constraints for table `plant_approval_committee`
--
ALTER TABLE `plant_approval_committee`
ADD CONSTRAINT `fk_plant_approval_cert_id` FOREIGN KEY (`plant_approval_certificate_id`) REFERENCES `plant_approval_certificate` (`id`);

--
-- Constraints for table `plant_capacity`
--
ALTER TABLE `plant_capacity`
ADD CONSTRAINT `fk_capacity_plant_id` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`),
ADD CONSTRAINT `plant_capacity_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `registration`
--
ALTER TABLE `registration`
ADD CONSTRAINT `fk_countries_registration` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `shipment`
--
ALTER TABLE `shipment`
ADD CONSTRAINT `fk_countries_shipment_destination` FOREIGN KEY (`destination_id`) REFERENCES `countries` (`id`),
ADD CONSTRAINT `fk_countries_shipment_origin` FOREIGN KEY (`origin_id`) REFERENCES `countries` (`id`),
ADD CONSTRAINT `fk_shipment_member_id` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`),
ADD CONSTRAINT `fk_shipment_third_country` FOREIGN KEY (`third_country_transit`) REFERENCES `countries` (`id`);

--
-- Constraints for table `shipment_clearance_certificate`
--
ALTER TABLE `shipment_clearance_certificate`
ADD CONSTRAINT `fk_scc_id` FOREIGN KEY (`id`) REFERENCES `certificate` (`id`),
ADD CONSTRAINT `fk_scc_shipment_id` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`),
ADD CONSTRAINT `fk_shipment_certi_plant_id` FOREIGN KEY (`plant_id`) REFERENCES `plant` (`id`);

--
-- Constraints for table `shipment_container`
--
ALTER TABLE `shipment_container`
ADD CONSTRAINT `fk_sc_shipment_id` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`);

--
-- Constraints for table `shipment_product`
--
ALTER TABLE `shipment_product`
ADD CONSTRAINT `fk_shipment_product_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
ADD CONSTRAINT `fk_shipment_product_shipment` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`);

--
-- Constraints for table `super_administrator`
--
ALTER TABLE `super_administrator`
ADD CONSTRAINT `fk_super_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `FK_user_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Constraints for table `verifying_authority`
--
ALTER TABLE `verifying_authority`
ADD CONSTRAINT `fk_countries_verifying_auth` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
ADD CONSTRAINT `fk_verifier_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
