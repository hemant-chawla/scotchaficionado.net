package com.capexil.bootstrap.resource;

import javax.faces.application.ProjectStage;
import javax.faces.context.FacesContext;

import org.richfaces.configuration.ConfigurationServiceHelper;
import org.richfaces.resource.ResourceKey;
import org.richfaces.resource.ResourceLibrary;

import com.capexil.bootstrap.BootstrapConfiguration;
import com.google.common.collect.ImmutableList;

public class CssLibrary implements ResourceLibrary
{

	private static final ImmutableList<ResourceKey> BOOTSTRAP_CLIENT_SIDE = ImmutableList.of(
			ResourceKey.create("bootstrap.less", "com.capexil.develop"), ResourceKey.create("less.min.js", "less"),
			ResourceKey.create("less.instant.js", "less"));

	private static final ImmutableList<ResourceKey> BOOTSTRAP_MIN = ImmutableList.of(ResourceKey.create(
			"bootstrap.css", "com.capexil"));

	@Override
	public Iterable<ResourceKey> getResources()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ProjectStage stage = facesContext.getApplication().getProjectStage();
		Boolean isClientSideStyle = ConfigurationServiceHelper.getBooleanConfigurationValue(facesContext,
				BootstrapConfiguration.Items.clientSideStyle);

		if (ProjectStage.Production != stage && isClientSideStyle)
		{
			return BOOTSTRAP_CLIENT_SIDE;
		}
		else
		{
			return BOOTSTRAP_MIN;
		}
	}
}
