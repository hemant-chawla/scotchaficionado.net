package com.capexil.bootstrap.ui.icon;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;

import org.richfaces.renderkit.RendererBase;

/**
 * Base class for the icon renderer
 * 
 */
@ResourceDependencies(
	{ @ResourceDependency(library = "javax.faces", name = "jsf.js"),
			@ResourceDependency(library = "com.capexil", name = "bootstrap-css.reslib") })
public abstract class IconRendererBase extends RendererBase
{
	public static final String RENDERER_TYPE = "com.capexil.bootstrap.IconRenderer";
}