package com.capexil.bootstrap.ui.pagination;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.richfaces.ui.common.HtmlConstants;
import org.richfaces.ui.iteration.dataScroller.AbstractDataScroller;
import org.richfaces.ui.iteration.dataScroller.ControlsState;
import org.richfaces.ui.iteration.dataScroller.DataScrollerBaseRenderer;

@ResourceDependencies({ @ResourceDependency(library = "javax.faces", name = "jsf.js"),
    @ResourceDependency(library = "org.richfaces", name = "jquery.js"),
    @ResourceDependency(library = "org.richfaces", name = "richfaces.js"),
    @ResourceDependency(library = "org.richfaces", name = "richfaces-queue.reslib"),
    @ResourceDependency(library = "org.richfaces", name = "common/richfaces-base-component.js"),
    @ResourceDependency(library="org.richfaces", name = "richfaces-event.js"),
    @ResourceDependency(library = "org.richfaces", name = "iteration/dataScroller/datascroller.js")})
public class PaginationRendererBase extends DataScrollerBaseRenderer
{
	public static final String RENDERER_TYPE = "com.capexil.bootstrap.PaginationRenderer";

	@Override
	public Map<String, Map<String, String>> getControls(FacesContext context, UIComponent component,
			ControlsState controlsState)
	{

		Map<String, Map<String, String>> controls = new HashMap<String, Map<String, String>>();
		Map<String, String> right = new HashMap<String, String>();
		Map<String, String> left = new HashMap<String, String>();

		String clientId = component.getClientId(context);

		if (controlsState.getFirstRendered() && controlsState.getFirstEnabled())
		{
			left.put(clientId + "_ds_f", AbstractDataScroller.FIRST_FACET_NAME);
		}

		if (controlsState.getFastRewindRendered() && controlsState.getFastRewindEnabled())
		{
			left.put(clientId + "_ds_fr", AbstractDataScroller.FAST_REWIND_FACET_NAME);
		}

		if (controlsState.getPreviousRendered() && controlsState.getPreviousEnabled())
		{
			left.put(clientId + "_ds_prev", AbstractDataScroller.PREVIOUS_FACET_NAME);
		}

		if (controlsState.getFastForwardRendered() && controlsState.getFastForwardEnabled())
		{
			right.put(clientId + "_ds_ff", AbstractDataScroller.FAST_FORWARD_FACET_NAME);
		}

		if (controlsState.getNextRendered() && controlsState.getNextEnabled())
		{
			right.put(clientId + "_ds_next", AbstractDataScroller.NEXT_FACET_NAME);
		}

		if (controlsState.getLastRendered() && controlsState.getLastEnabled())
		{
			right.put(clientId + "_ds_l", AbstractDataScroller.LAST_FACET_NAME);
		}

		if (!left.isEmpty())
		{
			controls.put("left", left);
		}

		if (!right.isEmpty())
		{
			controls.put("right", right);
		}
		return controls;
	}

	@Override
	public Map<String, String> renderPager(ResponseWriter out, FacesContext context, UIComponent component)
			throws IOException
	{

		int currentPage = (Integer) component.getAttributes().get("page");
		int maxPages = (Integer) component.getAttributes().get("maxPagesOrDefault");
		int pageCount = (Integer) component.getAttributes().get("pageCount");

		Map<String, String> digital = new HashMap<String, String>();

		if (pageCount <= 1)
		{
			return digital;
		}

		int delta = maxPages / 2;

		int pages;
		int start;

		if (pageCount > maxPages && currentPage > delta)
		{
			pages = maxPages;
			start = currentPage - pages / 2 - 1;
			if (start + pages > pageCount)
			{
				start = pageCount - pages;
			}
		}
		else
		{
			pages = pageCount < maxPages ? pageCount : maxPages;
			start = 0;
		}

		String clientId = component.getClientId(context);

		int size = start + pages;
		for (int i = start; i < size; i++)
		{

			boolean isCurrentPage = (i + 1 == currentPage);
			String styleClass;
			String style;

			if (isCurrentPage)
			{
				styleClass = (String) component.getAttributes().get("selectedStyleClass");
				style = (String) component.getAttributes().get("selectedStyle");
			}
			else
			{
				styleClass = (String) component.getAttributes().get("inactiveStyleClass");
				style = (String) component.getAttributes().get("inactiveStyle");
			}

			out.startElement(HtmlConstants.LI_ELEMENT, component);

			if (isCurrentPage)
			{
				styleClass = (styleClass == null ? "active" : "active " + styleClass);
			}

			out.writeAttribute(HtmlConstants.CLASS_ATTRIBUTE, styleClass, null);

			if (null != style)
			{
				out.writeAttribute(HtmlConstants.STYLE_ATTRIBUTE, style, null);
			}

			if (isCurrentPage)
			{
				out.startElement(HtmlConstants.SPAN_ELEM, component);
			}
			else
			{
				out.startElement(HtmlConstants.A_ELEMENT, component);
				out.writeAttribute(HtmlConstants.HREF_ATTR, "javascript:void(0)", null);
			}

			String page = Integer.toString(i + 1);
			String id = clientId + "_ds_" + page;

			out.writeAttribute(HtmlConstants.ID_ATTRIBUTE, id, null);

			digital.put(id, page);

			out.writeText(page, null);

			if (isCurrentPage)
			{
				out.endElement(HtmlConstants.SPAN_ELEM);
			}
			else
			{
				out.endElement(HtmlConstants.A_ELEMENT);
			}

			out.endElement(HtmlConstants.LI_ELEMENT);
		}

		return digital;
	}
}
