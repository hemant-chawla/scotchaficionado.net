package com.capexil.bootstrap.ui.pagination;

import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.JsfRenderer;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.cdk.annotations.TagType;
import org.richfaces.ui.iteration.dataScroller.AbstractDataScroller;
import org.richfaces.ui.iteration.dataScroller.DataScrollerHandler;

/**
 * <p>
 * The &lt;r:dataScroller&gt; component is used for navigating through multiple
 * pages of tables or grids.
 * </p>
 */
@JsfComponent(type = AbstractPagination.COMPONENT_TYPE, family = AbstractPagination.COMPONENT_FAMILY,
		renderer = @JsfRenderer(type = PaginationRendererBase.RENDERER_TYPE), tag = @Tag(name = "pagination",
				handlerClass = DataScrollerHandler.class, type = TagType.Facelets))
public abstract class AbstractPagination extends AbstractDataScroller
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.Pagination";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.Pagination";
}