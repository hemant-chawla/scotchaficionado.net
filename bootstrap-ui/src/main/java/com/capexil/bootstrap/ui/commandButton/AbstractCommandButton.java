package com.capexil.bootstrap.ui.commandButton;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.richfaces.cdk.annotations.Attribute;
import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.JsfRenderer;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.ui.ajax.region.AjaxContainer;
import org.richfaces.ui.attribute.AjaxProps;
import org.richfaces.ui.attribute.CommandButtonProps;
import org.richfaces.ui.attribute.CoreProps;
import org.richfaces.ui.common.AbstractActionComponent;
import org.richfaces.ui.common.AjaxConstants;
import org.richfaces.ui.common.Mode;
import org.richfaces.ui.common.meta.MetaComponentResolver;

import com.capexil.bootstrap.component.BootstrapScale;
import com.capexil.bootstrap.component.BootstrapSeverity;
import com.capexil.bootstrap.component.props.CardinalPositionProps;
import com.capexil.bootstrap.semantic.RenderSeparatorFacetCapable;

/**
 * Base class for the commandButton component
 * 
 */
@JsfComponent(type = AbstractCommandButton.COMPONENT_TYPE, family = AbstractCommandButton.COMPONENT_FAMILY,
		renderer = @JsfRenderer(type = CommandButtonRendererBase.RENDERER_TYPE), tag = @Tag(name = "commandButton"))
public abstract class AbstractCommandButton extends AbstractActionComponent implements AjaxProps, CoreProps,
		CommandButtonProps, CardinalPositionProps, RenderSeparatorFacetCapable, MetaComponentResolver
{
	public enum Facets
	{
		icon, split
	}

	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.CommandButton";

	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.CommandButton";

	@Attribute
	public abstract String getColor();

	public List<UIComponent> getFacetChildren(String facetName)
	{
		UIComponent facet = getFacet(facetName);
		if (facet != null && facet.isRendered())
		{
			if ("javax.faces.Panel".equals(facet.getFamily()))
			{
				return facet.getChildren();
			}
			else
			{
				List<UIComponent> children = new ArrayList<UIComponent>();
				children.add(facet);
				return children;
			}
		}
		return null;
	}

	/**
	 * The icon to be displayed with the CommandButton
	 */
	@Attribute
	public abstract String getIcon();

	/**
	 * <p>
	 * Determines how the menu item requests are submitted. Valid values:
	 * </p>
	 * <ol>
	 * <li>server, the default setting, submits the form normally and completely
	 * refreshes the page.</li>
	 * <li>ajax performs an Ajax form submission, and re-renders elements
	 * specified with the render attribute.</li>
	 * <li>
	 * client causes the action and actionListener items to be ignored, and the
	 * behavior is fully defined by the nested components instead of responses
	 * from submissions</li>
	 * </ol>
	 */
	@Attribute(defaultValue = "Mode.ajax")
	public abstract Mode getMode();

	@Attribute(suggestedValue = BootstrapScale.XS + "," + BootstrapScale.SM + "," + BootstrapScale.LG)
	public abstract String getScale();

	@Override
	public String getSeparatorFacetRendererType()
	{
		return "com.capexil.bootstrap.CommandButtonSeparatorFacetRenderer";
	}

	@Attribute(suggestedValue = BootstrapSeverity.PRIMARY + "," + BootstrapSeverity.SUCCESS + ","
			+ BootstrapSeverity.INFO + "," + BootstrapSeverity.WARNING + "," + BootstrapSeverity.DANGER + ","
			+ BootstrapSeverity.INVERSE)
	public abstract String getSeverity();

	/**
	 * HMTL tag used to create the button. Can be either "button" or "input". If
	 * not specified, the default value is "button".
	 */
	@Attribute(defaultValue = "button")
	public abstract String getTag();

	@Attribute(hidden = true)
	public abstract Object getValue();

	public boolean hasFacet(String facetName)
	{
		return getFacet(facetName) != null && getFacet(facetName).isRendered();
	}

	public String resolveClientId(FacesContext facesContext, UIComponent contextComponent, String metaComponentId)
	{
		return null;
	}

	public String substituteUnresolvedClientId(FacesContext facesContext, UIComponent contextComponent,
			String metaComponentId)
	{
		if (AjaxContainer.META_COMPONENT_ID.equals(metaComponentId))
		{
			return AjaxConstants.FORM;
		}
		return null;
	}
}