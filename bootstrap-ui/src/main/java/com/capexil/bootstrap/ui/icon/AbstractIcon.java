package com.capexil.bootstrap.ui.icon;

import javax.faces.component.UIOutput;

import org.richfaces.cdk.annotations.Attribute;
import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.JsfRenderer;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.ui.attribute.CoreProps;

/**
 * Base class for the icon component
 * 
 * 
 */
@JsfComponent(type = AbstractIcon.COMPONENT_TYPE, family = AbstractIcon.COMPONENT_FAMILY, renderer = @JsfRenderer(
		type = IconRendererBase.RENDERER_TYPE), tag = @Tag(name = "icon"))
public abstract class AbstractIcon extends UIOutput implements CoreProps
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.Icon";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.Icon";

	@Attribute
	public abstract String getColor();
}
