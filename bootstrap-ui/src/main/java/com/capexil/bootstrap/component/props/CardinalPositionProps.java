package com.capexil.bootstrap.component.props;

import org.richfaces.cdk.annotations.Attribute;

import com.capexil.bootstrap.component.HorizontalPosition;
import com.capexil.bootstrap.component.VerticalPosition;

public interface CardinalPositionProps
{

	/**
	 * This attribute allows you to position horizontally a component or its
	 * content.
	 */
	@Attribute(suggestedValue = "left,right")
	HorizontalPosition getHorizontal();

	/**
	 * This attribute allows you to position vertically a component or its
	 * content.
	 */
	@Attribute(suggestedValue = "top,bottom")
	VerticalPosition getVertical();
}
