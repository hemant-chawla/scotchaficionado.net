package com.capexil.bootstrap.component;

public final class BootstrapSeverity
{
	public static final String DANGER = "danger";
	public static final String ERROR = "error";
	public static final String IMPORTANT = "important";
	public static final String INFO = "info";
	public static final String INVERSE = "inverse";
	public static final String PRIMARY = "primary";
	public static final String SUCCESS = "success";
	public static final String WARNING = "warning";

	private BootstrapSeverity()
	{
	}
}
