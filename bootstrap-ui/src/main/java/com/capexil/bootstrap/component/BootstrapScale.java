package com.capexil.bootstrap.component;

public final class BootstrapScale
{
	public static final String LG = "lg";
	public static final String MD = "md";
	public static final String SM = "sm";
	public static final String XS = "xs";

	private BootstrapScale()
	{
	}
}
