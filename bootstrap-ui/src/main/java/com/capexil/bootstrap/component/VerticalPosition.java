package com.capexil.bootstrap.component;

public enum VerticalPosition
{
	bottom(""), top("dropup");

	private String buttonClass;

	private VerticalPosition(String buttonClass)
	{
		this.buttonClass = buttonClass;
	}

	public String getButtonClass()
	{
		return buttonClass;
	}
}
