package com.capexil.bootstrap.component;

public enum HorizontalPosition
{
	left("", "pull-left"), right("pull-right", "pull-right");

	private String buttonClass;

	private String navbarClass;

	private HorizontalPosition(String buttonClass, String navbarClass)
	{
		this.buttonClass = buttonClass;
		this.navbarClass = navbarClass;
	}

	public String getButtonClass()
	{
		return buttonClass;
	}

	public String getNavbarClass()
	{
		return navbarClass;
	}
}
