package com.capexil.bootstrap.function;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.richfaces.cdk.annotations.Function;
import org.richfaces.ui.misc.RichFunction;

import com.capexil.bootstrap.javascript.api.Hideable;
import com.capexil.bootstrap.javascript.api.Showable;
import com.capexil.bootstrap.javascript.api.Toggleable;

/**
 * Bootstrap specific functions to operate with client-side component API.
 * 
 */
public final class BootstrapFunction
{
	private static String bootstrapCall(String target, Class<?> type, String operationName)
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		UIComponent component = RichFunction.findComponent(facesContext, target);
		BootstrapOperation operation = new BootstrapOperation(facesContext, component, operationName);

		operation.verifyComponent(type);
		String call = operation.getClientSideCall();
		return call;
	}

	@Function
	public static String hide(String target)
	{
		return bootstrapCall(target, Hideable.class, "hide");
	}

	@Function
	public static String show(String target)
	{
		return bootstrapCall(target, Showable.class, "show");
	}

	@Function
	public static String toggle(String target)
	{
		return bootstrapCall(target, Toggleable.class, "toggle");
	}
}
