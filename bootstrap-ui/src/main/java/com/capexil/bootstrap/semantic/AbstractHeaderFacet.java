package com.capexil.bootstrap.semantic;

import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.ui.attribute.CoreProps;

/**
 * Base class for the header component
 * 
 */
@JsfComponent(type = AbstractHeaderFacet.COMPONENT_TYPE, family = AbstractHeaderFacet.COMPONENT_FAMILY, tag = @Tag(
		name = "headerFacet"))
public abstract class AbstractHeaderFacet extends AbstractSemanticComponentBase<RenderHeaderFacetCapable> implements
		CoreProps
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.HeaderFacet";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.HeaderFacet";

	@Override
	public Class<RenderHeaderFacetCapable> getRendererCapability()
	{
		return RenderHeaderFacetCapable.class;
	}

	@Override
	public String getRendererType(RenderHeaderFacetCapable container)
	{
		return container.getHeaderFacetRendererType();
	}
}
