package com.capexil.bootstrap.semantic;

import javax.faces.component.UIComponent;

import org.richfaces.cdk.annotations.Attribute;
import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.Tag;

/**
 * Base class for the menuFacet component.
 * 
 */
@JsfComponent(type = AbstractMenuFacet.COMPONENT_TYPE, family = AbstractMenuFacet.COMPONENT_FAMILY, tag = @Tag(
		name = "menuFacet"))
public abstract class AbstractMenuFacet extends AbstractSemanticComponentBase<RenderMenuFacetCapable>
{
	public static final String ACTIVE_ATTRIBUTE_NAME = "active";
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.MenuFacet";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.MenuFacet";

	@Attribute
	public abstract String getLabel();

	public int getLevel()
	{
		return getLevel(null);
	}

	public int getLevel(String stopParentFamily)
	{
		UIComponent parent = this.getParent();
		int level = 0;

		while (parent != null)
		{
			if (parent instanceof AbstractMenuFacet)
			{
				++level;
			}

			if (stopParentFamily != null && stopParentFamily.equals(parent.getFamily()))
			{
				parent = null;
			}
			else
			{
				parent = parent.getParent();
			}
		}

		return level;
	}

	@Override
	public Class<RenderMenuFacetCapable> getRendererCapability()
	{
		return RenderMenuFacetCapable.class;
	}

	@Override
	public String getRendererType(RenderMenuFacetCapable container)
	{
		return container.getMenuFacetRendererType();
	}

	@Attribute
	public abstract boolean isActive();
}
