package com.capexil.bootstrap.semantic;

public interface RenderBodyFacetCapable
{
	String getBodyFacetRendererType();
}
