package com.capexil.bootstrap.semantic;

public interface RenderMenuFacetCapable
{
	String getMenuFacetRendererType();
}
