package com.capexil.bootstrap.semantic;

public interface RenderFooterFacetCapable
{
	String getFooterFacetRendererType();
}
