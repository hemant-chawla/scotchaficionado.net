package com.capexil.bootstrap.semantic;

public interface RenderHeaderFacetCapable
{
	String getHeaderFacetRendererType();
}
