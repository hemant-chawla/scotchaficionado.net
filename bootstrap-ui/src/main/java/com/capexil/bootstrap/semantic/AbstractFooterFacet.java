package com.capexil.bootstrap.semantic;

import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.ui.attribute.CoreProps;

/**
 * Base class for the footer component
 * 
 */
@JsfComponent(type = AbstractFooterFacet.COMPONENT_TYPE, family = AbstractFooterFacet.COMPONENT_FAMILY, tag = @Tag(
		name = "footerFacet"))
public abstract class AbstractFooterFacet extends AbstractSemanticComponentBase<RenderFooterFacetCapable> implements
		CoreProps
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.FooterFacet";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.FooterFacet";

	@Override
	public Class<RenderFooterFacetCapable> getRendererCapability()
	{
		return RenderFooterFacetCapable.class;
	}

	@Override
	public String getRendererType(RenderFooterFacetCapable container)
	{
		return container.getFooterFacetRendererType();
	}
}
