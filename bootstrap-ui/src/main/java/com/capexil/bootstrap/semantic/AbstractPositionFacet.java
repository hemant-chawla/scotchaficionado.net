package com.capexil.bootstrap.semantic;

import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.Tag;

import com.capexil.bootstrap.component.props.CardinalPositionProps;

/**
 * Base class for the FacetPosition component.
 * 
 */
@JsfComponent(type = AbstractPositionFacet.COMPONENT_TYPE, family = AbstractPositionFacet.COMPONENT_FAMILY, tag = @Tag(
		name = "positionFacet"))
public abstract class AbstractPositionFacet extends AbstractSemanticComponentBase<RenderPositionFacetCapable> implements
		CardinalPositionProps
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.PositionFacet";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.PositionFacet";

	@Override
	public Class<RenderPositionFacetCapable> getRendererCapability()
	{
		return RenderPositionFacetCapable.class;
	}

	@Override
	public String getRendererType(RenderPositionFacetCapable container)
	{
		return container.getPositionFacetRendererType();
	}
}
