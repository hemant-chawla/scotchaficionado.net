package com.capexil.bootstrap.semantic;

import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.ui.attribute.CoreProps;

/**
 * Base class for the separator semantic component
 * 
 */
@JsfComponent(type = AbstractSeparatorFacet.COMPONENT_TYPE, family = AbstractSeparatorFacet.COMPONENT_FAMILY,
		tag = @Tag(name = "separatorFacet"))
public abstract class AbstractSeparatorFacet extends AbstractSemanticComponentBase<RenderSeparatorFacetCapable>
		implements CoreProps
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.SeparatorFacet";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.SeparatorFacet";

	@Override
	public Class<RenderSeparatorFacetCapable> getRendererCapability()
	{
		return RenderSeparatorFacetCapable.class;
	}

	@Override
	public String getRendererType(RenderSeparatorFacetCapable container)
	{
		return container.getSeparatorFacetRendererType();
	}

}
