package com.capexil.bootstrap.semantic;

public interface RenderPositionFacetCapable
{
	String getPositionFacetRendererType();
}
