package com.capexil.bootstrap.semantic;

import org.richfaces.cdk.annotations.JsfComponent;
import org.richfaces.cdk.annotations.Tag;
import org.richfaces.ui.attribute.CoreProps;

/**
 * Base class for the body component
 * 
 */
@JsfComponent(type = AbstractBodyFacet.COMPONENT_TYPE, family = AbstractBodyFacet.COMPONENT_FAMILY, tag = @Tag(
		name = "bodyFacet"))
public abstract class AbstractBodyFacet extends AbstractSemanticComponentBase<RenderBodyFacetCapable> implements
		CoreProps
{
	public static final String COMPONENT_FAMILY = "com.capexil.bootstrap.BodyFacet";
	public static final String COMPONENT_TYPE = "com.capexil.bootstrap.BodyFacet";

	@Override
	public Class<RenderBodyFacetCapable> getRendererCapability()
	{
		return RenderBodyFacetCapable.class;
	}

	@Override
	public String getRendererType(RenderBodyFacetCapable container)
	{
		return container.getBodyFacetRendererType();
	}

}
