package com.capexil.bootstrap.semantic;

public interface RenderSeparatorFacetCapable
{
	String getSeparatorFacetRendererType();
}
