package com.capexil.bootstrap;

import org.richfaces.configuration.ConfigurationItem;

public final class BootstrapConfiguration
{
	public enum Items
	{
		@ConfigurationItem(defaultValue = "false", names = CLIENT_SIDE_STYLE_PARAM_NAME)
		clientSideStyle
	}

	public static final java.lang.String CLIENT_SIDE_STYLE_PARAM_NAME = "com.capexil.clientSideStyle";

	private BootstrapConfiguration()
	{
	}
}
